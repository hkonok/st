-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 19, 2014 at 11:29 AM
-- Server version: 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `st`
--
CREATE DATABASE IF NOT EXISTS `st` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `st`;

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE IF NOT EXISTS `class` (
  `id` int(30) NOT NULL AUTO_INCREMENT,
  `teacher_id` int(30) NOT NULL,
  `start_time` time NOT NULL,
  `finishing_time` time NOT NULL,
  `days` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`id`, `teacher_id`, `start_time`, `finishing_time`, `days`) VALUES
(2, 13, '03:00:00', '04:00:00', 'Monday,Thursday,Saturday'),
(3, 7, '02:30:00', '02:30:00', 'Saturday'),
(4, 12, '02:30:00', '02:30:00', 'Tuesday,Sunday'),
(5, 7, '02:30:00', '02:30:00', 'Sunday,Tuesday');

-- --------------------------------------------------------

--
-- Table structure for table `class_student`
--

CREATE TABLE IF NOT EXISTS `class_student` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `class_id` int(20) NOT NULL,
  `student_id` int(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `class_student`
--

INSERT INTO `class_student` (`id`, `class_id`, `student_id`) VALUES
(3, 2, 6),
(4, 2, 15),
(5, 3, 6),
(6, 3, 15),
(7, 4, 14),
(8, 4, 16),
(9, 5, 14),
(10, 5, 15);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_2` (`name`),
  KEY `id` (`id`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `course_assigned`
--

CREATE TABLE IF NOT EXISTS `course_assigned` (
  `teacher_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `course_taken`
--

CREATE TABLE IF NOT EXISTS `course_taken` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `customer_service`
--

CREATE TABLE IF NOT EXISTS `customer_service` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `country` text NOT NULL,
  `time_zone` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer_service`
--

INSERT INTO `customer_service` (`id`, `first_name`, `last_name`, `country`, `time_zone`) VALUES
(8, 'Rafi', 'Ahmed', '', 0),
(9, 'Dulal', 'Mia', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `manager`
--

CREATE TABLE IF NOT EXISTS `manager` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `country` text NOT NULL,
  `time_zone` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `manager`
--

INSERT INTO `manager` (`id`, `first_name`, `last_name`, `country`, `time_zone`) VALUES
(2, 'Robiul', 'Islam', '', 0),
(3, 'Rishad', 'Rizwan', '', 0),
(4, 'Farhan', 'Akhter', '', 0),
(5, 'Anisur', 'Rahman', '', 0),
(10, 'Naimul', 'Arif', '', 0),
(11, 'Muntakim', 'Sadik', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE IF NOT EXISTS `student` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `birthday` date NOT NULL,
  `country` text NOT NULL,
  `time_zone` tinyint(4) NOT NULL,
  `paid_bill` float NOT NULL,
  `due_bill` float NOT NULL,
  `coupon` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`),
  KEY `id_3` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `first_name`, `last_name`, `birthday`, `country`, `time_zone`, `paid_bill`, `due_bill`, `coupon`) VALUES
(6, 'Imtiaz', 'Hasan', '0000-00-00', '', 0, 0, 0, ''),
(14, 'Robiul', 'Islam', '0000-00-00', '', 0, 0, 0, ''),
(15, 'Saidur', 'Rahman', '0000-00-00', '', 0, 0, 0, ''),
(16, 'Rashed', 'Khan', '0000-00-00', '', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendence`
--

CREATE TABLE IF NOT EXISTS `student_attendence` (
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `is_attend` tinyint(1) NOT NULL,
  KEY `student_id` (`student_id`),
  KEY `course_id` (`course_id`),
  KEY `datetime` (`datetime`),
  KEY `is_attend` (`is_attend`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `super_admin`
--

CREATE TABLE IF NOT EXISTS `super_admin` (
  `id` int(11) NOT NULL,
  `frist_name` text NOT NULL,
  `last_name` text NOT NULL,
  `country` text NOT NULL,
  `time_zone` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE IF NOT EXISTS `teacher` (
  `id` int(11) NOT NULL,
  `first_name` text NOT NULL,
  `last_name` text NOT NULL,
  `country` text NOT NULL,
  `time_zone` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_2` (`id`),
  KEY `id` (`id`),
  KEY `time_zone` (`time_zone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacher`
--

INSERT INTO `teacher` (`id`, `first_name`, `last_name`, `country`, `time_zone`) VALUES
(7, 'Sudipto', 'Chalma', '', 0),
(12, 'Miraz', 'Ahmed', '', 0),
(13, 'Faisal', 'Ahmed', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `teacher_attendence`
--

CREATE TABLE IF NOT EXISTS `teacher_attendence` (
  `teacher_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `is_attend` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(60) NOT NULL,
  `user_type` tinyint(4) NOT NULL,
  `password` text NOT NULL,
  `reg_datetime` datetime NOT NULL,
  `last_login_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `user_type`, `password`, `reg_datetime`, `last_login_datetime`) VALUES
(2, 'masummsm@gmail.com', 1, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 10:27:33', '0000-00-00 00:00:00'),
(3, 'rishadrizwan71@gmail.com', 1, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 10:33:17', '0000-00-00 00:00:00'),
(4, 'farhan_buet_010@yahoo.com', 1, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 10:34:33', '0000-00-00 00:00:00'),
(5, 'anisur_rahman251@yahoo.com', 1, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 10:38:32', '0000-00-00 00:00:00'),
(6, 'imtiazhimu@yahoo.com', 3, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 11:27:59', '0000-00-00 00:00:00'),
(7, 'sudeeptoeee@yahoo.com', 2, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 11:28:56', '0000-00-00 00:00:00'),
(8, 'rafibuet09@gmail.com', 4, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 11:29:54', '0000-00-00 00:00:00'),
(9, 'abd_eee08@yahoo.com', 4, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 22:36:04', '0000-00-00 00:00:00'),
(10, 'ddss@yahoo.com', 1, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 22:43:58', '0000-00-00 00:00:00'),
(11, 'faul@yahoo.com', 1, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-01 22:44:42', '0000-00-00 00:00:00'),
(12, 'mm@yahoo.com', 2, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-16 09:04:17', '0000-00-00 00:00:00'),
(13, 'fff@yahoo.com', 2, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-16 09:04:44', '0000-00-00 00:00:00'),
(14, 'rrr101@gmail.com', 3, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-16 09:05:12', '0000-00-00 00:00:00'),
(15, 'sujon335@yahoo.com', 3, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-16 09:05:32', '0000-00-00 00:00:00'),
(16, 'rashed@gmail.com', 3, '81dc9bdb52d04dc20036dbd8313ed055', '2014-03-16 09:05:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE IF NOT EXISTS `user_group` (
  `level` tinyint(4) NOT NULL,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`name`),
  UNIQUE KEY `level` (`level`),
  UNIQUE KEY `name_2` (`name`),
  KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
