
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Customer Service<small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Running Classes</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="fa fa-calendar"></i>
                            <span>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="clearfix">
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa  fa-ticket"></i>Running Classes
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="list-group">
                            <?php
                            if (sizeof($list_class) > 0):
                                foreach ($list_class as $row):
                                    
                                    ?>
                                    <a href="<?php echo base_url(); ?>index.php/admin/class_view/<?php echo $row['class_id']; ?>" class="list-group-item">
                                        <?php 
                                            echo $row['course_name'].' - '.$row['course_level']." ( ".$row['student_name']." )";
                                        ?>
                                        <span class="badge badge-info">
                                            <?php
                                            echo $row['datetime'];
                                            ?>
                                        </span>
                                    </a>
                                    <?php
                                endforeach; 
                            endif;
                            ?>

                        </div>

                    </div>
                </div>

            </div>

           
        </div>
     
    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
