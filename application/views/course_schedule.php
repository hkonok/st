<?php
$course_name = '';
if ($course_data['course_id'] == 1)
    $course_name = "Arabic";
else if ($course_data['course_id'] == 1)
    $course_name = "Quran";
else
    $course_name = "Arabic and Quran";

$level_name = "";
if ($course_data['level'] == 1)
    $level_name = "Beginner";
else if ($course_data['level'] == 6)
    $level_name = "Intermediate";
else
    $level_name = "Advanced";
?>


<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Schedule Time<small></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        <a href="<?php echo base_url(); ?>index.php/admin/pending_request/">Pending Request</a>
                        <i class="fa fa-angle-right"></i>
                        <a href="<?php echo base_url(); ?>index.php/admin/pending_request_details/<?php echo $request_id; ?>/"> <?php echo $course_name." ( ".$level_name." ) "; ?></a>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <?php if (validation_errors() != ''): ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($success) && $success == 1): ?>
                <div class="alert alert-success">
                    New course request has been successfully submitted. 
                </div>
            <?php endif; ?>

            <?php if (isset($success) && $success == -1): ?>
                <div class="alert alert-danger">
                    You already have a pending request.
                </div>
            <?php endif; ?>
        </div>



        <!-- BEGIN SAMPLE TABLE PORTLET-->
        <div class="row">

            <div class="portlet box green  col-md-9" style="margin-left: 10px;">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-clock-o"></i><?php echo $course_name . " ( " . $level_name . " ) - Expected Time Schedule"; ?>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Day
                                    </th>
                                    <th>
                                        Time
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($schedule as $row): $i++;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td class="success">
                                            <?php echo $row['day']; ?>
                                        </td>
                                        <td class="warning">
                                            <?php echo $row['time'] ?>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->

        <!-- END SAMPLE TABLE PORTLET-->



        <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/add_student_schedule_time/<?php echo $request_id; ?>/course_schedule/">
            <div class="row">
                <div class="form-group">
                    <label class="control-label col-md-3">Select Day</label>
                    <div class="col-md-4">
                        <select class="form-control select2me" name="day" data-placeholder="Select...">
                            <option value=""></option>
                            <option value="Saturday">Saturday</option>
                            <option value="Sunday">Sunday</option>
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-md-3">Time</label>
                    <div class="col-md-3">
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" name="time" value="00:00:00" class="form-control timepicker-24" readonly>
                            <span class="input-group-btn">
                                <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                            </span>

                        </div>
                        <span class="help-block">
                            Please click on the clock icon to change time
                        </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn green">Add</button>

                    </div>
                </div>
            </div>
        </form>
        <br><br>
        <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_student_schedule_time/<?php echo $request_id; ?>/course_schedule/">
            <div class="row">
                <div class="form-group">
                    <label class="control-label col-md-3">Remove a Schedule</label>
                    <div class="col-md-4">
                        <select class="form-control select2me" name="remove_schedule" data-placeholder="Select...">
                            <option value=""></option>
                            <?php foreach ($schedule as $row): ?>
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['day'] . "( " . $row['time'] . " )"; ?></option>
                            <?php endforeach; ?>

                        </select>

                    </div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn green">Remove</button>
                    </div>
                </div>
            </div>
        </form>


    </div>




    <!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT -->

<!-- END CONTAINER -->