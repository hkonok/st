
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Customer Service<small>statistics and more</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Tickets</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                  
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="fa fa-calendar"></i>
                            <span>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="clearfix">
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa  fa-ticket"></i>Tickets
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="list-group">
                            <?php
                            if (isset($ticket_list)):
                                foreach ($ticket_list as $row):
                                    ?>
                                    <a href="<?php echo base_url(); ?>index.php/admin/ticket_message/<?php echo $row['id']; ?>" class="list-group-item">
                                        <?php echo $row['name']; ?>
                                        <span class="badge badge-info">
                                            <?php
                                            $datetime = new DateTime($row['datetime']); // current time = server time
                                            
                                            echo  $datetime->format('d, M Y H:i:s');
                                            ?>
                                        </span>
                                    </a>
                                    <?php
                                endforeach;
                            endif;
                            ?>

                        </div>

                    </div>
                </div>

            </div>

            <!-- END PAGINATION PORTLET-->
        </div>
        <div class="row">
            <div class="col-md-9">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa   fa-plus"></i>Create New Ticket
                        </div>
                    </div>

                    <div class="portlet-body">
                        <form action="" class="form-horizontal" method="post">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Subject</label>
                                    <div class="col-md-8">
                                        <input type="text" name="subject" class="form-control" placeholder="Ticket Subject">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Message</label>
                                    <div class="col-md-8" >
                                        <textarea name="message" class="form-control" rows="5" placeholder="Enter Message"></textarea>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Create Ticket</button>
                                </div>
                            </div>



                        </form>

                    </div>
                </div>

            </div>

            <!-- END PAGINATION PORTLET-->
        </div>
    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
