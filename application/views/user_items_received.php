<?php
switch ($user_basic['user_type']) {
    case 0 :
        $user_type_name = 'Super Admin';
        break;
    case 1:
        $user_type_name = 'Manger';
        break;
    case 2:
        $user_type_name = 'Techniclal Manager';
        break;
    case 3:
        $user_type_name = 'Teacher Manager';
        break;
    case 4:
        $user_type_name = 'Personnel Manager';
        break;
    case 5:
        $user_type_name = 'Customer Support';
        break;
    case 6:
        $user_type_name = 'Teacher';
        break;
    case 7:
        $user_type_name = 'Student';
        break;
    default :
        $user_type_name = "";
}
?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name']." ".$user_data['last_name']; ?> <small><?php echo $user_type_name; ?> Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#"><?php echo $user_type_name; ?> Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Items Received From Company
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            Item
                                                        </th>
                                                        <th>
                                                            Date
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=0; foreach($items as $row): $i++; ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td class="success">
                                                            <?php echo $row['item']; ?>
                                                        </td>
                                                        <td class="warning">
                                                            <?php echo $row['date'] ?>
                                                        </td>

                                                    </tr>
                                                    <?php endforeach; ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <?php if($user_type == 4): ?>
                                <!-- END SAMPLE TABLE PORTLET-->
                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url() ?>index.php/admin/item_received_add_item/<?php echo $teacher_id; ?>/">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Add Item</label>
                                            <div class="col-md-4">
                                                <select class="form-control select2me" name="item" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="Banner">Banner</option>
                                                    <option value="Camera">Camera</option>
                                                    <option value="Headset">Headset</option>
                                                    <option value="Flash USB">Flash USB</option>
                                                    <option value="SIM card">SIM card</option>
                                                    <option value="Visa payroll">Visa payroll</option>
                                                    <option value="Electronic board + 1electronic pen">Electronic board + 1electronic pen</option>
                                                </select>
                                               
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Date of borrowing</label>
                                            <div class="col-md-3">
                                                <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" name="date" size="16" type="text" value=""/>
                                                <span class="help-block">
                                                    Select date
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn green">Add</button>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <br><br>
                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/item_received_return_item/<?php echo $teacher_id ?>/">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Return Item</label>
                                            <div class="col-md-4">
                                                <select class="form-control select2me" name="return_item" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <?php foreach ($items as $row): ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['item']."( ".$row['date']." )"; ?></option>
                                                    <?php endforeach; ?>
                                                   
                                                </select>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Date of returning</label>
                                            <div class="col-md-3">
                                                <input class="form-control form-control-inline input-medium date-picker" data-date-format="yyyy-mm-dd" name="returning_date" size="16" type="text" value=""/>
                                                <span class="help-block">
                                                    Select date
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn green">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
