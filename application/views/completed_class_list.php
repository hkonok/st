<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Completed Class List 
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-power-off"></i>
                        <a href="#">Completed Class</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet ">
                    
                    <div class="portlet-body">

                        <?php if (isset($completed_class_list)) : ?>
                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>
                                    <tr>
                                        <th >
                                            Course Name
                                        </th>
                                        <th>
                                            Teacher Name
                                        </th>
                                        <th>
                                            Students
                                        </th>
                                        <th>
                                            Started On
                                        </th>
                                        <th>
                                            End Date
                                        </th>
                                        <?php if(isset($user_type) && $user_type == 5):  ?>
                                        <th>
                                            View Details
                                        </th>
                                        <?php endif; ?>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $class_name = array(
                                            '1' => 'Arabic',
                                            '2' => 'Quran',
                                            '3' => 'Arabic and Quran'
                                        );
                                    foreach ($completed_class_list as $row) : ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $class_name[$row['course_id']]; ?>
                                            </td>
                                            <td>
                                                <?php echo $teacher_details[$row['id']]['first_name']." ".$teacher_details[$row['id']]['last_name']; ?>
                                                <br>
                                                <?php echo $teacher_basic[$row['id']]['email']; ?>
                                            </td>
                                            <td>
                                                <?php
                                                    foreach($class_students[$row['id']] as $student_data){
                                                        echo $student_data['first_name']." ".$student_data['last_name']." (".$student_data['email'].")<br>";
                                                    }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                    $datetime = new DateTime($row['start_date']);
                                                    echo date_format($datetime, "M d, Y"); 
                                                 ?>
                                            </td>
                                            <td>
                                                <?php
                                                 if($row['end_date'] != "0000-00-00 00:00:00"){
                                                    $datetime = new DateTime($row['end_date']);
                                                    echo date_format($datetime, "M d, Y");
                                                 }
                                                 else {
                                                     echo "None";
                                                 }
                                                 ?>
                                            </td>
                                            <?php if(isset($user_type) && $user_type == 5):  ?>
                                            <td class="center">
                                                <div class="col-md-12" style="margin-bottom: 10px;">
                                                    <a href="<?php echo base_url(); ?>index.php/admin/assign_class_details/<?php echo $row['id']; ?>" class="btn green"> View</a>
                                                </div>
                                            </td>
                                            <?php endif; ?>
                                        </tr>

                                    <?php endforeach; ?>

                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>
    <!-- END PAGE CONTENT-->
</div>
