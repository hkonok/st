<?php
switch ($user_basic['user_type']) {
    case 0 :
        $user_type_name = 'Super Admin';
        break;
    case 1:
        $user_type_name = 'Manger';
        break;
    case 2:
        $user_type_name = 'Techniclal Manager';
        break;
    case 3:
        $user_type_name = 'Teacher Manager';
        break;
    case 4:
        $user_type_name = 'Personnel Manager';
        break;
    case 5:
        $user_type_name = 'Customer Support';
        break;
    case 6:
        $user_type_name = 'Teacher';
        break;
    case 7:
        $user_type_name = 'Student';
        break;
    default :
        $user_type_name = "";
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name'] . " " . $user_data['last_name']; ?> <small><?php echo $user_type_name; ?> Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#"><?php echo $user_type_name; ?> Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">



                                <?php if(isset($rating_list) && sizeof($rating_list) > 0):
                                        $i = 0;
                                        foreach($rating_list as $row):
                                    ?>
                                <div class="row">
                                    <div class="col-md-9 news-page">
                                        <div class="news-blocks" style="margin-bottom: 1px;">
                                            <?php if(isset($user_type) && $user_type <= 2): ?>
                                            <h4><?php if(isset($rating_from[$i])) echo $rating_from[$i]; ?></h4>
                                            <?php endif; ?>
                                            <div class="row" style="margin-left: 1%;">
                                            <div id="star<?php echo $i; ?>"></div>
                                            </div>
                                            <div class="news-block-tags">
                                                <strong><?php echo $row['rating']; ?>/5</strong>
                                                <em> <?php
                                                    $datetime  = new DateTime($row['datetime']);
                                                    echo date_format($datetime, "M d, y");
                                                ?></em>
                                            </div>
                                            <p>
                                                <?php echo $row['feedback']; ?>
                                            </p>
                                            
                                        </div>
                                    </div>
                                </div>
                                <?php $i++; endforeach; ?>
                                <?php else: ?>
                                
                                <?php endif; ?>

                                <?php if(isset($user_type) && ($user_type < 2 || $user_type == 4)): ?>
                                <div class="row">
                                    <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/rate_teacher/-200/<?php echo $user_data['id']; ?>/">


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Give Rating*</label>


                                                <div class="col-md-8">
                                                    <div id="star"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Give Feedback*</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="feedback" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn green">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <?php endif; ?>

                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
