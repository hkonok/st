<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
							 Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
					Users <small>List of Classes</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-group"></i>
                        <a href="index.html">Users</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">students</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">List</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div >
                <?php if (isset($msg))
                    echo $msg; ?>
            </div>
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Class List
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <a href="<?php echo base_url(); ?>index.php/admin/create_class" class="btn blue">Create Class <i class="fa fa-plus"></i></a>
                            </div>

                        </div>
<?php if (isset($class_list)) { ?>
                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th >
                                                                    
                                    </th>
                                    <th>
									Teacher
                                    </th>
                                    <th>
									Days
                                    </th>
                                    <th>
									Starting Time
                                    </th>
                                    <th>
									Finishing Time
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
<?php foreach ($class_list as $row) { ?>
                                <tr class="odd gradeX">
                                    <td>

                                    </td>
                                    <td>
<?php echo $row->first_name; ?>
                                    </td>
                                    <td>
<?php echo $row->days; ?>
                                    </td>
                                    <td>
<?php echo $row->start_time; ?>
                                    </td>
                                    <td class="center">
<?php echo $row->finishing_time; ?>
                                    </td>
                                    <td>
                                      

                                    </td>
                                </tr>

<?php } ?>

                            </tbody>
                        </table>
<?php } ?>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>
    <!-- END PAGE CONTENT-->
</div>
