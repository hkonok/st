<?php
$course_name = '';
if ($request_data['course_id'] == 1)
    $course_name = "Arabic";
else if ($request_data['course_id'] == 2)
    $course_name = "Quran";
else
    $course_name = "Arabic and Quran";

$level_name = "";

if ($request_data['level'] == 1)
    $level_name = "Beginner";
else if ($request_data['level'] == 2)
    $level_name = "Intermediate";
else
    $level_name = "Advanced";
?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Pending Request
                </h3>

                

                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-clock-o"></i>
                        <a href="<?php echo base_url(); ?>index.php/admin/pending_request/">Pending Request</a>
                        <i class="fa fa-angle-right"></i>
                        <a href="#"><?php echo $course_name . " ( " . $level_name . " )"; ?></a>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <?php if (validation_errors() != ''): ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($success) && $success == 1): ?>
                <div class="alert alert-success">
                    New course request has been successfully submitted. 
                </div>
            <?php endif; ?>

            <?php if (isset($success) && $success == -1): ?>
                <div class="alert alert-danger">
                    You already have a pending request.
                </div>
            <?php endif; ?>
        </div>

        <div class="table-toolbar">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <a href="<?php echo base_url(); ?>index.php/admin/student_remove_pending_request/<?php echo $request_id; ?>/" class="btn red">Remove Request <i class="fa fa-times"></i></a>
                    </div>

                </div>
        
        <hr>
        
        
        <div class="row">
            <div class="page-breadcrumb breadcrumb"><li><label class="control-label">Update Request</label></li></div>
            <!-- BEGIN FORM-->
            <form action="" class="form-horizontal" method="post">
                <div class="form-body">

                    <div class="form-group">
                        <label class="control-label col-md-3">Level</label>
                        <div class="col-md-4">
                            <select name="level" class="form-control input-large select2me" data-placeholder="Select...">
                                <option value=""></option>
                                <option value="1" <?php if ($request_data['level'] == 1) echo "selected"; ?>>Beginner</option>
                                <option value="6" <?php if ($request_data['level'] == 6) echo "selected"; ?>>Intermediate</option>
                                <option value="13" <?php if ($request_data['level'] == 13) echo "selected"; ?>>Advanced</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Skills Should be in Teacher</label>
                        <div class="col-md-4">
                            <select name="teacher_skill" class="form-control input-large select2me" data-placeholder="Select...">
                                <option value=""></option>
                                <option value="1" <?php if ($request_data['skills_in_teacher'] == 1) echo "selected"; ?>>Good English</option>
                                <option value="2" <?php if ($request_data['skills_in_teacher'] == 2) echo "selected"; ?>>Nice</option>
                                <option value="3" <?php if ($request_data['skills_in_teacher'] == 3) echo "selected"; ?>>Good With Kids</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Study Hours/Week</label>
                        <div class="col-md-4">
                            <select name="study_hours" class="form-control input-large select2me" data-placeholder="Select...">
                                <option value=""></option>
                                <option value="1" <?php if ($request_data['study_hours'] == 1) echo "selected"; ?>>Light</option>
                                <option value="2" <?php if ($request_data['study_hours'] == 2) echo "selected"; ?>>Standard</option>
                                <option value="3" <?php if ($request_data['study_hours'] == 3) echo "selected"; ?>>Intensive</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Start Date</label>
                        <div class="col-md-3">
                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                <input name="start_date" type="text" class="form-control" value="<?php echo $request_data['start_date']; ?>" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                </span>
                            </div>
                            <!-- /input-group -->

                        </div>
                    </div>

                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn blue">Save Changes</button>
                            <a class="btn default" href="<?php echo base_url(); ?>index.php/admin/pending_request/">Cancel</a>
                        </div>
                    </div>

                </div>

            </form>
        </div>
        <!-- END FORM-->
        <br><br>
        <div class="row">
            <!-- BEGIN SAMPLE TABLE PORTLET-->

            <div class="portlet box green  col-md-9" style="margin-left: 10px;">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-clock-o"></i><?php echo $course_name . " ( " . $level_name . " ) - Expected Time Schedule"; ?>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>
                                        #
                                    </th>
                                    <th>
                                        Day
                                    </th>
                                    <th>
                                        Time
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i = 0;
                                foreach ($schedule as $row): $i++;
                                    ?>
                                    <tr>
                                        <td>
                                            <?php echo $i; ?>
                                        </td>
                                        <td class="success">
                                            <?php echo $row['day']; ?>
                                        </td>
                                        <td class="warning">
                                            <?php echo $row['time'] ?>
                                        </td>

                                    </tr>
                                <?php endforeach; ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- -->

            <!-- END SAMPLE TABLE PORTLET-->
        </div><!-- end row-->



        <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/add_student_schedule_time/<?php echo $request_id; ?>/pending_request_details/">

            <div class="form-group">
                <div class="row">
                    <label class="control-label col-md-3">Select Day</label>
                    <div class="col-md-4">
                        <select class="form-control select2me" name="day" data-placeholder="Select...">
                            <option value=""></option>
                            <option value="Saturday">Saturday</option>
                            <option value="Sunday">Sunday</option>
                            <option value="Monday">Monday</option>
                            <option value="Tuesday">Tuesday</option>
                            <option value="Wednesday">Wednesday</option>
                            <option value="Thursday">Thursday</option>
                        </select>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="row">
                    <label class="control-label col-md-3">Time</label>
                    <div class="col-md-3">
                        <div class="input-group bootstrap-timepicker">
                            <input type="text" name="time" value="00:00:00" class="form-control timepicker-24" readonly>
                            <span class="input-group-btn">
                                <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                            </span>

                        </div>
                        <span class="help-block">
                            Please click on the clock icon to change time
                        </span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn green">Add</button>

                    </div>
                </div>
            </div>
        </form>
        <br><br>
        <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_student_schedule_time/<?php echo $request_id; ?>/pending_request_details/">

            <div class="form-group">
                <div class="row">
                    <label class="control-label col-md-3">Remove a Schedule</label>
                    <div class="col-md-4">
                        <select class="form-control select2me" name="remove_schedule" data-placeholder="Select...">
                            <option value=""></option>
                            <?php foreach ($schedule as $row): ?>
                                <option value="<?php echo $row['id']; ?>"><?php echo $row['day'] . "( " . $row['time'] . " )"; ?></option>
                            <?php endforeach; ?>

                        </select>

                    </div>
                </div>
            </div>
            <br>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-4">
                        <button type="submit" class="btn green">Remove</button>
                    </div>
                </div>
            </div>
        </form>



    </div>
    <!-- END PAGE CONTENT-->
</div>
</div>
<!-- END CONTENT -->

<!-- END CONTAINER -->