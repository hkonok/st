
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Customer Service<small>statistics and more</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="index.html">Course</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Running Courses</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="fa fa-calendar"></i>
                            <span>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="clearfix">
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa  fa-ticket"></i>Running Courses
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="list-group">
                            <?php
                            if (isset($course_list)):
                                foreach ($course_list as $row):
                                    ?>
                                    <a href="<?php echo base_url(); ?>index.php/admin/class_view/<?php echo $row['id']; ?>" class="list-group-item">
                                        <?php 
                                            switch($row['course_id']){
                                                case 1:
                                                    $course_name = 'Arabic';
                                                    break;
                                                case 2:
                                                    $course_name = 'Quaran';
                                                    break;
                                                case 3:
                                                    $course_name = 'Arabic and Quran';
                                                    break;
                                                default :
                                                    $course_name = 'None';
                                            }
                                            
                                            switch($row['level']){
                                                case 1:
                                                    $course_name .= ' - Beginner';
                                                    break;
                                                case 6:
                                                    $course_name .= ' - Intermediate';
                                                    break;
                                                case 13:
                                                    $course_name .= ' - Advanced';
                                                    break;
                                                default :
                                                    $course_name .= ' - None';
                                            }
                                            
                                            echo $course_name;
                                        ?>
                                        <span class="badge badge-info">
                                            <?php
                                            $datetime = new DateTime($row['start_date']); // current time = server time
                                            
                                            echo  $datetime->format('d, M Y H:i:s');
                                            ?>
                                        </span>
                                    </a>
                                    <?php
                                endforeach;
                            endif;
                            ?>

                        </div>

                    </div>
                </div>

            </div>

           
        </div>
     
    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
