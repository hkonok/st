<div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">

                            <li class="<?php if($sub_menu_id == 1) echo 'active'; ?>">
                                <?php 
                                    $link_data = 'view_teacher';
                                    if(isset($user_basic) && $user_basic['user_type'] == 3)
                                        $link_data = 'edit_teacher_manager';
                                    if(isset($user_basic) && $user_basic['user_type'] == 2)
                                        $link_data = 'edit_technical_manager';
                                    if(isset($user_basic) && $user_basic['user_type'] == 5)
                                        $link_data = 'edit_customer_service';
                                ?>
                                <a  href="<?php echo base_url(); ?>index.php/admin/<?php echo $link_data; ?>/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-user"></i> Basic info </a>
                                <span class="after">
                                </span>
                            </li>
                            <?php if(isset($user_basic) && $user_basic['user_type'] < 5): ?>
                            <li class="<?php if($sub_menu_id == 2) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/user_items_received/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-check-square-o"></i> Items Received </a>
                                <span class="after">
                                </span>
                            </li>
                            <?php endif; ?>
                            
                            <?php if(isset($user_basic) && $user_basic['user_type'] < 5): ?>
                            <li class="<?php if($sub_menu_id == 3) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/papers_from_employees/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-pencil-square-o"></i> Paper from Employee </a>
                                <span class="after">
                                </span>
                            </li>
                            <?php endif; ?>
                            <li class="<?php if($sub_menu_id == 4) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/annual_vacation/<?php echo $teacher_id; ?>/">
                                    <i class="fa  fa-calendar"></i> Annual Vacation </a>
                                <span class="after">
                                </span>
                            </li>
                            
                            <?php if(isset($user_basic['user_type']) && $user_basic['user_type'] == 6): ?>
                            
                            <?php if($user_type == 3 || $user_type == 2 || $user_type == 1 || $user_type == 0): ?>
                            <li class="<?php if($sub_menu_id == 5) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_schedule_time/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-clock-o"></i> Schedule Time </a>
                                <span class="after">
                                </span>
                            </li>
                            <li class="<?php if($sub_menu_id == 6) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_quality_monitoring/<?php echo $teacher_id; ?>/">
                                    <i class="fa  fa-toggle-right"></i> Quality Monitoring </a>
                                <span class="after">
                                </span>
                            </li>
                            <li class="<?php if($sub_menu_id == 7) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_daily_update/<?php echo $teacher_id; ?>/">
                                    <i class="fa  fa-copy"></i> Daily Update </a>
                                <span class="after">
                                </span>
                            </li>
                            <?php endif; ?>
                            
                            <?php if($user_type == 2 || $user_type == 1 || $user_type == 0): ?>
                            <li class="<?php if($sub_menu_id == 8) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_technical_info/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-info-circle"></i>Technical Info</a>
                                <span class="after">
                                </span>
                            </li>
                            <li class="<?php if($sub_menu_id == 9) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_monitoring_class/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-eye"></i>Monitoring Class</a>
                                <span class="after">
                                </span>
                            </li>
                            <li class="<?php if($sub_menu_id == 10) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_obligation_to_the_managerial_order/<?php echo $teacher_id; ?>/">
                                    <i class="fa fa-user"></i>Obligation to Managerial Order</a>
                                <span class="after">
                                </span>
                            </li>
                            <?php endif; ?>
                            
                            <?php endif; ?>
                            
                            <li class="<?php if(isset($sub_menu_id) && $sub_menu_id == 11) echo 'active'; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/teacher_feedback_rating/<?php echo $teacher_id; ?>/">
                                    <i class="fa  fa-thumbs-o-up"></i>Rating and Feedbacks</a>
                                <span class="after">
                                </span>
                            </li>

                        </ul>
                    </div>