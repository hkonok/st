
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Customer Service<small>assign class</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-exchange"></i>
                        <a href="#">Assign Class</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                  
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="fa fa-calendar"></i>
                            <span>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="clearfix">
        </div>

        <div class="row">
            <div class="col-md-9">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa  fa-ticket"></i>Pending Requests
                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="list-group">
                            <?php
                            if (isset($request_list)):
                                foreach ($request_list as $row):
                                    ?>
                                    <a href="<?php echo base_url(); ?>index.php/admin/assign_class_details/<?php echo $row['request_id']; ?>" class="list-group-item">
                                        <?php 
                                            if($row['course_id'] == 1)
                                                echo "Arabic";
                                            else if($row['course_id'] == 2)
                                                echo "Quran";
                                            else 
                                                echo "Arabic and Quran";
                                            
                                            if($row['level'] == 1)
                                                echo " ( Beginner ) ";
                                            else if($row['level'] == 6)
                                                echo " ( Intermediate ) ";
                                            else 
                                                echo " ( Advanced ) ";
                                        ?>
                                        <span class="badge badge-info">
                                            <?php
                                            $datetime = new DateTime($row['request_datetime']); // current time = server time
                                            
                                            echo  $datetime->format('d, M Y H:m:i');
                                            ?>
                                        </span>
                                    </a>
                                    <?php
                                endforeach;
                            endif;
                            ?>

                        </div>

                    </div>
                </div>

            </div>

            <!-- END PAGINATION PORTLET-->
        </div>
    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
