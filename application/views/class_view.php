
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <?php
        $course_name = "";
        switch ($class_data['course_id']) {
            case 1:
                $course_name = "Arabic";
                break;
            case 2:
                $course_name = "Quaran";
                break;
            case 3:
                $course_name = "Arabic and Quaran";
                break;
            default :
                $course_name = "None";
        }

        $course_level = "";
        switch ($class_data['level']) {
            case 1:
                $course_level = "Beginner";
                break;
            case 6:
                $course_level = "Intermediate";
                break;
            case 13:
                $course_level = "Advanced";
                break;
            default :
                $course_level = "None";
        }
        ?>
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <b> <?php echo $course_name; ?> </b><small><b> Level: <?php echo $course_level; ?></b></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <?php
                    if (isset($user_type) && $user_type == 6):
                        if ($class_data['status'] == 1) {
                            $course_status_name = "Running Classes";
                            $class_list_link = "running_classes";
                        } else {
                            $course_status_name = "Completed Classes";
                            $class_list_link = "completed_classes";
                        }
                        ?>

                        <li>
                            <i class="fa fa-home"></i>
                            <a href="<?php echo base_url(); ?>index.php/admin/<?php echo $class_list_link; ?>/"><?php echo $course_status_name; ?></a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    <?php endif; ?>

                    <?php if (isset($user_type) && $user_type == 7): ?>
                        <li>
                            <i class="fa fa-home"></i>
                            <a href="#">Course</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <?php
                            if ($class_data['status'] == 1) {
                                $course_status = 'running_course';
                                $course_status_name = 'Running Course';
                            } else {
                                $course_status = 'completted_course';
                                $course_status_name = 'Completted Course';
                            }
                            ?>
                            <a href="<?php echo base_url(); ?>index.php/admin/<?php echo $course_status; ?>"><?php echo $course_status_name; ?></a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    <?php endif; ?>
                    <li>
                        <a href="#">Class</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">

                            <li class="<?php if ($tabname == "") echo "active"; ?>">
                                <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-calendar"></i> Class Details </a>
                                <span class="after">
                                </span>
                            </li>


                            <li class="<?php if ($tabname == "message") echo "active"; ?>">
                                <a data-toggle="tab" href="#tab_3-3"><i class="fa fa-inbox"></i> Messages</a>
                            </li>
                            <li class="<?php if ($tabname == "files") echo "active"; ?>">
                                <a data-toggle="tab" href="#tab_4-4">
                                    <i class="fa fa-file"></i> Files </a>
                                <span class="after">
                                </span>
                            </li>
                            <li class="<?php if ($tabname == "grades") echo "active"; ?>">
                                <a data-toggle="tab" href="#tab_5-5">
                                    <i class="fa fa-bar-chart-o"></i> Grades</a>
                                <span class="after">
                                </span>
                            </li>
                            <?php if (isset($class_data['status']) && $class_data['status'] == 2 && $user_type == 7): ?>
                                <li class="<?php if ($tabname == "rate_teacher") echo "active"; ?>">
                                    <a data-toggle="tab" href="#tab_6-6">
                                        <i class="fa fa-bar-chart-o"></i> Rate Teacher</a>
                                    <span class="after">
                                    </span>
                                </li>
                            <?php endif; ?>
                            <li class="<?php if ($tabname == "attendance") echo "active"; ?>">
                                <a data-toggle="tab" href="#tab_7-7">
                                    <i class="fa fa-calendar"></i> Attendance</a>
                                <span class="after">
                                </span>
                            </li>

                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane <?php if ($tabname == "") echo "active"; ?>">

                                <div class="form-group">
                                    <label class="control-label">Teacher</label>
                                    <div class="well" style="font-family:cambria; color: #58595A; font-size: 15px;">
                                        <div class=" row-fluid">
                                            <?php echo $teacher_details['first_name'] . " " . $teacher_details['last_name']; ?>
                                        </div>
                                        <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>
                                            
                                        <?php if(!isset($user_type) || $user_type!=6): ?>
                                        <hr style="margin: 5px;">
                                        <div class=" row-fluid">
                                            Contact Teacher
                                            <div id="SkypeButton_Call_<?php echo $teacher_details['skype_id']; ?>_1" style=" padding: 0px; margin: 0px; border: 2px;">

                                                <script type="text/javascript">
                                                    Skype.ui({
                                                        "name": "chat",
                                                        "element": "SkypeButton_Call_<?php echo $teacher_details['skype_id']; ?>_1",
                                                        "participants": ["<?php echo $teacher_details['skype_id']; ?>"],
                                                        "imageSize": 32
                                                    });
                                                </script>
                                            </div>

                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label">Students</label>
                                    <div class="well" style="font-family:cambria; color: #58595A; font-size: 15px;">
                                        <?php
                                        $line_num = 0;
                                        foreach ($class_students as $row2): $row = $student_details[$row2['student_id']];
                                            ?>
                                            <?php if ($line_num > 0): ?>
                                                <hr style="margin: 5px;">
                                            <?php endif; ?>
                                            <?php
                                            $line_num++;
                                            echo $row['first_name'] . " " . $row['last_name'];
                                            ?>  
                                             <?php if(isset($user_type) && $user_type == 6): ?>
                                            <div id="SkypeButton_Call_<?php echo $row['skype_id']; ?>_1" style=" padding: 0px; margin: 0px; border: 2px; height: 50px;">

                                                <script type="text/javascript">
                                                    Skype.ui({
                                                        "name": "chat",
                                                        "element": "SkypeButton_Call_<?php echo $row['skype_id']; ?>_1",
                                                        "participants": ["<?php echo $row['skype_id']; ?>"],
                                                        "imageSize": 24
                                                    });
                                                </script>
                                            </div>
                                                <?php endif; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <script type="text/javascript" src="http://www.skypeassets.com/i/scom/js/skype-uri.js"></script>

                                <div class="form-group">
                                    <label class="control-label">Study Hours</label>
                                    <div class="well" style="font-family:cambria; color: #58595A; font-size: 15px; padding-top: 2px; padding-bottom: 2px;">
                                        <?php
                                        $study_hours = array('1' => 'Light', '2' => 'Standard', '3' => 'Intensive');
                                        echo $study_hours[$class_data['study_hours']];
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Start Date</label>
                                    <div class="well" style="font-family:cambria; color: #58595A; font-size: 15px; padding-top: 2px; padding-bottom: 2px;">

                                        <?php
                                        $tmp_date = new DateTime($class_data['start_date']);
                                        echo date_format($tmp_date, "M d Y");
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">End Date</label>
                                    <div class="well" style="font-family:cambria; color: #58595A; font-size: 15px; padding-top: 2px; padding-bottom: 2px;">

                                        <?php
                                        $tmp_date = new DateTime($class_data['end_date']);
                                        echo date_format($tmp_date, "M d Y");
                                        ?>
                                    </div>
                                </div>
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-inbox"></i>Class Schedule
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            Day
                                                        </th>
                                                        <th>
                                                            Time
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($class_schedule as $row): $i++;
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $i; ?>
                                                            </td>
                                                            <td class="success">
                                                                <?php echo $row['day']; ?>
                                                            </td>
                                                            <td class="warning">
                                                                <?php echo $row['time']; ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->

                            </div>
                            <div id="tab_3-3" class="tab-pane <?php if ($tabname == "message") echo "active"; ?>">


                                <div class="col-md-8">
                                    <!-- BEGIN PORTLET-->
                                    <div class="portlet">

                                        <div class="portlet-body" id="chats">
                                            <div class="scroller" style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
                                                <ul class="chats">
                                                    <?php
                                                    if (isset($class_message)):

                                                        foreach ($class_message as $row):
                                                            ?>
                                                            <li class="<?php
                                                            if ($row['from'] != $class_data['teacher_id'])
                                                                echo 'in';
                                                            else
                                                                echo 'out';
                                                            ?>">
                                                                    <?php
                                                                    if ($row['from'] != $class_data['teacher_id'])
                                                                        $pic_name = 'student.png';
                                                                    else
                                                                        $pic_name = 'teacher.png';
                                                                    ?>
                                                                <img class="avatar img-responsive" alt="" src="<?php echo base_url(); ?>assets/user/<?php echo $pic_name; ?>"/>
                                                                <div class="message">
                                                                    <span class="arrow">
                                                                    </span>
                                                                    <a href="#" class="name"><?php
                                                                        if ($row['from'] != $class_data['teacher_id'])
                                                                            echo $student_details[$row['from']]['first_name'] . " " . $student_details[$row['from']]['last_name'];
                                                                        else
                                                                            echo $teacher_details['first_name'] . " " . $teacher_details['last_name'];
                                                                        ?>
                                                                    </a>
                                                                    <span class="datetime">
                                                                        at <?php
                                                                        $datetime = new DateTime($row['datetime']); // current time = server time

                                                                        echo $datetime->format('d, M Y H:i:s');
                                                                        ?>
                                                                    </span>
                                                                    <span class="body">
                                                                        <?php
                                                                           
                                                                           echo preg_replace("([_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4}))", "****@***.com" ,$row['message']);
                                                                            
                                                                        ?>
                                                                    </span>
                                                                </div>
                                                            </li>

                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </ul>
                                            </div>
                                            <?php if ($class_data['status'] == '1'): ?>
                                                <form action="" method="post">
                                                    <div class="chat-form">
                                                        <div class="input-cont">
                                                            <textarea class="form-control" type="text" name="message" placeholder="Type a message here..."/> </textarea>
                                                        </div>
                                                    </div>
                                                    <div class="btn-cont" style=" margin-top: 2px;">
                                                        <span class="arrow">
                                                        </span>
                                                        <button type="submit" class="btn blue icn-only">Send</button>
                                                    </div>
                                                </form>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                    <!-- END PORTLET-->
                                </div>

                            </div>

                            <div id="tab_4-4" class="tab-pane <?php if ($tabname == "files") echo "active"; ?>">

                                <div class="portlet box green  col-md-9" style="margin-left: 10px;">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-clock-o"></i>Files
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            Name
                                                        </th>
                                                        <th>
                                                            Date
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($class_file as $row): $i++;
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $i; ?>
                                                            </td>
                                                            <td class="success">
                                                                <a href="<?php echo base_url(); ?>class_files/<?php echo $row['file_url']; ?>" target="_blank"><?php echo $row['file_name']; ?></a>
                                                            </td>
                                                            <td class="success">
                                                                <?php echo $row['datetime'] ?>
                                                            </td>

                                                        </tr>
                                                    <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <?php if (isset($user_type) && $user_type == 6): ?>   

                                    <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/class_upload_file/<?php echo $class_id; ?>/" enctype="multipart/form-data">


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Choose File</label>
                                                <div class="col-md-4">
                                                    <input type="file" name="choose_file" accept="application/pdf">
                                                </div>
                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn green">Upload</button>

                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                <?php endif; ?>

                            </div>

                            <div id="tab_5-5" class="tab-pane <?php if ($tabname == "grades") echo "active"; ?>">
                                <?php
                                if (isset($class_students)):
                                    foreach ($class_students as $row):
                                        if ($user_type == 7 && $user_id != $row['student_id'])
                                            continue;
                                        ?>
                                        <div class="row">
                                            <div class="portlet box green  col-md-9" style="margin-left: 10px;">
                                                <div class="portlet-title">
                                                    <div class="caption">
                                                        <i class="fa fa-clock-o"></i><?php echo $student_details[$row['student_id']]['first_name'] . " " . $student_details[$row['student_id']]['last_name']; ?> - Grades
                                                    </div>

                                                </div>
                                                <div class="portlet-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th>
                                                                        #
                                                                    </th>
                                                                    <th>
                                                                        Topic
                                                                    </th>
                                                                    <th>
                                                                        Date
                                                                    </th>
                                                                    <th>
                                                                        Marks
                                                                    </th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php
                                                                $i = 0;
                                                                foreach ($quiz_marks[$row['student_id']] as $row2): $i++;
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $i; ?>
                                                                        </td>
                                                                        <td class="success">
                                                                            <?php echo $row2['topic']; ?>
                                                                        </td>
                                                                        <td class="success">
                                                                            <?php echo $row2['date'] ?>
                                                                        </td>
                                                                        <td class="warning">
                                                                            <?php echo $row2['obtain_marks'] ?> / <?php echo $row2['total_marks'] ?>
                                                                        </td>

                                                                    </tr>
                                                                <?php endforeach; ?>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div><!-- end row-->
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                <?php if (isset($user_type) && $user_type == 6): ?>      
                                    <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/clss_view_update_marks/<?php echo $class_id; ?>/">
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Select Student</label>
                                                <div class="col-md-4">
                                                    <select class="form-control select2me" name="student" data-placeholder="Select...">
                                                        <option value=""></option>
                                                        <?php
                                                        foreach ($class_students as $row):
                                                            ?>
                                                            <option value="<?php echo $row['student_id']; ?>"><?php echo $student_details[$row['student_id']]['first_name'] . " " . $student_details[$row['student_id']]['last_name']; ?></option>
                                                        <?php endforeach; ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Topic Name</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="topic" class="form-control textfield" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Total Marks</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="total_marks"  class="form-control textfield" >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Obtained Marks</label>
                                                <div class="col-md-4">
                                                    <input type="text" name="obtain_marks"  class="form-control textfield" >
                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Date</label>
                                                <div class="col-md-3">
                                                    <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                        <input name="start_date" type="text" class="form-control" readonly>
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                        </span>
                                                    </div>
                                                    <!-- /input-group -->

                                                </div>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn green">Add</button>

                                                </div>
                                            </div>
                                        </div>
                                    </form>


                                    <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_quiz_marks/<?php echo $class_id; ?>/">



                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Remove a quiz marks</label>
                                                <div class="col-md-4">
                                                    <select class="form-control select2me" name="remove_schedule" data-placeholder="Select...">
                                                        <option value=""></option>
                                                        <?php
                                                        if (isset($class_students)):
                                                            foreach ($class_students as $row):
                                                                foreach ($quiz_marks[$row['student_id']] as $row2):
                                                                    ?>
                                                                    <option value="<?php echo $row2['id']; ?>"> <?php echo $student_details[$row['student_id']]['first_name'] . " " . $student_details[$row['student_id']]['last_name']; ?> - ( <?php echo $row2['date']; ?> ) </option>
                                                                    <?php
                                                                endforeach;
                                                            endforeach;
                                                        endif;
                                                        ?>


                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn green">Remove</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                <?php endif; ?>


                            </div>
                            <?php if (isset($teacher_rating['teacher_id']) && $class_data['status'] == 2): ?>
                                <div id="tab_6-6" class="tab-pane <?php if ($tabname == "rating") echo "active"; ?>">
                                    <form role="form" class="from-horizontal" method="post" action="#">


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Rating</label>

                                                <div class="col-md-8">
                                                    <div id="star"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Feedback</label>
                                                <div class="col-md-9 blog-page">

                                                    <div class="media">

                                                        <div class="media-body">
                                                            <h6 class="media-heading">
                                                                <span>
                                                                    <?php
                                                                    $datetime = new DateTime($teacher_rating['datetime']);

                                                                    echo date_format($datetime, "M d, y");
                                                                    ?>
                                                                </span>
                                                            </h6>
                                                            <p>
                                                                <?php echo $teacher_rating['feedback']; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br>

                                    </form>
                                </div>
                            <?php elseif (!isset($teacher_rating['teacher_id']) && $class_data['status'] == 2): ?>
                                <div id="tab_6-6" class="tab-pane">
                                    <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/rate_teacher/<?php echo $class_id; ?>">


                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Give Rating*</label>


                                                <div class="col-md-8">
                                                    <div id="star"></div>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <label class="control-label col-md-3">Give Feedback*</label>
                                                <div class="col-md-9">
                                                    <textarea class="form-control" name="feedback" rows="5"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-3"></div>
                                                <div class="col-md-4">
                                                    <button type="submit" class="btn green">Submit</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            <?php endif; ?>

                            <div id="tab_7-7" class="tab-pane <?php if ($tabname == "attendance") echo "active"; ?>">
                                <!-- BEGIN ACCORDION PORTLET-->
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            </i>Attendance List
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="panel-group accordion scrollable" id="accordion2">

                                            <?php
                                            if (isset($class_students)):
                                                $i = 0;
                                                foreach ($class_students as $row):
                                                    if ($user_type == 7 && $row['student_id'] != $user_id)
                                                        continue;
                                                    $i++;
                                                    ?>
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">
                                                            <h4 class="panel-title">
                                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_<?php echo $i; ?>">
                                                                    <?php echo $student_details[$row['student_id']]['first_name'] . " " . $student_details[$row['student_id']]['last_name']; ?></a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapse_2_<?php echo $i; ?>" class="panel-collapse <?php
                                                        if ($i == 1)
                                                            echo "in";
                                                        else
                                                            echo "collapse";
                                                        ?>">
                                                            <div class="panel-body">
                                                                <!-- BEGIN SAMPLE TABLE PORTLET-->

                                                                <div class="table-responsive">
                                                                    <table class="table table-bordered table-hover">
                                                                        <thead>
                                                                            <tr>
                                                                                <th>
                                                                                    #
                                                                                </th>
                                                                                <th>
                                                                                    Date Time
                                                                                </th>
                                                                                <th>
                                                                                    Attendance
                                                                                </th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            <?php
                                                                            $j = 0;
                                                                            foreach ($student_attendance[$row['student_id']] as $attendance):
                                                                                $j++;
                                                                                $datetime = new DateTime($attendance['datetime']);
                                                                                ?>
                                                                                <tr>
                                                                                    <td>
                                                                                        <?php echo $j; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php
                                                                                        echo date_format($datetime, "M d, Y");
                                                                                        $hmtl_class_name = array('1' => "success", '0' => "danger");
                                                                                        ?>
                                                                                    </td>
                                                                                    <td class="<?php echo $hmtl_class_name[$attendance['attendance']]; ?>">
                                                                                        <?php
                                                                                        $tmp_data = array('1' => "Present", '0' => "Absent");
                                                                                        echo $tmp_data[$attendance['attendance']];
                                                                                        ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <?php
                                                                            endforeach;
                                                                            ?>
                                                                        </tbody>
                                                                    </table>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>

                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>


                                            <!-- END SAMPLE TABLE PORTLET-->


                                            <?php if (isset($user_type) && $user_type == 6): ?>

                                                <br><br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <ul class="page-breadcrumb breadcrumb">
                                                            <li class="caption">
                                                                Give Attendance
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>


                                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/give_student_attendance/<?php echo $class_id; ?>/">
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3">Select Date</label>
                                                            <div class="col-md-4">
                                                                <select class="form-control select2me" name="datetime" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                    <?php
                                                                    if (isset($student_attendance_dates)):
                                                                        foreach ($student_attendance_dates as $row):
                                                                            $st_datetime = $row['datetime'];
                                                                            ?>
                                                                            <option value="<?php echo date_format($st_datetime, "Y-m-d") . " " . $row['class_time']; ?>"><?php echo date_format($st_datetime, "M d, Y") . " " . $row['class_time']; ?></option>
                                                                            <?php
                                                                        endforeach;
                                                                    endif;
                                                                    ?>

                                                                </select>

                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <?php foreach ($class_students as $students): ?>
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label class="control-label col-md-3"><?php echo $student_details[$students['student_id']]['first_name'] . " " . $student_details[$students['student_id']]['last_name']; ?></label>

                                                                <div class="col-md-4">
                                                                    <select class="form-control select2me" name="attendance<?php echo $students['student_id']; ?>" data-placeholder="Select...">
                                                                        <option value=""></option>
                                                                        <option value="1">Present</option>
                                                                        <option value="0">Absent</option>
                                                                    </select>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <br>
                                                    <?php endforeach; ?>

                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-4">
                                                                <button type="submit" class="btn green">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div>
                                <!-- END ACCORDION PORTLET-->
                            </div>

                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
