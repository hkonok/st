
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name']." ".$user_data['last_name']; ?> <small>Teacher Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#">Teacher Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form role="form" class="from-horizontal" method="post" action="">
                                    <div class="form-group">
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="first_name" placeholder="Frist Name" value="<?php echo $user_data['first_name']; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name="last_name" placeholder="Last Name" value="<?php echo $user_data['last_name']; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email Address</label>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input type="email" name="email" placeholder="Email Address" value="<?php echo $user_basic['email']; ?>" name="email" class="form-control" >
                                        </div>

                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="control-label">Skype ID</label>
                                        <input type="text" name="skype_id" placeholder="Skype ID" value="<?php echo $user_data['skype_id']; ?>" class="form-control"/>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="control-label">Date of Birth</label>
                                        <input class="form-control date-picker" name="birthday" data-date-format="yyyy-mm-dd"  type="text" value="<?php echo $user_data['birthday'] ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Select Timezone</label>
                                        <div >
                                            <select class="form-control select2me" name="time_zone"  data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="-12.0" <?php if($user_data['time_zone']== -12.0) echo 'selected'; ?>>(GMT -12:00) Eniwetok, Kwajalein</option>
                                                <option value="-11.0" <?php if($user_data['time_zone']== -11.0) echo 'selected'; ?>>(GMT -11:00) Midway Island, Samoa</option>
                                                <option value="-10.0" <?php if($user_data['time_zone']== -10.0) echo 'selected'; ?>>(GMT -10:00) Hawaii</option>
                                                <option value="-9.0" <?php if($user_data['time_zone']== -9.0) echo 'selected'; ?>>(GMT -9:00) Alaska</option>
                                                <option value="-8.0" <?php if($user_data['time_zone']== -8.0) echo 'selected'; ?>>(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                                                <option value="-7.0" <?php if($user_data['time_zone']== -7.0) echo 'selected'; ?>>(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                                                <option value="-6.0" <?php if($user_data['time_zone']== -6.0) echo 'selected'; ?>>(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                                                <option value="-5.0" <?php if($user_data['time_zone']== -5.0) echo 'selected'; ?>>(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                                                <option value="-4.0" <?php if($user_data['time_zone']== -4.0) echo 'selected'; ?>>(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                                                <option value="-3.5" <?php if($user_data['time_zone']== -3.5) echo 'selected'; ?>>(GMT -3:30) Newfoundland</option>
                                                <option value="-3.0" <?php if($user_data['time_zone']== -3.0) echo 'selected'; ?>>(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                                                <option value="-2.0" <?php if($user_data['time_zone']== -2.0) echo 'selected'; ?>>(GMT -2:00) Mid-Atlantic</option>
                                                <option value="-1.0" <?php if($user_data['time_zone']== -1.0) echo 'selected'; ?>>(GMT -1:00 hour) Azores, Cape Verde Islands</option>
                                                <option value="0.0" <?php if($user_data['time_zone']== -0.0) echo 'selected'; ?>>(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                                                <option value="1.0" <?php if($user_data['time_zone']== 1.0) echo 'selected'; ?>>(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
                                                <option value="2.0" <?php if($user_data['time_zone']== 2.0) echo 'selected'; ?>>(GMT +2:00) Kaliningrad, South Africa</option>
                                                <option value="3.0" <?php if($user_data['time_zone']== 3.0) echo 'selected'; ?>>(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                                                <option value="3.5" <?php if($user_data['time_zone']== 3.5) echo 'selected'; ?>>(GMT +3:30) Tehran</option>
                                                <option value="4.0" <?php if($user_data['time_zone']== 4.0) echo 'selected'; ?>>(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                                                <option value="4.5" <?php if($user_data['time_zone']== 4.5) echo 'selected'; ?>>(GMT +4:30) Kabul</option>
                                                <option value="5.0" <?php if($user_data['time_zone']== 5.0) echo 'selected'; ?>>(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                                                <option value="5.5" <?php if($user_data['time_zone']== 5.5) echo 'selected'; ?>>(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                                <option value="5.75" <?php if($user_data['time_zone']== 5.75) echo 'selected'; ?>>(GMT +5:45) Kathmandu</option>
                                                <option value="6.0" <?php if($user_data['time_zone']== 6.0) echo 'selected'; ?>>(GMT +6:00) Almaty, Dhaka, Colombo</option>
                                                <option value="7.0" <?php if($user_data['time_zone']== 7.0) echo 'selected'; ?>>(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                                                <option value="8.0" <?php if($user_data['time_zone']== 8.0) echo 'selected'; ?>>(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                                                <option value="9.0" <?php if($user_data['time_zone']== 9.0) echo 'selected'; ?>>(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                                                <option value="9.5" <?php if($user_data['time_zone']== 9.5) echo 'selected'; ?>>(GMT +9:30) Adelaide, Darwin</option>
                                                <option value="10.0" <?php if($user_data['time_zone']== 10.0) echo 'selected'; ?>>(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                                                <option value="11.0" <?php if($user_data['time_zone']== 11.0) echo 'selected'; ?>>(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                                                <option value="12.0" <?php if($user_data['time_zone']== 12.0) echo 'selected'; ?>>(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>

                                            </select>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Date of Contract Signing</label>
                                        <input class="form-control date-picker" name="date_of_contract"  data-date-format="yyyy-mm-dd" type="text" value="<?php echo $user_data['date_of_contract']; ?>"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Bank Account Number</label>
                                        <input class="form-control"  type="text" name="bank_account" value="<?php echo $user_data['bank_account']; ?>"/>
                                    </div>



                                    <?php if(isset($user_type) && $user_type != '5'): ?>
                                    <div class="margiv-top-10">
                                        <button type="submit" class="btn green">Save Changes</button>
                                        <a href="#" class="btn default">Cancel</a>
                                    </div>
                                    <?php endif; ?>
                                </form>
                                
                                
                                <?php if (isset($user_type) && ($user_type == 1 || $user_type == 0)): ?>
                                <br><br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5 class="page-title">
                                                Change Password
                                            </h5>
                                        </div>
                                    </div>
                                    <?php if (isset($message) && $message == '1'): ?>
                                        <div class="alert alert-success">
                                            Password has been changed successfully
                                        </div>
                                    <?php endif; ?>

                                    <form action="<?php echo base_url(); ?>index.php/admin/change_password/<?php echo $user_basic['id']; ?>/view_teacher/" class="form-horizontal" method="post">
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">New Password</label>
                                                <div class="col-md-4">
                                                    <input type="password" name="password" class="form-control" placeholder="Enter New Password Here">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="col-md-3 control-label">Retype Password</label>
                                                <div class="col-md-4">
                                                    <input type="password" name="password2" class="form-control" placeholder="Enter New Password Here">
                                                </div>
                                            </div>


                                            <div class="form-actions fluid">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn blue">Change</button>                        
                                                </div>
                                            </div>

                                        </div>
                                    </form>

                                <?php endif; ?>
                                
                                
                                
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>