<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Promotional Offers
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa  fa-mail-forward"></i>
                        <a href="=#">Promotional Offers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->


        <div class="row" >
            <div class="col-md-12">

                <div class="inbox-content">

                    <table class="table table-striped table-advance table-hover">
                        <thead>
                        
                        <div class="table-toolbar">
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <a href="<?php echo base_url(); ?>index.php/admin/new_promotional_offer/" class="btn green">New Promotional Offer <i class="fa fa-plus"></i></a>
                            </div>
                        </div>
                        
                        </thead>
                        <tbody>

                            <?php $i = 0;
                            foreach ($promotional_offers as $row): $i++;
                                ?>

                                <tr class="">

                                    <td class="inbox-small-cells">
                                        <input type="checkbox" class="mail-checkbox">
                                    </td>

                                    <td class="view-message hidden-xs">
    <?php echo $user_name[$row['id']]['first_name'] . " " . $user_name[$row['id']]['last_name']; ?> 
                                    </td>
                                    <td class="view-message ">
                                        <a href="<?php echo base_url(); ?>index.php/admin/view_promotional_offer/<?php echo $row['id']; ?>/"> <?php echo $row['subject']; ?></a>
                                    </td>
                                    <td class="view-message inbox-small-cells">
                                        <i class="fa fa-paper-clip"></i>
                                    </td>
                                    <td class="view-message text-right">
                                        <?php
                                        $datetime = new DateTime($row['datetime']);
                                        echo date_format($datetime, "M d, Y h:m");
                                        ?> 
                                    </td>
                                </tr>
<?php endforeach; ?>

                        </tbody>
                    </table>
                </div>

            </div>
        </div>



    </div>