<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
      

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Users <small>Edit Manager Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-group"></i>
                        <a href="index.html">Users</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Managers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Edit</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="alert alert-error">
                <?php echo validation_errors(); ?>
                <?php
                if (isset($record)) {
                    foreach ($record as $row) {
                        ?>
                    </div>
                    <div class="">



                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url(); ?>index.php/admin/edit_manager/<?php echo $row->id; ?>" class="form-horizontal" method="post">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Firstname</label>
                                    <div class="col-md-4">
                                        <input type="text" name="firstname" class="form-control" value="<?php echo $row->first_name; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Lastname</label>
                                    <div class="col-md-4">
                                        <input type="text" name="lastname" class="form-control" value="<?php echo $row->last_name; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Email Address</label>
                                    <div class="col-md-4">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input type="email" name="email" class="form-control" value="<?php echo $row->email; ?>">
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn blue">Save</button>
                                    <button type="button" class="btn default">Back to List</button>
                                </div>
                            </div>



                        </form>

                    <?php }
                } ?>
                <!-- END FORM-->
            </div>

            <div class="row">
                <div class="col-md-12">
                    <h5 class="page-title">
                        Change Password
                    </h5>
                </div>
            </div>
            <?php if(isset($message) && $message == '1'): ?>
            <div class="alert alert-success">
                Password has been changed successfully
            </div>
            <?php endif; ?>
            <form action="<?php echo base_url(); ?>index.php/admin/change_password/<?php echo $row->id; ?>/edit_manager/" class="form-horizontal" method="post">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">New Password</label>
                        <div class="col-md-4">
                            <input type="password" name="password" class="form-control" placeholder="Enter New Password Here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Retype Password</label>
                        <div class="col-md-4">
                            <input type="password" name="password2" class="form-control" placeholder="Enter New Password Here">
                        </div>
                    </div>


                    <div class="form-actions fluid">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn blue">Change</button>                        
                        </div>
                    </div>

                </div>

            </form>            



        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->

<!-- END CONTAINER -->