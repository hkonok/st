<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Manage Class
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <?php if (!isset($class_data['status']) || $class_data['status'] == 0): ?>
                            <i class="fa fa-exchange"></i>
                            <a href="<?php echo base_url(); ?>index.php/admin/assign_class/">Assign Class</a>
                            <i class="fa fa-angle-right"></i>
                        <?php elseif($class_data['status'] == 1): ?>
                            <i class="fa fa-check"></i>
                            <a href="<?php echo base_url(); ?>index.php/admin/running_class/">Running Class</a>
                            <i class="fa fa-angle-right"></i>
                        
                        <?php else: ?>
                            <i class="fa fa-power-off"></i>
                            <a href="<?php echo base_url(); ?>index.php/admin/completed_class/">Completed Class</a>
                            <i class="fa fa-angle-right"></i>
                        <?php endif; ?>
                    </li>
                    <li>
                        <a href="#">Manage Class</a>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

            <?php
            if (isset($message) && $message == 1):
                ?>
                <div class="alert alert-success">
                    This class has been activated <b>successfully</b>.
                </div>
            <?php endif; ?>
                <?php
            if (isset($message) && $message == 0):
                ?>       
                <div class="alert alert-danger">
                    Please check <b>Class Details</b> and try again to activate.
                </div>  

            <?php endif; ?>
            <div class="col-md-12"></div>
        </div>

   
    <?php if (!isset($class_data['status']) || $class_data['status'] == 0): ?>
        <div class="table-toolbar">
            <div class="col-md-12" style="margin-bottom: 10px;">
                <a href="<?php echo base_url(); ?>index.php/admin/activate_class/<?php echo $class_id; ?>/assign_class_details/" class="btn green">Activate Class <i class="fa  fa-check"></i></a>
            </div>

        </div>
    <?php endif; ?>
    <?php if (!isset($class_data['status']) || $class_data['status'] == 1): ?>
        <div class="table-toolbar">
            <div class="col-md-12" style="margin-bottom: 10px;">
                <a href="<?php echo base_url(); ?>index.php/admin/class_mark_as_complete/<?php echo $class_id; ?>/assign_class_details/" class="btn red">Mark as Compete <i class="fa  fa-power-off"></i></a>
            </div>

        </div>
    <?php endif; ?>
        
        <?php if (isset($class_data['status']) && $class_data['status'] == 2): ?>
            <div class="table-toolbar">
                
                <form action="<?php echo base_url(); ?>index.php/admin/reactivate_class/<?php echo $class_id; ?>" method="post">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                 <div class="form-group">
                                        <label class="control-label">Change end date *</label>
                                        <div class="">
                                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                <input name="end_date" type="text" class="form-control" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                    </div>
            <div class="col-md-12" style="margin-bottom: 10px;">
                <button class="btn green">Reactivate Class <i class="fa  fa-check-circle"></i></button>
            </div>
                </form>

        </div>
        <?php endif; ?>

    <hr>


    <div class="row">
        <div class="col-md-12">

            <ul class="page-breadcrumb breadcrumb">
                <li class="caption">
                    Class Details
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-bookmark-o"></i>Request From User
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">

                            <tbody>

                                <tr>
                                    <td>
                                        Crouse
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['course_id'])) {
                                            switch ($request_data['course_id']) {
                                                case 1:
                                                    echo "Arabic";
                                                    break;
                                                case 2:
                                                    echo "Quran";
                                                    break;
                                                case 3:
                                                    echo "Arabic and Quran";
                                                    break;
                                            }
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Crouse Level
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['level'])) {
                                            switch ($request_data['level']) {
                                                case 1:
                                                    echo "Beginner";
                                                    break;
                                                case 6:
                                                    echo "Intermediate";
                                                    break;
                                                case 13:
                                                    echo "Advanced";
                                                    break;
                                            }
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            Requested Schedule
                                        </b>
                                    </td>
                                    <td class="danger">
                                        <b>
                                            <?php
                                            if (isset($requested_schedule)) {
                                                foreach ($requested_schedule as $row) {
                                                    echo $row['day'] . " - " . $row['time'] . "<br>";
                                                }
                                            } else {

                                                echo "None";
                                            }
                                            ?>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Skill in Teacher
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['skills_in_teacher'])) {
                                            switch ($request_data['skills_in_teacher']) {
                                                case 1:
                                                    echo "Good English";
                                                    break;
                                                case 2:
                                                    echo "Nice";
                                                    break;
                                                case 3:
                                                    echo "Good With Kids";
                                                    break;
                                            }
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Study Hours
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['study_hours'])) {
                                            switch ($request_data['study_hours']) {
                                                case 1:
                                                    echo "Light";
                                                    break;
                                                case 2:
                                                    echo "Standard";
                                                    break;
                                                case 3:
                                                    echo "Intensive";
                                                    break;
                                            }
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Start Date
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['start_date'])) {
                                            echo $request_data['start_date'];
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        End Date
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['end_date']) && $request_data['end_date']!= '0000-00-00') {
                                            echo $request_data['end_date'];
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Request Datetime
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($request_data['request_datetime'])) {
                                            echo $request_data['request_datetime'];
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>
                                </tr>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <div class="portlet box green">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-check-circle"></i>Class Details
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">

                            <tbody>

                                <tr>
                                    <td>
                                        Teacher
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($teacher_details)) {
                                            echo $teacher_details['first_name'] . " " . $teacher_details['last_name'] . " ( " . $teacher_basic['email'] . " )";
                                        } else {
                                            echo "None";
                                        }
                                        ?>
                                    </td>


                                </tr>
                                <tr>
                                    <td>
                                        Student
                                    </td>
                                    <td class="success">
                                        <?php
                                        if (isset($student_details)) {
                                            echo $student_details['first_name'] . " " . $student_details['last_name'] . " ( " . $student_basic['email'] . " )<br>";
                                        } else {
                                            echo "None";
                                        }
                                        if (isset($more_class_students)) {
                                            foreach ($more_class_students as $row) {
                                                echo $row['first_name'] . " " . $row['last_name'] . " ( " . $row['email'] . " )<br>";
                                            }
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>
                                            Schedule
                                        </b>
                                    </td>
                                    <td class="danger">
                                        <b>
                                            <?php
                                            if (isset($class_schedule)) {
                                                foreach ($class_schedule as $row) {
                                                    echo $row['day'] . " " . $row['time'] . "<br>";
                                                }
                                            } else {
                                                echo "None";
                                            }
                                            ?>
                                        </b>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
<?php if(isset($class_data) && $class_data['status'] != 2): ?>
    <div class="row">
        <div class="col-md-12">

            <ul class="page-breadcrumb breadcrumb">
                <li class="caption">
                    Class Schedule
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">

            <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/add_class_time/<?php if (isset($class_id)) echo $class_id; ?>/assign_class_details/">
                <div class="form-group">   
                    <div class="row">
                        <label class="control-label col-md-3">Select Day</label>
                        <div class="col-md-5">
                            <select class="form-control select2me" name="day" data-placeholder="Select...">
                                <option value=""></option>
                                <option value="Sat">Saturday</option>
                                <option value="Sun">Sunday</option>
                                <option value="Mon">Monday</option>
                                <option value="Tue">Tuesday</option>
                                <option value="Wed">Wednesday</option>
                                <option value="Thu">Thursday</option>
                                <option value="Fri">Friday</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="row">
                        <label class="control-label col-md-3">Time</label>
                        <div class="col-md-5">
                            <div class="input-group bootstrap-timepicker">
                                <input type="text" name="time" value="00:00:00" class="form-control timepicker-24" readonly>
                                <span class="input-group-btn">
                                    <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-5">
                            <button type="submit" class="btn green">Add</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-md-6">
            <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_class_time/<?php echo $class_id; ?>/assign_class_details">
                <div class="form-group">   
                    <div class="row">
                        <label class="control-label col-md-3">Select Schedule</label>
                        <div class="col-md-8">
                            <select class="form-control select2me" name="remove_schedule" data-placeholder="Select...">
                                <option value=""></option>
                                <?php
                                if (isset($class_schedule)):
                                    foreach ($class_schedule as $row) :
                                        ?>
                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['day'] . " - " . $row['time']; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>

                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-4">
                            <button type="submit" class="btn red">Remove</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>


    </div>



    <div class="row">
        <div class="col-md-12">

            <ul class="page-breadcrumb breadcrumb">
                <li class="caption">
                    More Students
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>

    <div class="row">

        <div class="col-md-6">
            <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/add_class_student/<?php echo $class_id ?>/assign_class_details/">
                <div class="row">
                    <div class="form-group">
                        <label class="control-label col-md-3">Select Student</label>
                        <div class="col-md-8">
                            <select class="form-control select2me" name="student_id" data-placeholder="Select...">
                                <option value=""></option>
                                <?php
                                if (isset($more_student_list)):
                                    foreach ($more_student_list as $row):
                                        ?>
                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'] . " " . $row['last_name'] . " ( " . $row['email'] . " )"; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>

                        </div>
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-8">
                            <button type="submit" class="btn green">Add</button>

                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-6">
            <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_class_student/<?php echo $class_id; ?>/assign_class_details/">
                <div class="form-group">   
                    <div class="row">
                        <label class="control-label col-md-3">Select Student</label>
                        <div class="col-md-8">
                            <select class="form-control select2me" name="student_id" data-placeholder="Select...">
                                <option value=""></option>
                                <?php
                                if (isset($more_student_list)):
                                    foreach ($more_student_list as $row):
                                        ?>
                                        <option value="<?php echo $row['id']; ?>"><?php echo $row['first_name'] . " " . $row['last_name'] . " ( " . $row['email'] . " )"; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>

                            </select>
                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-8">
                            <button type="submit" class="btn red">Remove</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <br>
    <div class="row">
        <!-- <div class="portlet box yellow"> -->

        <div class="portlet-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href="#tab_1_1" data-toggle="tab">Suggested Teachers</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="tab_1_1">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet box green">
                        <div class="portlet-title">


                        </div>
                        <div class="portlet-body">


                            <table class="table table-striped table-bordered table-hover" id="sample_2">
                                <thead>

                                    <tr>
                                        <th >
                                            Email
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Rating
                                        </th>
                                        <th>
                                            Personality
                                        </th>
                                        <th>
                                            Teaching Preference
                                        </th>
                                        <th>
                                            English Level
                                        </th>
                                        <th>
                                            Running Schedule
                                        </th>
                                        <th>
                                            Select
                                        </th>

                                    </tr>

                                </thead>
                                <tbody>
                                    <?php
                                    if (isset($suggested_teachers)):
                                        $i=0;
                                        foreach ($suggested_teachers as $row):
                                            ?>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <?php echo $row['email']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['first_name'] . " " . $row['last_name']; ?>
                                                </td>
                                                <td>
                                                    <div id="star<?php echo $i; ?>"></div>
                                                    <div>
                                                        <p>
                                                            <small><?php
                                                            if(!isset($teacher_rating[$i]['user_count']) || $teacher_rating[$i]['user_count'] == 0)
                                                                $total_rating = 0;
                                                            else 
                                                                $total_rating = (double)$teacher_rating[$i]['rating']/(double)$teacher_rating[$i]['user_count'];
                                                            
                                                            echo $total_rating."/5";
                                                            ?> by <?php
                                                    if(isset($teacher_rating[$i]['user_count'])) echo $teacher_rating[$i]['user_count'];
                                                    else echo "0" ;
                                                    $i++;
                                                        ?> User</small>
                                                        </p>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php echo $row['personality']; ?>
                                                </td>
                                                <td class="center">
                                                    <?php echo $row['teaching_preference']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $row['english_level']; ?>
                                                </td>
                                                <td>

                                                    <?php
                                                    if (isset($teacher_schedule[$row['user_id']]) && $teacher_schedule[$row['user_id']] != "")
                                                        echo $teacher_schedule[$row['user_id']];
                                                    else
                                                        echo "None";
                                                    ?>
                                                </td>
                                                <td>
                                                    <div class="col-md-12" style="margin-bottom: 10px;">
                                                        <a href="<?php echo base_url(); ?>index.php/admin/add_class_teacher/<?php echo $class_id; ?>/<?php echo $row['user_id']; ?>/assign_class_details/" class="btn green"> Select</a>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
        </div>
        <!--   </div> <!-- end of select teacher -->

    </div>
    <div class="row">
         <div class="table-toolbar">
                    <div class="col-md-12" style="margin-bottom: 10px;">
                        <a href="<?php echo base_url(); ?>index.php/admin/sm_remove_pending_request/<?php echo $class_id; ?>/" class="btn red">Remove Request <i class="fa fa-times"></i></a>
                    </div>

                </div>
    </div>

<?php endif; ?>

</div>
<!-- END PAGE CONTENT-->
</div>
