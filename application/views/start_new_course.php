<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <small>Course</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-calendar"></i>
                        <a href="">Start New Course</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <?php if (validation_errors() != ''): ?>
                <div class="alert alert-danger">
                    <?php echo validation_errors(); ?>
                </div>
            <?php endif; ?>
            <?php if (isset($success) && $success == 1): ?>
                <div class="alert alert-success">
                    New course request has been successfully submitted. 
                </div>
            <?php endif; ?>

            <?php if (isset($success) && $success == -1): ?>
                <div class="alert alert-danger">
                    You already have a pending request.
                </div>
            <?php endif; ?>


            <div class="">
                <!-- BEGIN FORM-->
                <form action="" class="form-horizontal" method="post">
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Course</label>
                            <div class="col-md-4">
                                <select name="course" class="form-control input-large select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <option value="1">Arabic</option>
                                    <option value="2">Quran</option>
                                    <option value="3">Arabic & Quran</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Level</label>
                            <div class="col-md-4">
                                <select name="level" class="form-control input-large select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <option value="1">Beginner</option>
                                    <option value="6">Intermediate</option>
                                    <option value="13">Advanced</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Skills Should be in Teacher</label>
                            <div class="col-md-4">
                                <select name="teacher_skill" class="form-control input-large select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <option value="1">Good English</option>
                                    <option value="2">Nice</option>
                                    <option value="3">Good With Kids</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Study Hours/Week</label>
                            <div class="col-md-4">
                                <select name="study_hours" class="form-control input-large select2me" data-placeholder="Select...">
                                    <option value=""></option>
                                    <option value="1">Light</option>
                                    <option value="2">Standard</option>
                                    <option value="3">Intensive</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-3">Start Date</label>
                            <div class="col-md-3">
                                <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                    <input name="start_date" type="text" class="form-control" readonly>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                    </span>
                                </div>
                                <!-- /input-group -->

                            </div>
                        </div>

                        <div class="form-actions fluid">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn blue">Next</button>
                                <a class="btn default" href="<?php echo base_url(); ?>index.php/admin/index/">Cancel</a>
                            </div>
                        </div>

                    </div>

                </form>
                <!-- END FORM-->



            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->

    <!-- END CONTAINER -->