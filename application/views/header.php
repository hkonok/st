<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0.3
Version: 1.5.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8"/>
        <title>Studioarabia</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1.0" name="viewport"/>
        <meta content="" name="description"/>
        <meta content="" name="author"/>
        <meta name="MobileOptimized" content="320">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo base_url(); ?>assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="<?php echo base_url(); ?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/compiled/timepicker.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/select2/select2_metro.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/data-tables/DT_bootstrap.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/datepicker.css"/>

        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/jquery-tags-input/jquery.tagsinput.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/clockface/css/clockface.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/jquery-multi-select/css/multi-select.css"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/select2/select2_metro.css"/>

        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES -->
        <link href="<?php echo base_url(); ?>assets/css/style-metronic.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/style-responsive.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/plugins.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/pages/tasks.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/themes/light.css" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet" type="text/css"/>
        
        <link href="<?php echo base_url(); ?>assets/css/pages/news.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href=".<?php echo base_url(); ?>assets/favicon.ico"/>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->
                <a class="navbar-brand" href="index.html" style="padding: 0px;">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png"  alt="logo" class="img-responsive"/>
                </a>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <img src="<?php echo base_url(); ?>assets/img/menu-toggler.png" alt=""/>
                </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <li class="dropdown" id="header_notification_bar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="fa fa-warning"></i>
                            <?php if(isset($inbox_notification['other_notification']) && sizeof($inbox_notification['other_notification']) > 0): ?>
                            <span class="badge">
                                1
                            </span>
                            <?php endif; ?>
                        </a>
                        <ul class="dropdown-menu extended notification">
                            <li>
                                <p>
                                    <?php if(isset($inbox_notification['other_notification']) && sizeof($inbox_notification['other_notification']) > 0): ?>
                                        You have <?php echo sizeof($inbox_notification['other_notification']); ?> new notifications
                                    <?php else: ?>
                                        You have no new notification.
                                    <?php endif; ?>
                                </p>
                            </li>
                            
                                    <?php 
                                        if(isset($inbox_notification['other_notification']) ):
                                            foreach($inbox_notification['other_notification'] as $row):
                                    ?>
                                    <li>
                                        <a href="<?php echo base_url(); ?>index.php/admin/process_notification/<?php echo $row['id']; ?>">
                                            <span class="label label-sm label-icon label-success">
                                                <i class="fa fa-file"></i>
                                            </span>
                                            <?php echo $row['name']; ?>
                                            <span class="time">
                                                <?php
                                                    $datetime = new DateTime($row['datetime']);
                                                    echo date_format($datetime, "M d y");
                                                ?>
                                            </span>
                                        </a>
                                    </li>
                                    <?php
                                            endforeach;
                                        endif;
                                    ?>
                               
                            
                        </ul>
                    </li>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN INBOX DROPDOWN -->
                    <li class="dropdown" id="header_inbox_bar">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="fa fa-envelope"></i>
                             <?php if(isset($inbox_notification['inbox_notification']) && sizeof($inbox_notification['inbox_notification']) > 0): ?>
                            <span class="badge">
                               <?php echo sizeof($inbox_notification['inbox_notification']); ?>
                            </span>
                            <?php endif; ?>
                        </a>
                        <ul class="dropdown-menu extended inbox">
                            <li>
                                <p>
                                    <?php if(isset($inbox_notification['inbox_notification']) && sizeof($inbox_notification['inbox_notification']) > 0): ?>
                                    You have <?php echo sizeof($inbox_notification['inbox_notification']); ?> new messages
                                    <?php else: ?>
                                    
                                    You have no new message
                                    <?php endif; ?>
                                </p>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 250px;">
                                    <?php 
                                        if(isset($inbox_notification['inbox_notification'])):
                                            foreach($inbox_notification['inbox_notification'] as $row):
                                     ?>
                                    <li>
                                        <a href="<?php echo base_url();?>index.php/admin/process_inbox/<?php echo $row['id']; ?>">
                                            
                                            <span class="subject">
                                                <span class="from">
                                                    <?php echo $row['from_name']; ?>
                                                </span>
                                                <span class="time">
                                                    <?php
                                                        $date_time = new DateTime($row['datetime']);
                                                        
                                                        echo date_format($date_time, "M d, y");
                                                    ?>
                                                </span>
                                            </span>
                                            <span class="message">
                                                <?php echo $row['message']; ?>
                                            </span>
                                        </a>
                                    </li>
                                    <?php
                                        endforeach;
                                    endif; 
                                    ?>
                                </ul>
                            </li>
                            <li class="external">
                                
                            </li>
                        </ul>
                    </li>
                    <!-- END INBOX DROPDOWN -->

                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" src="<?php echo base_url(); ?>assets/img/avatar1_small.jpg"/>
                            <span class="username">
                                <?php if ($this->session->userdata('user_id')): ?>
                                    <?php echo $this->session->userdata('first_name') . " " . $this->session->userdata('last_name'); ?>

                                <?php endif; ?>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo base_url(); ?>index.php/admin/profile/"><i class="fa fa-user"></i> My Profile</a>
                            </li>
                           
                            
                            <li class="divider">
                            </li>

                            <li>
                                <a href="<?php echo base_url(); ?>index.php/admin/log_out/"><i class="fa fa-key"></i> Log Out</a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>
        <!-- END HEADER -->
        <div class="clearfix">
        </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container light">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu">
                        <li class="sidebar-toggler-wrapper">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler hidden-phone">
                            </div>
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                        </li>

                        <br>
                        <!-- menu-> 1 -->
                        <li class="start  <?php if (isset($menu_id) && $menu_id == 1) echo 'active'; ?>">
                            <a href="<?php echo base_url(); ?>index.php/home">
                                <i class="fa fa-home"></i>
                                <span class="title">
                                    Home
                                </span>
                                <span class="selected">
                                </span>
                            </a>
                        </li>
                        <?php if (isset($user_type) && $user_type <= 5): ?>
                            <!-- menu-> 2 -->
                            <li <?php if (isset($menu_id) && $menu_id == 2) echo 'class="active"'; ?>>
                                <a href="javascript:;">
                                    <i class="fa fa-group"></i>
                                    <span class="title">
                                        Users
                                    </span>
                                    <span class="arrow ">
                                    </span>
                                </a>
                                <ul class="sub-menu">
                                    <?php if ($user_type < 1): ?>
                                        <!--sub menu-> 1 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 1) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/manager_list">

                                                Manager</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($user_type < 2 || $user_type == 4): ?>
                                        <!-- sub menu-> 2 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 2) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/technical_manager_list">

                                                Technical Manager</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($user_type < 2 || $user_type == 4): ?>
                                        <!-- sub menu-> 3 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 3) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/teacher_manager_list">

                                                Teacher Manager</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($user_type < 2): ?>
                                        <!-- sub menu-> 4 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 4) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/personal_manager_list">

                                                Personnel Manager</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($user_type < 2 || $user_type == 4): ?>
                                        <!-- sub menu-> 5 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 5) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/customer_service_list">
                                                Customer Service</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($user_type < 6): ?>
                                        <!-- sub menu-> 6 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 6) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/teacher_list">
                                                Teacher</a>
                                        </li>
                                    <?php endif; ?>

                                    <?php if ($user_type < 7 && $user_type != 4 && $user_type != 3 && $user_type != 2 ): ?>
                                        <!-- sub menu-> 7 -->
                                        <li <?php if (isset($sub_menu_id) && $sub_menu_id == 7) echo 'class="active"'; ?>>
                                            <a href="<?php echo base_url(); ?>index.php/student_list">
                                                Student</a>
                                        </li>
                                    <?php endif; ?>

                                </ul>
                            </li>

                        <?php endif; ?>

                        <?php if (isset($user_type) && $user_type == 7): ?>
                            <!-- menu-> 7 -->
                            <li <?php if (isset($menu_id) && $menu_id == 7) echo 'class="active"'; ?>>
                                <a href="javascript:;">
                                    <i class="fa fa-calendar"></i>
                                    <span class="title">
                                        Courses
                                    </span>
                                    <span class="arrow ">
                                    </span>
                                </a>
                                <ul class="sub-menu">
                                    <li <?php if (isset($menu_id)&& isset($sub_menu_id) && $menu_id == 7 && $sub_menu_id == 2) echo 'class="active"'; ?>>
                                        <a href="<?php echo base_url(); ?>index.php/admin/running_courses/">
                                                Active Courses
                                        </a>
                                    </li>
                                    <?php if(FALSE): ?>
                                    <li <?php if (isset($menu_id)&& isset($sub_menu_id) && $menu_id == 7 && $sub_menu_id == 1) echo 'class="active"'; ?>>
                                        <a href="<?php echo base_url(); ?>index.php/admin/start_new_course/">
                                                Start New Course
                                        </a>
                                    </li>
                                    <?php endif; ?>
                                    
                                    <li <?php if (isset($menu_id)&& isset($sub_menu_id) && $menu_id == 7 && $sub_menu_id == 3) echo 'class="active"'; ?>>
                                        <a href="<?php echo base_url(); ?>index.php/admin/completted_courses/">
                                                Completed Courses
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- menu-> 8 -->
                            <?php if(FALSE): ?>
                            <li <?php if (isset($menu_id) && $menu_id == 8) echo 'class="active"'; ?>>
                                <a href="<?php echo base_url(); ?>index.php/admin/pending_request/">
                                    <i class="fa  fa-clock-o"></i>
                                    <span class="title">
                                        Pending Requests
                                    </span>
                                    <span class="selected">
                                    </span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <!-- menu-> 9 -->
                            <!---
                            <li class="">
                                <a href="#">
                                    <i class="fa fa-list-alt"></i>
                                    <span class="title">
                                        Running Courses
                                    </span>
                                    <span class="selected">
                                    </span>
                                </a>
                            </li>
                            -->
                            <!-- menu-> 10 --
                            <li class="">
                                <a href="#">
                                    <i class="fa fa-share"></i>
                                    <span class="title">
                                        Promotional Offers
                                    </span>
                                    <span class="selected">
                                    </span>
                                </a>
                            </li>
                            <!-- menu-> 11 --
                            <li class="">
                                <a href="#">
                                    <i class="fa fa-money"></i>
                                    <span class="title">
                                        Billing Information
                                    </span>
                                    <span class="selected">
                                    </span>
                                </a>
                            </li>
                            <!-- menu-> 12 -->
                            <li class="<?php if(isset($menu_id) && $menu_id == 12) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/customer_service/">
                                    <i class="fa  fa-info-circle"></i>
                                    <span class="title">
                                        Customer Service
                                    </span>
                                    <span class="selected">
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>


                        
                        <?php if (isset($user_type) && $user_type <= 1): ?>
                            <!-- menu-> 14 -->
                            <li class="">
                                <a href="javascript:;">
                                    <i class="fa fa-map-marker"></i>
                                    <span class="title">
                                        Maps
                                    </span>
                                    <span class="arrow ">
                                    </span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="maps_google.html">
                                            Google Maps</a>
                                    </li>
                                    <li>
                                        <a href="maps_vector.html">
                                            Vector Maps</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- menu-> 15 -->

                        <?php endif; ?>
                            
                        <?php if( isset($user_type) && $user_type == '6'): ?>
                            <!-- menu-> 16 -->
                            <li class="<?php if( isset($menu_id) && $menu_id == 16) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/running_classes/">
                                    <i class="fa  fa-check"></i>
                                    <span class="title">
                                        Running Classes
                                    </span>
                                </a>
                            </li>
                            <!-- menu-> 17 -->
                            <li class="<?php if( isset($menu_id) && $menu_id == 17) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/completed_classes/">
                                    <i class="fa  fa-power-off"></i>
                                    <span class="title">
                                        Completed Classes
                                    </span>
                                </a>
                            </li>
                            <!-- menu-> 18 -->
                            <li class="<?php if( isset($menu_id) && $menu_id == 18) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/contact_support/">
                                    <i class="fa fa-user"></i>
                                    <span class="title">
                                        Contact Support
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                            
                          <?php if(isset($user_type) &&( $user_type == 5 || $user_type == 0 || $user_type == 1)): ?>  
                             <li class="<?php if( isset($menu_id) && $menu_id == 19) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/customer_service_inbox/">
                                    <i class="fa fa-user"></i>
                                    <span class="title">
                                        <?php if($user_type!=5):?>
                                          Customer Service
                                        <?php endif;?>
                                          Inbox
                                    </span>
                                </a>
                            </li>
                            
                            <li class="<?php if( isset($menu_id) && $menu_id == 30) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/create_class/">
                                    <i class="fa fa-plus"></i>
                                    <span class="title">
                                        Create Class
                                    </span>
                                </a>
                            </li>
                            
                            
                            <li class="<?php if( isset($menu_id) && $menu_id == 20) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/assign_class/">
                                    <i class="fa fa-exchange"></i>
                                    <span class="title">
                                        Assign Class
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                        
                        <?php if(isset($user_type) && ($user_type == 5 || $user_type < 2)): ?>
                            <li class="<?php if( isset($menu_id) && $menu_id == 21) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/running_class/">
                                    <i class="fa fa-check"></i>
                                    <span class="title">
                                        Running Class
                                    </span>
                                </a>
                            </li>
                            <li class="<?php if( isset($menu_id) && $menu_id == 22) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/completed_class/">
                                    <i class="fa  fa-power-off"></i>
                                    <span class="title">
                                        Completed Class
                                    </span>
                                </a>
                            </li>
                        <?php endif; ?>
                            
                       <?php if(isset($user_type) && $user_type <= 1): ?>
                            <li class="<?php if( isset($menu_id) && $menu_id == 23) echo "active"; ?>">
                                <a href="<?php echo base_url(); ?>index.php/admin/promotional_offers/">
                                    <i class="fa   fa-mail-forward"></i>
                                    <span class="title">
                                        Promotional Offers
                                    </span>
                                </a>
                            </li>
                       <?php endif; ?>
                            
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
            </div>

            <!-- END SIDEBAR -->