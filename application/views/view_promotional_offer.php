<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    View Promotional Offer
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa  fa-mail-forward"></i>
                        <a href="<?php echo base_url(); ?>index.php/admin/promotional_offers/">Promotional Offers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">View Promotional Offer</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->


        <div class="row">
            <div class="col-md-12 news-page">
                <div class="row">      
                    <div class="table-toolbar">
                        <div class="col-md-12" style="margin-bottom: 10px;">
                            <a href="<?php echo base_url(); ?>index.php/admin/new_promotional_offer/" class="btn green">New Promotional Offer <i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-10">
                        <div class="news-blocks" >
                            <h3>
                                <div> <?php echo $promotional_offer_data['subject']; ?></div>
                            </h3>
                            <br>
                            <div class="news-block-tags">

                                <em> <?php
                                    $datetime = new DateTime($promotional_offer_data['datetime']);
                                    echo date_format($datetime, "M d, y h:m");
                                    ?></em>
                            </div>
                            <div>
                                <?php echo $promotional_offer_data['offer']; ?>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>



    </div>