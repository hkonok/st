<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">

                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-calendar"></i>
                        <a href="#">Pending Requests</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

            <div class="alert alert-danger">
                No Pending Request Found.
            </div>

            <?php if (isset($success) && $success == 1): ?>
                <div class="alert alert-success">
                    Request has been successfully updated.
                </div>
            <?php endif; ?>

            <div class="">


                <div class="panel-group accordion" id="accordion1">
                    <?php $i = 0; foreach ($request_list as $row): $i++;?>
                    <div class="panel">
                        <div class="panel-heading accordion-toggle btn btn-block btn-success" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php echo $i; ?>">
                            <h4 class="panel-title ">
                                <?php
                                    if($row['course_id'] == 1)echo "Arabic";  
                                    else if($row['course_id'] == 2)echo "Quran";  
                                    else if($row['course_id'] == 3)echo "Arabic and Quran";  
                                ?>
                            </h4>
                        </div>
                        <div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse">
                            <div class="panel-body">
                                <!-- BEGIN FORM-->
                                <form action="" class="form-horizontal" method="post">
                                    <div class="form-body">

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Course</label>
                                            <div class="col-md-4">
                                                <select name="course" class="form-control input-large select2me" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="1" <?php if($row['course_id'] == 1) echo "selected"; ?>>Arabic</option>
                                                    <option value="2" <?php if($row['course_id'] == 2) echo "selected"; ?>>Quran</option>
                                                    <option value="3" <?php if($row['course_id'] == 3) echo "selected"; ?>>Arabic & Quran</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Level</label>
                                            <div class="col-md-4">
                                                <select name="level" class="form-control input-large select2me" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="1" <?php if($row['level'] == 1) echo "selected"; ?>>Beginner</option>
                                                    <option value="6" <?php if($row['level'] == 6) echo "selected"; ?>>Intermediate</option>
                                                    <option value="13" <?php if($row['level'] == 13) echo "selected"; ?>>Advanced</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Skills Should be in Teacher</label>
                                            <div class="col-md-4">
                                                <select name="teacher_skill" class="form-control input-large select2me" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="1" <?php if($row['skills_in_teacher'] == 1) echo "selected"; ?>>Good English</option>
                                                    <option value="2" <?php if($row['skills_in_teacher'] == 2) echo "selected"; ?>>Nice</option>
                                                    <option value="3" <?php if($row['skills_in_teacher'] == 3) echo "selected"; ?>>Good With Kids</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Study Hours/Week</label>
                                            <div class="col-md-4">
                                                <select name="study_hours" class="form-control input-large select2me" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="1" <?php if($row['study_hours'] == 1) echo "selected"; ?>>Light</option>
                                                    <option value="2" <?php if($row['study_hours'] == 2) echo "selected"; ?>>Standard</option>
                                                    <option value="3" <?php if($row['study_hours'] == 3) echo "selected"; ?>>Intensive</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Preferred Days</label>
                                            <div class="col-md-4">
                                                <input name="preferred_days" type="test" id="select2_sample5" class="form-control" 
                                                 value="<?php
                                                    $tmpa_data = $additional_data[$i-1];
                                                    $j = 0;
                                                    $tmp_time = '';
                                                    foreach($tmpa_data as $row2){
                                                        if($j > 0)
                                                            echo ", ";
                                                        echo $row2['day'];
                                                        $tmp_time = $row2['time'];
                                                        $j++;
                                                    }
                                                 ?>"
                                                 disabled>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-md-3">Class Time</label>
                                            <div class="col-md-3">
                                                <div class="input-group bootstrap-timepicker">
                                                    <input name="class_time" type="text" class="form-control timepicker-24" value="<?php echo $tmp_time; ?>" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button"><i class="fa fa-clock-o"></i></button>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="form-group">
                                            <label class="control-label col-md-3">Start Date</label>
                                            <div class="col-md-3">
                                                <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                    <input name="start_date" type="text" class="form-control" value="<?php echo $row['start_date']; ?>" readonly>
                                                    <span class="input-group-btn">
                                                        <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                    </span>
                                                </div>
                                                <!-- /input-group -->

                                            </div>
                                        </div>

                                        <div class="form-actions fluid">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn blue">Save Changes</button>
                                                <a class="btn default" href="<?php echo base_url(); ?>index.php/home/">Cancel</a>
                                            </div>
                                        </div>

                                    </div>

                                </form>
                                <!-- END FORM-->
                                
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    

                </div>

            </div>
            <!-- END PAGE CONTENT-->
        </div>
    </div>
    <!-- END CONTENT -->

    <!-- END CONTAINER -->