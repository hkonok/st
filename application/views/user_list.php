<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
							 Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
					Users <small>List of Managers</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-group"></i>
                        <a href="index.html">Users</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Managers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">List</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">

            <div class="col-md-12" style="margin-bottom: 10px;">
                <a href="#" class="btn blue">Create Manager <i class="fa fa-plus"></i></a>
            </div>
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-user"></i>User List
                        </div>

                    </div>
                    <div class="portlet-body">

                        <div class="row" style="margin-bottom:10px;">
                            <div class="col-md-6 col-sm-12">
                                <div id="sample_1_length" class="dataTables_length">
                                    <label>

                                        <select size="1" name="sample_1_length" aria-controls="sample_1" class="form-control input-small select2-offscreen" tabindex="-1">
                                            <option value="5">5</option>
                                            <option value="15">15</option>
                                            <option value="20">20</option>
                                            <option value="-1">All</option>
                                        </select> records
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="dataTables_filter" id="sample_1_filter">
                                    <label>Search: <input type="text" aria-controls="sample_1" class="form-control input-small" style="display:inline;"></label>
                                </div>
                            </div>
                        </div>



                        <table class="table table-striped table-bordered table-hover" id="sample_1">
                            <thead>
                                <tr>
                                    <th>
									Name
                                    </th>
                                    <th>
									Email
                                    </th>
                                    <th>
									Status
                                    </th>
                                    <th>
									Registered On
                                    </th>
                                    <th>
									Last Login
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
									konok habibullah araphat
                                    </td>
                                    <td>
									Internet Explorer 4.0
                                    </td>
                                    <td>
									Win 95+
                                    </td>
                                    <td>
									4
                                    </td>
                                    <td>
									X
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									Presto
                                    </td>
                                    <td>
									Opera 9.0
                                    </td>
                                    <td>
									Win 95+ / OSX.3+
                                    </td>
                                    <td>
									-
                                    </td>
                                    <td>
									A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									Presto
                                    </td>
                                    <td>
									Opera 9.2
                                    </td>
                                    <td>
									Win 88+ / OSX.3+
                                    </td>
                                    <td>
									-
                                    </td>
                                    <td>
									A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									Presto
                                    </td>
                                    <td>
									Opera 9.5
                                    </td>
                                    <td>
									Win 88+ / OSX.3+
                                    </td>
                                    <td>
									-
                                    </td>
                                    <td>
									A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									Presto
                                    </td>
                                    <td>
									Opera for Wii
                                    </td>
                                    <td>
									Wii
                                    </td>
                                    <td>
									-
                                    </td>
                                    <td>
									A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									Presto
                                    </td>
                                    <td>
									Nokia N800
                                    </td>
                                    <td>
									N800
                                    </td>
                                    <td>
									-
                                    </td>
                                    <td>
									A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
									Presto
                                    </td>
                                    <td>
									Nintendo DS browser
                                    </td>
                                    <td>
									Nintendo DS
                                    </td>
                                    <td>
									8.5
                                    </td>
                                    <td>
									C/A<sup>1</sup>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="row"><div class="col-md-5 col-sm-12"><div class="dataTables_info" id="sample_1_info">Showing 1 to 10 of 43 entries</div></div><div class="col-md-7 col-sm-12"><div class="dataTables_paginate paging_bootstrap"><ul class="pagination" style="visibility: visible;"><li class="prev disabled"><a href="#" title="Previous"><i class="fa fa-angle-left"></i></a></li><li class="active"><a href="#">1</a></li><li><a href="#">2</a></li><li><a href="#">3</a></li><li><a href="#">4</a></li><li><a href="#">5</a></li><li class="next"><a href="#" title="Next"><i class="fa fa-angle-right"></i></a></li></ul></div></div></div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->