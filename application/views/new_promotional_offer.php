<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    New Promotional Offers
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa  fa-mail-forward"></i>
                        <a href="<?php echo base_url(); ?>index.php/admin/promotional_offers/">Promotional Offers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">New Promotional Offer</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->


        <div class="row" >
            <div class="col-md-12">
                <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">

                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-2">Subject:</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="subject">
                            </div>
                        </div>
                        <div class="form-group last">
                            <label class="control-label col-md-2">CKEditor</label>
                            <div class=" col-md-10">
                                <textarea class="ckeditor form-control" name="offer" rows="6"></textarea>
                            </div>
                        </div>
                    </div>
                     <div class="form-actions">
                                <div class="col-md-offset-2 col-md-10">
                                    <button type="submit" class="btn blue">Send</button>
                                </div>
                     </div>
                </form>
            </div>
        </div>



    </div>