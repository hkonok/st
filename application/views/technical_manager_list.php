<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    Users <small>List of Technical Managers</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-group"></i>
                        <a href="index.html">Users</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">Technical Managers</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#">List</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div >
                <?php
                if (isset($msg))
                    echo $msg;
                ?>
            </div>
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-globe"></i>Technical Managers List
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="table-toolbar">
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <a href="<?php echo base_url(); ?>index.php/admin/create_technical_manager" class="btn blue">Create Technical Manager <i class="fa fa-plus"></i></a>
                            </div>

                        </div>
                        <?php if (isset($technical_manager_list)) { ?>
                            <table class="table table-striped table-bordered table-hover" id="sample_1">
                                <thead>
                                    <tr>
                                        <th >
                                            Email
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Registered On
                                        </th>
                                        <th>
                                            Last Login
                                        </th>
                                        <th>
                                            Profile
                                        </th>
                                        <th>
                                            Status
                                        </th>
                                        <?php
                                        if (isset($user_type) && ($user_type == 1 || $user_type == 0))
                                            echo '<th>Change Status</th>';
                                        ?>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($technical_manager_list as $row) { ?>
                                        <tr class="odd gradeX">
                                            <td>
                                                <?php echo $row->email; ?>
                                            </td>
                                            <td>
                                                <?php echo $row->first_name . " " . $row->last_name; ?>
                                            </td>
                                            <td>
                                                <?php echo $row->reg_datetime; ?>
                                            </td>
                                            <td>
                                                <?php echo $row->last_login_datetime; ?>
                                            </td>
                                            <td class="center">
                                                <div class="col-md-12" style="margin-bottom: 10px;">
                                                    <a href="<?php echo base_url(); ?>index.php/admin/edit_technical_manager/<?php echo $row->id; ?>" class="btn green"> View</a>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="form-group">

                                                    <?php if ($row->status == 1): ?>
                                                        <div class="alert-success">
                                                            <strong>Active</strong>
                                                        </div>
                                                    <?php else: ?>
                                                        <div class="alert-danger">
                                                            <strong>Deactivated</strong>
                                                        </div>
                                                    <?php endif; ?>

                                                </div>
                                            </td>
                                            <?php
                                            if (isset($user_type) && ($user_type == 1 || $user_type == 0)):
                                                ?>
                                                <td>
                                                    <div class="" style="margin-bottom: 1px;">
                                                        <a href="<?php echo base_url(); ?>index.php/admin/change_user_status/<?php echo $row->id; ?>/technical_manager_list/" class="btn red"> Change Status</a>
                                                    </div>
                                                </td>
                                            <?php endif; ?>
                                        </tr>

                                    <?php } ?>

                                </tbody>
                            </table>
                        <?php } ?>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>


    </div>
    <!-- END PAGE CONTENT-->
</div>
