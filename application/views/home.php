
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    StudioArabiya <small> </small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="clearfix">
        </div>

        <div class="row ">
            <div class="col-md-6" >
                <!-- BEGIN PORTLET-->
                <div class="calendar">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption">
                                Class Schedule
                            </div>
                        </div>
                        <div class="portlet-body light-grey">
                            <div id="calendar">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row" style="margin-left: 10px; ">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet paddingless">
                        <div class="portlet-title line">
                            <div class="caption">
                                <i class="fa fa-bell-o"></i>Feeds
                            </div>

                        </div>
                        <div class="portlet-body">
                            <!--BEGIN TABS-->
                            <div class="tabbable tabbable-custom">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab_1_1" data-toggle="tab">Messages and Notifications</a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_1_1">
                                        <div class="scroller" style="height:auto " data-always-visible="1" data-rail-visible="0">

                                            <ul class="feeds">
                                                <?php
                                                if ($inbox_notification['inbox_notification']):
                                                    foreach ($inbox_notification['inbox_notification'] as $row):
                                                        ?>


                                                        <li>
                                                            <div class="col1">
                                                                <div class="cont">
                                                                    <div class="cont-col1">
                                                                        <div class="label label-sm label-warning">
                                                                            <i class="fa fa-inbox"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="cont-col2">
                                                                        <div class="desc">
                                                                            <?php echo $row['from_name'] ?> 
                                                                            <a href="<?php echo base_url(); ?>index.php/admin/process_inbox/<?php echo $row['id']; ?>" style="text-decoration: none;">
                                                                                <span  class="label label-sm label-danger ">
                                                                                    View <i class="fa fa-share"></i>
                                                                                </span>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col2">
                                                                <div class="date">
                                                                    <?php
                                                                    $datetime = new DateTime($row['datetime']);
                                                                    echo date_format($datetime, "M d y");
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </li>


                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>

                                                <?php
                                                if ($inbox_notification['other_notification']):
                                                    foreach ($inbox_notification['other_notification'] as $row):
                                                        ?>
                                                        <li>
                                                            <a href="<?php echo base_url(); ?>index.php/admin/process_notification/<?php echo $row['id']; ?>" style="text-decoration: none;">

                                                                <div class="col1">
                                                                    <div class="cont">
                                                                        <div class="cont-col1">
                                                                            <div class="label label-sm label-success">
                                                                                <i class="fa fa-file"></i>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cont-col2">
                                                                            <div class="desc">
                                                                                <?php echo $row['name'] ?> 

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col2">
                                                                    <div class="date">
                                                                        <?php
                                                                        $datetime = new DateTime($row['datetime']);
                                                                        echo date_format($datetime, "M d y");
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </li>


                                                        <?php
                                                    endforeach;
                                                endif;
                                                ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
            </div>


        </div>

    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
