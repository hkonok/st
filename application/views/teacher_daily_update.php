<?php
switch ($user_basic['user_type']) {
    case 0 :
        $user_type_name = 'Super Admin';
        break;
    case 1:
        $user_type_name = 'Manger';
        break;
    case 2:
        $user_type_name = 'Techniclal Manager';
        break;
    case 3:
        $user_type_name = 'Teacher Manager';
        break;
    case 4:
        $user_type_name = 'Personnel Manager';
        break;
    case 5:
        $user_type_name = 'Customer Support';
        break;
    case 6:
        $user_type_name = 'Teacher';
        break;
    case 7:
        $user_type_name = 'Student';
        break;
    default :
        $user_type_name = "";
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name'] . " " . $user_data['last_name']; ?> <small><?php echo $user_type_name; ?> Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#"><?php echo $user_type_name; ?> Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">



                               
                                  <!-- BEGIN ACCORDION PORTLET-->
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            </i>Attendance List
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="panel-group accordion scrollable" id="accordion2">
                                            
                                         <?php if(isset($running_class)):
                                                    $i = 0;
                                                    $class_name = array('1'=> 'Arabic', '2' => 'Quran', '3' => 'Arabic and Quran');
                                                    foreach($running_class as $row):
                                                    $i++;
                                                    
                                             ?>
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_<?php echo $i; ?>">
                                                            <?php echo $class_name[$row['course_id']]; ?></a>
                                                    </h4>
                                                </div>
                                                <div id="collapse_2_<?php echo $i; ?>" class="panel-collapse <?php if($i == 1) echo "in"; else echo "collapse"; ?>">
                                                    <div class="panel-body">
                                                        <!-- BEGIN SAMPLE TABLE PORTLET-->

                                                        <div class="table-responsive">
                                                            <table class="table table-bordered table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            #
                                                                        </th>
                                                                        <th>
                                                                            Date Time
                                                                        </th>
                                                                        <th>
                                                                            Attendance
                                                                        </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        $j=0;
                                                                        foreach($attendance_data[$row['id']] as $attendance):
                                                                        $j++;
                                                                        $datetime = new DateTime($attendance['datetime']);
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <?php echo $j; ?>
                                                                        </td>
                                                                        <td>
                                                                            <?php
                                                                                echo date_format($datetime, "M d, Y"); 
                                                                                $hmtl_class_name = array('1' => "success", '0' => "danger");
                                                                            ?>
                                                                        </td>
                                                                        <td class="<?php echo $hmtl_class_name[$attendance['attendance']]; ?>">
                                                                            <?php
                                                                                $tmp_data = array('1' => "Present", '0' => "Absent");
                                                                                echo $tmp_data[$attendance['attendance']];
                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                    <?php
                                                                        endforeach;
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <?php
                                         endforeach;
                                                endif;
                                                ?>
                                            

                                            <!-- END SAMPLE TABLE PORTLET-->

                                            
                                            <?php if(isset($user_type) && $user_type == 3 ): ?>
                                            
                                            <br><br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <ul class="page-breadcrumb breadcrumb">
                                                        <li class="caption">
                                                            Give Attendance
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                            
                                            
                                            <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/give_teacher_attendance/<?php echo $teacher_id; ?>">
                                                <div class="row">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Select Date</label>
                                                        <div class="col-md-4">
                                                            <select class="form-control select2me" name="datetime" data-placeholder="Select...">
                                                                <option value=""></option>
                                                                <?php
                                                                if (isset($teacher_attendance_dates)):
                                                                    foreach ($teacher_attendance_dates as $row):
                                                                        $st_datetime = $row['datetime'];
                                                                        ?>
                                                                        <option value="<?php echo $row['class_id']."|".date_format($st_datetime, "Y-m-d") . " " . $row['class_time']; ?>"><?php echo $row['class_name']." ( ".date_format($st_datetime, "M d, Y") . " " . $row['class_time']." )"; ?></option>
                                                                        <?php
                                                                    endforeach;
                                                                endif;
                                                                ?>

                                                            </select>

                                                        </div>
                                                    </div>
                                                </div>
                                                <br>
                                                
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label class="control-label col-md-3"><?php echo $teacher_details['first_name'] . " " . $teacher_details['last_name']; ?></label>

                                                            <div class="col-md-4">
                                                                <select class="form-control select2me" name="attendance" data-placeholder="Select...">
                                                                    <option value=""></option>
                                                                    <option value="1">Present</option>
                                                                    <option value="0">Absent</option>
                                                                </select>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <br>

                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-4">
                                                            <button type="submit" class="btn green">Submit</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                            <?php endif; ?>


                                        </div>
                                    </div>
                                </div>
                                <!-- END ACCORDION PORTLET-->
                                
                                
                                
                                


                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
