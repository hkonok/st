
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    User Profile <small>user profile sample</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#">User Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <div class="col-md-3">
                        <ul class="ver-inline-menu tabbable margin-bottom-10">

                            <li class="active">
                                <a data-toggle="tab" href="#tab_1-1">
                                    <i class="fa fa-cog"></i> Personal info </a>
                                <span class="after">
                                </span>
                            </li>

                            <li>
                                <a data-toggle="tab" href="#tab_3-3"><i class="fa fa-lock"></i> Change Password</a>
                            </li>


                        </ul>
                    </div>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form role="form" class="from-horizontal" method="post" action="">
                                    <div class="form-group">
                                        <label class="control-label">First Name</label>
                                        <input type="text" name="first_name" placeholder="Frist Name" value="<?php if(isset($user_data['first_name'])) echo $user_data['first_name']; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Last Name</label>
                                        <input type="text" name="last_name" placeholder="Lst Name" value="<?php if(isset($user_data['last_name'])) echo $user_data['last_name']; ?>" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Email Address</label>

                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-envelope"></i>
                                            </span>
                                            <input type="email" name="email" placeholder="Email Address" value="<?php if(isset($user_basic['email'])) echo $user_basic['email']; ?>" name="email" class="form-control" >
                                        </div>
                                    </div>
                                    <?php if ($user_basic['user_type'] == 6 || $user_basic['user_type'] == 7): ?>
                                        <div class="form-group">
                                            <label class="control-label">Birthday</label>
                                            <div class="">
                                                <input class="form-control form-control-inline input-medium date-picker" name="birthday" data-date-format="yyyy-mm-dd" name="date" size="16" type="text" value="<?php if(isset($user_data['birthday'])) echo $user_data['birthday']; ?>"/>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label">Gender</label>
                                            <div class="">
                                                <select class="form-control select2me" name="gender" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="male" <?php if (isset($user_data['gender']) && $user_data['gender'] == 'male') echo 'selected'; ?>>Male</option>
                                                    <option value="female" <?php if (isset($user_data['gender']) && $user_data['gender'] == 'female') echo 'selected'; ?>>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php $change_country = FALSE; if( isset($user_data['country']) && $user_data['country'] != ''): ?>
                                    <div class="form-group">
                                            <label class="control-label">Country</label>
                                            <div class="">
                                                <select class="form-control select2me" data-placeholder="Select..." disabled>
                                                    <option value="<?php echo $user_data['country']; ?>"><?php echo $user_data['country']; ?></option>
                                                </select>
                                            </div>
                                    </div>
                                    <?php $change_country = TRUE; endif; ?>


                                    <div class="form-group">
                                        <label class="control-label"><?php if($change_country === TRUE) echo 'Change'; ?> Country</label>
                                        <div class="">
                                            <select class="form-control select2me" name="country" data-placeholder="Select...">
                                                <option value=""></option>


                                                <option value="Afganistan">Afghanistan</option>
                                                <option value="Albania">Albania</option>
                                                <option value="Algeria">Algeria</option>
                                                <option value="American Samoa">American Samoa</option>
                                                <option value="Andorra">Andorra</option>
                                                <option value="Angola">Angola</option>
                                                <option value="Anguilla">Anguilla</option>
                                                <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                                <option value="Argentina">Argentina</option>
                                                <option value="Armenia">Armenia</option>
                                                <option value="Aruba">Aruba</option>
                                                <option value="Australia">Australia</option>
                                                <option value="Austria">Austria</option>
                                                <option value="Azerbaijan">Azerbaijan</option>
                                                <option value="Bahamas">Bahamas</option>
                                                <option value="Bahrain">Bahrain</option>
                                                <option value="Bangladesh">Bangladesh</option>
                                                <option value="Barbados">Barbados</option>
                                                <option value="Belarus">Belarus</option>
                                                <option value="Belgium">Belgium</option>
                                                <option value="Belize">Belize</option>
                                                <option value="Benin">Benin</option>
                                                <option value="Bermuda">Bermuda</option>
                                                <option value="Bhutan">Bhutan</option>
                                                <option value="Bolivia">Bolivia</option>
                                                <option value="Bonaire">Bonaire</option>
                                                <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                                <option value="Botswana">Botswana</option>
                                                <option value="Brazil">Brazil</option>
                                                <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                                <option value="Brunei">Brunei</option>
                                                <option value="Bulgaria">Bulgaria</option>
                                                <option value="Burkina Faso">Burkina Faso</option>
                                                <option value="Burundi">Burundi</option>
                                                <option value="Cambodia">Cambodia</option>
                                                <option value="Cameroon">Cameroon</option>
                                                <option value="Canada">Canada</option>
                                                <option value="Canary Islands">Canary Islands</option>
                                                <option value="Cape Verde">Cape Verde</option>
                                                <option value="Cayman Islands">Cayman Islands</option>
                                                <option value="Central African Republic">Central African Republic</option>
                                                <option value="Chad">Chad</option>
                                                <option value="Channel Islands">Channel Islands</option>
                                                <option value="Chile">Chile</option>
                                                <option value="China">China</option>
                                                <option value="Christmas Island">Christmas Island</option>
                                                <option value="Cocos Island">Cocos Island</option>
                                                <option value="Colombia">Colombia</option>
                                                <option value="Comoros">Comoros</option>
                                                <option value="Congo">Congo</option>
                                                <option value="Cook Islands">Cook Islands</option>
                                                <option value="Costa Rica">Costa Rica</option>
                                                <option value="Cote DIvoire">Cote D'Ivoire</option>
                                                <option value="Croatia">Croatia</option>
                                                <option value="Cuba">Cuba</option>
                                                <option value="Curaco">Curacao</option>
                                                <option value="Cyprus">Cyprus</option>
                                                <option value="Czech Republic">Czech Republic</option>
                                                <option value="Denmark">Denmark</option>
                                                <option value="Djibouti">Djibouti</option>
                                                <option value="Dominica">Dominica</option>
                                                <option value="Dominican Republic">Dominican Republic</option>
                                                <option value="East Timor">East Timor</option>
                                                <option value="Ecuador">Ecuador</option>
                                                <option value="Egypt">Egypt</option>
                                                <option value="El Salvador">El Salvador</option>
                                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                <option value="Eritrea">Eritrea</option>
                                                <option value="Estonia">Estonia</option>
                                                <option value="Ethiopia">Ethiopia</option>
                                                <option value="Falkland Islands">Falkland Islands</option>
                                                <option value="Faroe Islands">Faroe Islands</option>
                                                <option value="Fiji">Fiji</option>
                                                <option value="Finland">Finland</option>
                                                <option value="France">France</option>
                                                <option value="French Guiana">French Guiana</option>
                                                <option value="French Polynesia">French Polynesia</option>
                                                <option value="French Southern Ter">French Southern Ter</option>
                                                <option value="Gabon">Gabon</option>
                                                <option value="Gambia">Gambia</option>
                                                <option value="Georgia">Georgia</option>
                                                <option value="Germany">Germany</option>
                                                <option value="Ghana">Ghana</option>
                                                <option value="Gibraltar">Gibraltar</option>
                                                <option value="Great Britain">Great Britain</option>
                                                <option value="Greece">Greece</option>
                                                <option value="Greenland">Greenland</option>
                                                <option value="Grenada">Grenada</option>
                                                <option value="Guadeloupe">Guadeloupe</option>
                                                <option value="Guam">Guam</option>
                                                <option value="Guatemala">Guatemala</option>
                                                <option value="Guinea">Guinea</option>
                                                <option value="Guyana">Guyana</option>
                                                <option value="Haiti">Haiti</option>
                                                <option value="Hawaii">Hawaii</option>
                                                <option value="Honduras">Honduras</option>
                                                <option value="Hong Kong">Hong Kong</option>
                                                <option value="Hungary">Hungary</option>
                                                <option value="Iceland">Iceland</option>
                                                <option value="India">India</option>
                                                <option value="Indonesia">Indonesia</option>
                                                <option value="Iran">Iran</option>
                                                <option value="Iraq">Iraq</option>
                                                <option value="Ireland">Ireland</option>
                                                <option value="Isle of Man">Isle of Man</option>
                                                <option value="Israel">Israel</option>
                                                <option value="Italy">Italy</option>
                                                <option value="Jamaica">Jamaica</option>
                                                <option value="Japan">Japan</option>
                                                <option value="Jordan">Jordan</option>
                                                <option value="Kazakhstan">Kazakhstan</option>
                                                <option value="Kenya">Kenya</option>
                                                <option value="Kiribati">Kiribati</option>
                                                <option value="Korea North">Korea North</option>
                                                <option value="Korea Sout">Korea South</option>
                                                <option value="Kuwait">Kuwait</option>
                                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                <option value="Laos">Laos</option>
                                                <option value="Latvia">Latvia</option>
                                                <option value="Lebanon">Lebanon</option>
                                                <option value="Lesotho">Lesotho</option>
                                                <option value="Liberia">Liberia</option>
                                                <option value="Libya">Libya</option>
                                                <option value="Liechtenstein">Liechtenstein</option>
                                                <option value="Lithuania">Lithuania</option>
                                                <option value="Luxembourg">Luxembourg</option>
                                                <option value="Macau">Macau</option>
                                                <option value="Macedonia">Macedonia</option>
                                                <option value="Madagascar">Madagascar</option>
                                                <option value="Malaysia">Malaysia</option>
                                                <option value="Malawi">Malawi</option>
                                                <option value="Maldives">Maldives</option>
                                                <option value="Mali">Mali</option>
                                                <option value="Malta">Malta</option>
                                                <option value="Marshall Islands">Marshall Islands</option>
                                                <option value="Martinique">Martinique</option>
                                                <option value="Mauritania">Mauritania</option>
                                                <option value="Mauritius">Mauritius</option>
                                                <option value="Mayotte">Mayotte</option>
                                                <option value="Mexico">Mexico</option>
                                                <option value="Midway Islands">Midway Islands</option>
                                                <option value="Moldova">Moldova</option>
                                                <option value="Monaco">Monaco</option>
                                                <option value="Mongolia">Mongolia</option>
                                                <option value="Montserrat">Montserrat</option>
                                                <option value="Morocco">Morocco</option>
                                                <option value="Mozambique">Mozambique</option>
                                                <option value="Myanmar">Myanmar</option>
                                                <option value="Nambia">Nambia</option>
                                                <option value="Nauru">Nauru</option>
                                                <option value="Nepal">Nepal</option>
                                                <option value="Netherland Antilles">Netherland Antilles</option>
                                                <option value="Netherlands">Netherlands (Holland, Europe)</option>
                                                <option value="Nevis">Nevis</option>
                                                <option value="New Caledonia">New Caledonia</option>
                                                <option value="New Zealand">New Zealand</option>
                                                <option value="Nicaragua">Nicaragua</option>
                                                <option value="Niger">Niger</option>
                                                <option value="Nigeria">Nigeria</option>
                                                <option value="Niue">Niue</option>
                                                <option value="Norfolk Island">Norfolk Island</option>
                                                <option value="Norway">Norway</option>
                                                <option value="Oman">Oman</option>
                                                <option value="Pakistan">Pakistan</option>
                                                <option value="Palau Island">Palau Island</option>
                                                <option value="Palestine">Palestine</option>
                                                <option value="Panama">Panama</option>
                                                <option value="Papua New Guinea">Papua New Guinea</option>
                                                <option value="Paraguay">Paraguay</option>
                                                <option value="Peru">Peru</option>
                                                <option value="Phillipines">Philippines</option>
                                                <option value="Pitcairn Island">Pitcairn Island</option>
                                                <option value="Poland">Poland</option>
                                                <option value="Portugal">Portugal</option>
                                                <option value="Puerto Rico">Puerto Rico</option>
                                                <option value="Qatar">Qatar</option>
                                                <option value="Republic of Montenegro">Republic of Montenegro</option>
                                                <option value="Republic of Serbia">Republic of Serbia</option>
                                                <option value="Reunion">Reunion</option>
                                                <option value="Romania">Romania</option>
                                                <option value="Russia">Russia</option>
                                                <option value="Rwanda">Rwanda</option>
                                                <option value="St Barthelemy">St Barthelemy</option>
                                                <option value="St Eustatius">St Eustatius</option>
                                                <option value="St Helena">St Helena</option>
                                                <option value="St Kitts-Nevis">St Kitts-Nevis</option>
                                                <option value="St Lucia">St Lucia</option>
                                                <option value="St Maarten">St Maarten</option>
                                                <option value="St Pierre &amp; Miquelon">St Pierre &amp; Miquelon</option>
                                                <option value="St Vincent &amp; Grenadines">St Vincent &amp; Grenadines</option>
                                                <option value="Saipan">Saipan</option>
                                                <option value="Samoa">Samoa</option>
                                                <option value="Samoa American">Samoa American</option>
                                                <option value="San Marino">San Marino</option>
                                                <option value="Sao Tome &amp; Principe">Sao Tome &amp; Principe</option>
                                                <option value="Saudi Arabia">Saudi Arabia</option>
                                                <option value="Senegal">Senegal</option>
                                                <option value="Serbia">Serbia</option>
                                                <option value="Seychelles">Seychelles</option>
                                                <option value="Sierra Leone">Sierra Leone</option>
                                                <option value="Singapore">Singapore</option>
                                                <option value="Slovakia">Slovakia</option>
                                                <option value="Slovenia">Slovenia</option>
                                                <option value="Solomon Islands">Solomon Islands</option>
                                                <option value="Somalia">Somalia</option>
                                                <option value="South Africa">South Africa</option>
                                                <option value="Spain">Spain</option>
                                                <option value="Sri Lanka">Sri Lanka</option>
                                                <option value="Sudan">Sudan</option>
                                                <option value="Suriname">Suriname</option>
                                                <option value="Swaziland">Swaziland</option>
                                                <option value="Sweden">Sweden</option>
                                                <option value="Switzerland">Switzerland</option>
                                                <option value="Syria">Syria</option>
                                                <option value="Tahiti">Tahiti</option>
                                                <option value="Taiwan">Taiwan</option>
                                                <option value="Tajikistan">Tajikistan</option>
                                                <option value="Tanzania">Tanzania</option>
                                                <option value="Thailand">Thailand</option>
                                                <option value="Togo">Togo</option>
                                                <option value="Tokelau">Tokelau</option>
                                                <option value="Tonga">Tonga</option>
                                                <option value="Trinidad &amp; Tobago">Trinidad &amp; Tobago</option>
                                                <option value="Tunisia">Tunisia</option>
                                                <option value="Turkey">Turkey</option>
                                                <option value="Turkmenistan">Turkmenistan</option>
                                                <option value="Turks &amp; Caicos Is">Turks &amp; Caicos Is</option>
                                                <option value="Tuvalu">Tuvalu</option>
                                                <option value="Uganda">Uganda</option>
                                                <option value="Ukraine">Ukraine</option>
                                                <option value="United Arab Erimates">United Arab Emirates</option>
                                                <option value="United Kingdom">United Kingdom</option>
                                                <option value="United States of America">United States of America</option>
                                                <option value="Uraguay">Uruguay</option>
                                                <option value="Uzbekistan">Uzbekistan</option>
                                                <option value="Vanuatu">Vanuatu</option>
                                                <option value="Vatican City State">Vatican City State</option>
                                                <option value="Venezuela">Venezuela</option>
                                                <option value="Vietnam">Vietnam</option>
                                                <option value="Virgin Islands (Brit)">Virgin Islands (Brit)</option>
                                                <option value="Virgin Islands (USA)">Virgin Islands (USA)</option>
                                                <option value="Wake Island">Wake Island</option>
                                                <option value="Wallis &amp; Futana Is">Wallis &amp; Futana Is</option>
                                                <option value="Yemen">Yemen</option>
                                                <option value="Zaire">Zaire</option>
                                                <option value="Zambia">Zambia</option>
                                                <option value="Zimbabwe">Zimbabwe</option>
                                            </select>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label">Select Timezone</label>
                                        <div >
                                            <select class="form-control select2me" name="time_zone"  data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="-12.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -12.0) echo 'selected'; ?>>(GMT -12:00) Eniwetok, Kwajalein</option>
                                                <option value="-11.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -11.0) echo 'selected'; ?>>(GMT -11:00) Midway Island, Samoa</option>
                                                <option value="-10.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -10.0) echo 'selected'; ?>>(GMT -10:00) Hawaii</option>
                                                <option value="-9.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -9.0) echo 'selected'; ?>>(GMT -9:00) Alaska</option>
                                                <option value="-8.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -8.0) echo 'selected'; ?>>(GMT -8:00) Pacific Time (US &amp; Canada)</option>
                                                <option value="-7.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -7.0) echo 'selected'; ?>>(GMT -7:00) Mountain Time (US &amp; Canada)</option>
                                                <option value="-6.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -6.0) echo 'selected'; ?>>(GMT -6:00) Central Time (US &amp; Canada), Mexico City</option>
                                                <option value="-5.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -5.0) echo 'selected'; ?>>(GMT -5:00) Eastern Time (US &amp; Canada), Bogota, Lima</option>
                                                <option value="-4.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -4.0) echo 'selected'; ?>>(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz</option>
                                                <option value="-3.5" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -3.5) echo 'selected'; ?>>(GMT -3:30) Newfoundland</option>
                                                <option value="-3.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -3.0) echo 'selected'; ?>>(GMT -3:00) Brazil, Buenos Aires, Georgetown</option>
                                                <option value="-2.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -2.0) echo 'selected'; ?>>(GMT -2:00) Mid-Atlantic</option>
                                                <option value="-1.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -1.0) echo 'selected'; ?>>(GMT -1:00 hour) Azores, Cape Verde Islands</option>
                                                <option value="0.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == -0.0) echo 'selected'; ?>>(GMT) Western Europe Time, London, Lisbon, Casablanca</option>
                                                <option value="1.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 1.0) echo 'selected'; ?>>(GMT +1:00 hour) Brussels, Copenhagen, Madrid, Paris</option>
                                                <option value="2.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 2.0) echo 'selected'; ?>>(GMT +2:00) Kaliningrad, South Africa</option>
                                                <option value="3.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 3.0) echo 'selected'; ?>>(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg</option>
                                                <option value="3.5" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 3.5) echo 'selected'; ?>>(GMT +3:30) Tehran</option>
                                                <option value="4.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 4.0) echo 'selected'; ?>>(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi</option>
                                                <option value="4.5" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 4.5) echo 'selected'; ?>>(GMT +4:30) Kabul</option>
                                                <option value="5.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 5.0) echo 'selected'; ?>>(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent</option>
                                                <option value="5.5" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 5.5) echo 'selected'; ?>>(GMT +5:30) Bombay, Calcutta, Madras, New Delhi</option>
                                                <option value="5.75" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 5.75) echo 'selected'; ?>>(GMT +5:45) Kathmandu</option>
                                                <option value="6.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 6.0) echo 'selected'; ?>>(GMT +6:00) Almaty, Dhaka, Colombo</option>
                                                <option value="7.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 7.0) echo 'selected'; ?>>(GMT +7:00) Bangkok, Hanoi, Jakarta</option>
                                                <option value="8.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 8.0) echo 'selected'; ?>>(GMT +8:00) Beijing, Perth, Singapore, Hong Kong</option>
                                                <option value="9.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 9.0) echo 'selected'; ?>>(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk</option>
                                                <option value="9.5" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 9.5) echo 'selected'; ?>>(GMT +9:30) Adelaide, Darwin</option>
                                                <option value="10.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 10.0) echo 'selected'; ?>>(GMT +10:00) Eastern Australia, Guam, Vladivostok</option>
                                                <option value="11.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 11.0) echo 'selected'; ?>>(GMT +11:00) Magadan, Solomon Islands, New Caledonia</option>
                                                <option value="12.0" <?php if (isset($user_data['time_zone']) &&  $user_data['time_zone'] == 12.0) echo 'selected'; ?>>(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka</option>

                                            </select>

                                        </div>
                                    </div>
                                    
                                    <?php if(isset($user_basic['user_type']) && $user_basic['user_type'] == 7): ?>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Skype ID</label>
                                        <input type="text" name="skype_id" placeholder="Skype ID" value="<?php if(isset($user_data['skype_id'])) echo $user_data['skype_id']; ?>" class="form-control"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Address</label>
                                        <textarea name="address" placeholder="Address" class="form-control"><?php if(isset($user_data['address'])) echo $user_data['address']; ?> </textarea>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="control-label">State</label>
                                        <input type="text" name="state" placeholder="State" value="<?php if(isset($user_data['state'])) echo $user_data['state']; ?>" class="form-control"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <input type="text" name="city" placeholder="City" value="<?php if(isset($user_data['city'])) echo $user_data['city']; ?>" class="form-control"/>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <label class="control-label">Zip Code</label>
                                        <input type="text" name="zip_code" placeholder="Zip Code" value="<?php if(isset($user_data['zip_code'])) echo $user_data['zip_code']; ?>" class="form-control"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Phone Number</label>
                                        <input type="text" name="phone_number" placeholder="Phone Number" value="<?php if(isset($user_data['phone_number'])) echo $user_data['phone_number']; ?>" class="form-control"/>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Secondary Phone</label>
                                        <input type="text" name="secondary_phone" placeholder="Secondary Phone" value="<?php if(isset($user_data['secondary_phone'])) echo $user_data['secondary_phone']; ?>" class="form-control"/>
                                    </div>
                                    
                                    <?php endif; ?>

                                    <div class="margiv-top-10">
                                        <button type="submit" class="btn green">Save Changes</button>
                                        <a href="#" class="btn default">Cancel</a>
                                    </div>
                                </form>
                            </div>

                            <div id="tab_3-3" class="tab-pane">
                                <form action="#">
                                    <div class="form-group">
                                        <label class="control-label">Old Password</label>
                                        <input type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">New Password</label>
                                        <input type="password" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Re-type New Password</label>
                                        <input type="password" class="form-control"/>
                                    </div>
                                    <div class="margin-top-10">
                                        <a href="#" class="btn green">Change Password</a>
                                        <a href="#" class="btn default">Cancel</a>
                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
