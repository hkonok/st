<?php
switch ($user_basic['user_type']) {
    case 0 :
        $user_type_name = 'Super Admin';
        break;
    case 1:
        $user_type_name = 'Manger';
        break;
    case 2:
        $user_type_name = 'Techniclal Manager';
        break;
    case 3:
        $user_type_name = 'Teacher Manager';
        break;
    case 4:
        $user_type_name = 'Personnel Manager';
        break;
    case 5:
        $user_type_name = 'Customer Support';
        break;
    case 6:
        $user_type_name = 'Teacher';
        break;
    case 7:
        $user_type_name = 'Student';
        break;
    default :
        $user_type_name = "";
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name'] . " " . $user_data['last_name']; ?> <small><?php echo $user_type_name; ?> Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#"><?php echo $user_type_name; ?> Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">


                                <div class="row ">

                                    <div class="col-md-8">
                                        <!-- BEGIN PORTLET-->
                                        <div class="portlet">

                                            <div class="portlet-body" id="chats">
                                                <div class="scroller" style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
                                                    <ul class="chats">
                                                        <?php
                                                        if (isset($quality)):
                                                            $i = 0;
                                                            foreach ($quality as $row):
                                                                $i++;
                                                                ?>
                                                                <li class="in">
                                                                        <?php
                                                                       $pic_name = 'customer_service.png';
                                                                        ?>
                                                                    <img class="avatar img-responsive" alt="" src="<?php echo base_url(); ?>assets/user/<?php echo $pic_name; ?>"/>
                                                                    <div class="message">
                                                                        <span class="arrow">
                                                                        </span>
                                                                        <a href="#" class="name">Customer Service
                                                                        </a>
                                                                        <span class="datetime">
                                                                            at <?php
                                                                            $datetime = new DateTime($row['datetime']); // current time = server time

                                                                            echo $datetime->format('d, M Y H:i:s');
                                                                            ?>
                                                                        </span>
                                                                        <span class="body">
                                                                            <?php echo $row['obligation']; ?>
                                                                        </span>
                                                                    </div>
                                                                </li>

                                                                <?php
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </ul>
                                                </div>
                                                <?php if(isset($user_type) && $user_type == 2): ?>
                                                <form action="" method="post">
                                                    <div class="chat-form">
                                                        <div class="input-cont">
                                                            <textarea class="form-control" type="text" name="obligation_to_the_managerial_order" placeholder="Type a message here..."/> </textarea>
                                                        </div>
                                                    </div>
                                                    <div class="btn-cont" style=" margin-top: 2px;">
                                                        <span class="arrow">
                                                        </span>
                                                        <button type="submit" class="btn blue icn-only">Send</button>
                                                    </div>
                                                </form>
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <!-- END PORTLET-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
