<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
       

        <div class="row">
            <div class="inbox-content">
					
<table class="table table-striped table-advance table-hover">
<thead>
<tr>
	<th colspan="3">
		<input type="checkbox" class="mail-checkbox mail-group-checkbox">
		<div class="btn-group">
			<a class="btn btn-sm blue" href="#" data-toggle="dropdown"> More <i class="fa fa-angle-down"></i>
			</a>
			<ul class="dropdown-menu">
				<li>
					<a href="#"><i class="fa fa-pencil"></i> Mark as Read</a>
				</li>
				<li>
					<a href="#"><i class="fa fa-ban"></i> Mark as Unread</a>
				</li>
				<li class="divider">
				</li>
				
			</ul>
		</div>
	</th>
	<th class="pagination-control" colspan="3">
		<span class="pagination-info">
			 1-30 of 789
		</span>
		<a class="btn btn-sm blue"><i class="fa fa-angle-left"></i></a>
		<a class="btn btn-sm blue"><i class="fa fa-angle-right"></i></a>
	</th>
</tr>
</thead>
<tbody>
    
<?php $i=0; foreach ($tickets as $row): $i++; ?>
    
<tr class="">

	<td class="inbox-small-cells">
		<input type="checkbox" class="mail-checkbox">
	</td>
	
	<td class="view-message hidden-xs">
           <?php if($i % 3 == 0) echo '<b>'; ?>  <?php echo $row['email']; ?> <?php if($i % 3 == 0) echo '</b>'; ?>
	</td>
        <td class="view-message ">
            <a href="<?php echo base_url(); ?>index.php/admin/customer_service_inbox_thread/<?php echo $row['id']; ?>/"> <?php if($i % 3 == 0) echo '<b>'; ?>	 <?php echo $row['name']; ?> <?php if($i % 3 == 0) echo '</b>'; ?></a>
	</td>
	<td class="view-message inbox-small-cells">
		<i class="fa fa-paper-clip"></i>
	</td>
	<td class="view-message text-right">
            <?php if($i % 3 == 0) echo '<b>'; ?> <?php echo $row['datetime']; ?> <?php if($i % 3 == 0) echo '</b>'; ?>
	</td>
</tr>
<?php endforeach; ?>

</tbody>
</table>
            </div>
            
        </div>
        
        
        
    </div>