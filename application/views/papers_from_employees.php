<?php
switch ($user_basic['user_type']) {
    case 0 :
        $user_type_name = 'Super Admin';
        break;
    case 1:
        $user_type_name = 'Manger';
        break;
    case 2:
        $user_type_name = 'Techniclal Manager';
        break;
    case 3:
        $user_type_name = 'Teacher Manager';
        break;
    case 4:
        $user_type_name = 'Personnel Manager';
        break;
    case 5:
        $user_type_name = 'Customer Support';
        break;
    case 6:
        $user_type_name = 'Teacher';
        break;
    case 7:
        $user_type_name = 'Student';
        break;
    default :
        $user_type_name = "";
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name']." ".$user_data['last_name']; ?> <small><?php echo $user_type_name; ?> Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#"><?php echo $user_type_name; ?> Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                     <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Papers Needed to Deliver
                                        </div>
                                        
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            Paper
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php $i=0; foreach($papers as $row): $i++; ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo $i; ?>
                                                        </td>
                                                        <td class="success">
                                                            <?php echo $row['paper']; ?>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach; ?>
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <?php if($user_type == 4): ?>
                                <!-- END SAMPLE TABLE PORTLET-->
                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/add_papers_from_employee/<?php echo $teacher_id; ?>/">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Add Needed Paper</label>
                                            <div class="col-md-4">
                                                <select class="form-control select2me" name="add_paper" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <option value="Personal photo">Personal photo</option>
                                                    <option value="Original educational certificate">Original educational certificate</option>
                                                    <option value="Copy of personal ID">Copy of personal ID</option>
                                                    <option value="Copy of the ID of owner of the phone that is being used for internet">Copy of the ID of owner of the phone that is being used for internet</option>
                                                    <option value="Finished filling the applications of the company">Finished filling the applications of the company</option>
                                                    <option value="Copy of the military stats">Copy of the military stats</option>
                                                    <option value="Feesh and Tashbeeh">Feesh and Tashbeeh</option>
                                                    <option value="Copy of CV">Copy of CV</option>
                                                    <option value="Original birth certificate">Original birth certificate</option>
                                                    
                                                </select>
                                                <span class="help-block">
                                                    Select the papers needed to submit
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn green">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                
                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_papers_from_employee/<?php echo $teacher_id; ?>/">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Remove Paper</label>
                                            <div class="col-md-4">
                                                <select class="form-control select2me" name="remove_paper" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <?php foreach($papers as $row): ?>
                                                    <option value="<?php echo $row['id']; ?>"><?php echo $row['paper']; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                                <span class="help-block">
                                                   submitted papers
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn green">Remove</button>
                                               
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
