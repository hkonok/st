<?php
if(isset($technical_info['user_id']))
    $is_valid = TRUE;
else 
    $is_valid = FALSE;
?>


<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name'] . " " . $user_data['last_name']; ?> <small>Teacher Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#">Teacher Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">
                                <form role="form" class="from-horizontal" method="post" action="">
                                    <div class="form-group">
                                        <label class="control-label">Personality</label>
                                        <div >
                                            <select id="select2_sample_modal_x" name="personality[]"  data-placeholder="Select..." class="form-control select2" multiple>
                                                <option value=""></option>
                                                <option value="Flexible" <?php if($is_valid && isset($personality['Flexible']) && $personality['Flexible'] == 1) echo 'selected'; ?>>Flexible</option>
                                                <option value="Nice" <?php if($is_valid && isset($personality['Nice']) && $personality['Nice'] == 1) echo 'selected'; ?>>Nice</option>
                                                <option value="Strict" <?php if($is_valid && isset($personality['Strict']) && $personality['Strict'] == 1) echo 'selected'; ?>>Strict</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Teaching Preference</label>
                                        <div >
                                            <select class="form-control select2me" name="teaching_preference"  data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="Only Adult" <?php if($is_valid && $technical_info['teaching_preference'] == 'Only Adult') echo 'selected'; ?>>Only Adult</option>
                                                <option value="Only Children" <?php if($is_valid && $technical_info['teaching_preference'] == 'Only Children') echo 'selected'; ?>>Only Children</option>
                                                <option value="Both" <?php if($is_valid && $technical_info['teaching_preference'] == 'Both') echo 'selected'; ?>>Both</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Teacher Of</label>
                                        <div >
                                            <select class="form-control select2me" name="teacher_of"  data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="Arabic" <?php if($is_valid && $technical_info['teacher_of'] == 'Arabic') echo 'selected'; ?>>Arabic</option>
                                                <option value="Quran" <?php if($is_valid && $technical_info['teacher_of'] == 'Quran') echo 'selected'; ?>>Quran</option>
                                                <option value="Arabic and Quran" <?php if($is_valid && $technical_info['teacher_of'] == 'Arabic and Quran') echo 'selected'; ?>>Arabic and Quran</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">English Level</label>
                                        <div >
                                            <select class="form-control select2me" name="english_level"  data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="None" <?php if($is_valid && $technical_info['english_level'] == 'None') echo 'selected'; ?>>None</option>
                                                <option value="Weak" <?php if($is_valid && $technical_info['english_level'] == 'Weak') echo 'selected'; ?>>Weak</option>
                                                <option value="Intermediat" <?php if($is_valid && $technical_info['english_level'] == 'Intermediat') echo 'selected'; ?>>Intermediate</option>
                                                <option value="Good" <?php if($is_valid && $technical_info['english_level'] == 'Good') echo 'selected'; ?>>Good</option>
                                                <option value="Very Good" <?php if($is_valid && $technical_info['english_level'] == 'Very Good') echo 'selected'; ?>>Very Good</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label">Other Skills</label>
                                        <div class="">
                                            <select id="select2_sample_modal_2" name='other_skill[]' class="form-control select2" multiple>
                                                <option value="">&nbsp;</option>
                                                <option value="Photoshop designer" <?php if(isset($other_skill['Photoshop designer'])) echo 'selected'; ?>>Photoshop designer</option>
                                                <option value="Spanish" <?php if(isset($other_skill['Spanish'])) echo 'selected'; ?>>Spanish</option>
                                                <option value="German" <?php if(isset($other_skill['German'])) echo 'selected'; ?>>German</option>
                                                <option value="English" <?php if(isset($other_skill['English'])) echo 'selected'; ?>>English</option>
                                                <option value="French" <?php if(isset($other_skill['French'])) echo 'selected'; ?>>French</option>
                                                <option value="Italian" <?php if(isset($other_skill['Italian'])) echo 'selected'; ?>>Italian</option>
                                                <option value="Turkish" <?php if(isset($other_skill['Turkish'])) echo 'selected'; ?>>Turkish</option>
                                                <option value="Ijaazat" <?php if(isset($other_skill['Ijaazat'])) echo 'selected'; ?>>Ijaazat</option>
                                                <option value="Teaching Azhar curriculum" <?php if(isset($other_skill['Teaching Azhar curriculum'])) echo 'selected'; ?>>Teaching Azhar curriculum</option>
                                                <option value="5+ years teaching experience" <?php if(isset($other_skill['5+ years teaching experience'])) echo 'selected'; ?>>5+ years teaching experience</option>
                                                <option value="10+ years teaching experience" <?php if(isset($other_skill['10+ years teaching experience'])) echo 'selected'; ?>>10+ years teaching experience</option>
                                                <option value="Advanced studies teacher" <?php if(isset($other_skill['Advanced studies teacher'])) echo 'selected'; ?>>Advanced studies teacher</option>
                                                <option value="Worked in other centers for teaching Arabic" <?php if(isset($other_skill['Worked in other centers for teaching Arabic'])) echo 'selected'; ?>>Worked in other centers for teaching Arabic</option>
                                                <option value="Worked in other centers for teaching Quran" <?php if(isset($other_skill['Worked in other centers for teaching Quran'])) echo 'selected'; ?>>Worked in other centers for teaching Quran</option>
                                                <option value="Worked in other centers for teaching Arabic and Quran" <?php if(isset($other_skill['Worked in other centers for teaching Arabic and Quran'])) echo 'selected'; ?>>Worked in other centers for teaching Arabic and Quran</option>

                                            </select>
                                        </div>
                                    </div>




                                    <!-- BEGIN ACCORDION PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-reorder"></i>Bio Data
							</div>
							
						</div>
						<div class="portlet-body">
							<div class="panel-group accordion scrollable" id="accordion2">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">
										 View</a>
										</h4>
									</div>
									<div id="collapse_2_1" class="panel-collapse in">
										<div class="panel-body">
                                                                                    <p>
                                                                                        <?php  if($is_valid) echo $technical_info['bio']; ?>
                                                                                    </p>
										</div>
									</div>
								</div>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2">
										Edit Bio </a>
										</h4>
									</div>
									<div id="collapse_2_2" class="panel-collapse collapse">
										<div class="panel-body">
                                                                                    <textarea class="col-md-12" name='bio' rows='20'>
                                                                                        <?php if($is_valid) echo $technical_info['bio']; ?>
                                                                                    </textarea>
										</div>
									</div>
								</div>
								
								
							</div>
						</div>
					</div>
					<!-- END ACCORDION PORTLET-->
                            <div class="margiv-top-10">
                                <button type="submit" class="btn green">Save Changes</button>
                                <a href="#" class="btn default">Cancel</a>
                            </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!--end col-md-9-->
            </div>






            <!--END TABS-->
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>
