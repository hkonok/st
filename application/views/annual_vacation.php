<?php
switch ($user_basic['user_type']) {
    case 0 :
        $user_type_name = 'Super Admin';
        break;
    case 1:
        $user_type_name = 'Manger';
        break;
    case 2:
        $user_type_name = 'Techniclal Manager';
        break;
    case 3:
        $user_type_name = 'Teacher Manager';
        break;
    case 4:
        $user_type_name = 'Personnel Manager';
        break;
    case 5:
        $user_type_name = 'Customer Support';
        break;
    case 6:
        $user_type_name = 'Teacher';
        break;
    case 7:
        $user_type_name = 'Student';
        break;
    default :
        $user_type_name = "";
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo $user_data['first_name'] . " " . $user_data['last_name']; ?> <small><?php echo $user_type_name; ?> Profile</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">

                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>

                    <li>
                        <a href="#"><?php echo $user_type_name; ?> Profile</a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row profile">
            <div class="col-md-12">
                <!--BEGIN TABS-->





                <div class="row profile-account">
                    <?php echo $sub_menu_data; ?>
                    <div class="col-md-9">
                        <div class="tab-content">
                            <div id="tab_1-1" class="tab-pane active">

                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-comments"></i>Annual Vacations
                                        </div>

                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>
                                                            #
                                                        </th>
                                                        <th>
                                                            From
                                                        </th>
                                                        <th>
                                                            To
                                                        </th>

                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 0;
                                                    foreach ($vacation_list as $row): $i++;
                                                        $datetime = new DateTime($row['from']); // current time = server time
                                                        
                                                        $post_time = $datetime->format('M, d');
                                                        $from = $post_time;
                                                        
                                                        $datetime = new DateTime($row['to']); // current time = server time
                                                        
                                                        $post_time = $datetime->format('M, d');
                                                        $to = $post_time;
                                                        ?>
                                                        <tr>
                                                            <td>
                                                                <?php echo $i; ?>
                                                            </td>
                                                            <td class="success">
                                                                <?php echo $from; ?>
                                                            </td>
                                                            <td class="warning">
                                                                <?php echo $to; ?>
                                                            </td>

                                                        </tr>
                                                    <?php endforeach; ?>


                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- END SAMPLE TABLE PORTLET-->

                                <?php if($user_type == 4 || $user_type == 3): ?>
                                <br><br>
                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/add_annual_vacation/<?php echo $teacher_id; ?>/">
                                    <div class="form-group">
                                        <div class="row">
                                            <label class="control-label col-md-3">Add Annual Vacation</label>
                                            <div class="col-md-4">
                                                <div class="input-group input-large date-picker input-daterange" data-date-format="mm-dd">
                                                    <input type="text" class="form-control" name="from">
                                                    <span class="input-group-addon">
                                                        to
                                                    </span>
                                                    <input type="text" class="form-control" name="to">
                                                </div>
                                                <!-- /input-group -->
                                                <span class="help-block">
                                                    Select date range
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn green">Add</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <br><br>
                                <form role="form" class="from-horizontal" method="post" action="<?php echo base_url(); ?>index.php/admin/remove_annual_vacation/<?php echo $teacher_id; ?>/">
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">Remove a Date Range</label>
                                            <div class="col-md-4">
                                                <select class="form-control select2me" name="remove_vacation" data-placeholder="Select...">
                                                    <option value=""></option>
                                                    <?php foreach($vacation_list as $row):
                                                        $datetime = new DateTime($row['from']); // current time = server time
                                                        
                                                        $post_time = $datetime->format('M, d');
                                                        $from = $post_time;
                                                        
                                                        $datetime = new DateTime($row['to']); // current time = server time
                                                        
                                                        $post_time = $datetime->format('M, d');
                                                        $to = $post_time;
                                                        
                                                        ?>
                                                    <option value="<?php echo $row['id']; ?>"> <?php echo $from." - ".$to; ?></option>
                                                    <?php endforeach; ?>
                                                    
                                                </select>
                                                <span class="help-block">
                                                    Select a date range to remove
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-4">
                                                <button type="submit" class="btn green">Remove</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <!--end col-md-9-->
                </div>






                <!--END TABS-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
