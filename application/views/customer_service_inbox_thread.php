<?php
$name = "";
if(isset($user_data['user_id'])){
    $name = $user_data['first_name']." ".$user_data['last_name'];
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">


        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                 <?php echo $user_basic['email'];?> <small> <?php echo $ticket['datetime']; ?></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php echo base_url(); ?>index.php/admin/customer_service_inbox/">Inbox</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#"><?php echo $ticket['name']; ?></a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="fa fa-calendar"></i>
                            <span>
                            </span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <div class="clearfix">
        </div>

        <div class="row ">

            <div class="col-md-8">
                <!-- BEGIN PORTLET-->
                <div class="portlet">

                    <div class="portlet-body" id="chats">
                        <div class="scroller" style="height: 435px;" data-always-visible="1" data-rail-visible1="1">
                            <ul class="chats">
                                <?php
                                if (isset($ticket_message)):
                                    $i = 0;
                                    foreach ($ticket_message as $row):
                                        $i++;
                                        ?>
                                        <li class="<?php
                                        if ($row['from'] == $ticket['user_id'])
                                            echo 'in';
                                        else
                                            echo 'out';
                                        ?>">
                                                <?php
                                                if ($row['from'] == $ticket['user_id'])
                                                    $pic_name = 'student.png';
                                                else
                                                    $pic_name = 'customer_service.png';
                                                ?>
                                            <img class="avatar img-responsive" alt="" src="<?php echo base_url(); ?>assets/user/<?php echo $pic_name; ?>"/>
                                            <div class="message">
                                                <span class="arrow">
                                                </span>
                                                <a href="#" class="name"><?php
                                                    if ($row['from'] == $ticket['user_id'])
                                                        echo $name;
                                                    else 
                                                        echo "Customer Service";
                                                    ?>
                                                </a>
                                                <span class="datetime">
                                                    at <?php
                                                    $datetime = new DateTime($row['datetime']); // current time = server time

                                                    echo $datetime->format('d, M Y H:i:s');
                                                    ?>
                                                </span>
                                                <span class="body">
                                                    <?php echo $row['message']; ?>
                                                </span>
                                            </div>
                                        </li>

                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </ul>
                        </div >
                        <?php if(isset($usre_type) && $user_type==5): ?>
                        <form action="" method="post">
                            <div class="chat-form">
                                <div class="input-cont">
                                    <textarea class="form-control" type="text" name="message" placeholder="Type a message here..."/> </textarea>
                                </div>
                            </div>
                            <div class="btn-cont" style=" margin-top: 2px;">
                                <span class="arrow">
                                </span>
                                <button type="submit" class="btn blue icn-only">Send</button>
                            </div>
                        </form>
                        <?php endif;?>
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT -->
</div>
<!-- END CONTAINER -->
