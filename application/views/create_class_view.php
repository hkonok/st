<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">
        <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        Widget settings form goes here
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn blue">Save changes</button>
                        <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
        <!-- BEGIN PAGE CONTENT-->


        <div class="row">
            <div class="col-md-12">
                <div class="portlet box green">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-reorder"></i>Create New Class
                        </div>

                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form action="<?php echo base_url(); ?>index.php/admin/create_class/" method="post" class="form-horizontal form-row-sepe" >
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Select Teacher</label>
                                    <div class="col-md-4">
                                        <select  name="teacher" class="form-control input-large select2me" data-placeholder="Select...">
                                            <option value=""></option>
                                            <?php
                                            if (isset($teacher)):
                                                foreach ($teacher as $row):
                                                    ?>
                                                    <option value="<?php echo $row->id; ?>"><?php echo $row->first_name." ".$row->last_name; ?></option>

                                                    <?php
                                                endforeach;
                                            endif;
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Course</label>
                                        <div class="col-md-4">
                                            <select name="course" class="form-control input-large select2me" data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="1">Arabic</option>
                                                <option value="2">Quran</option>
                                                <option value="3">Arabic & Quran</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Level</label>
                                        <div class="col-md-4">
                                            <select name="level" class="form-control input-large select2me" data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="1">Beginner</option>
                                                <option value="6">Intermediate</option>
                                                <option value="13">Advanced</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Skills Should be in Teacher</label>
                                        <div class="col-md-4">
                                            <select name="teacher_skill" class="form-control input-large select2me" data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="1">Good English</option>
                                                <option value="2">Nice</option>
                                                <option value="3">Good With Kids</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Study Hours/Week</label>
                                        <div class="col-md-4">
                                            <select name="study_hours" class="form-control input-large select2me" data-placeholder="Select...">
                                                <option value=""></option>
                                                <option value="1">Light</option>
                                                <option value="2">Standard</option>
                                                <option value="3">Intensive</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">Start Date</label>
                                        <div class="col-md-3">
                                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                <input name="start_date" type="text" class="form-control" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="control-label col-md-3">End Date</label>
                                        <div class="col-md-3">
                                            <div class="input-group input-medium date date-picker" data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                <input name="end_date" type="text" class="form-control" readonly>
                                                <span class="input-group-btn">
                                                    <button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group last">
                                        <label class="control-label col-md-3">Select Student</label>
                                        <div class="col-md-9">
                                            <select name="students[]" class="multi-select" multiple="" id="my_multi_select3">
                                                <?php
                                                if (isset($student)) :
                                                    foreach ($student as $row):
                                                        ?>
                                                        <option value="<?php echo $row->id; ?>"><?php echo $row->first_name." ".$row->last_name; ?></option>
                                               <?php
                                                        endforeach;
                                                endif;
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-large green"><i class="fa fa-check"></i> Create</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        
                    </div>
                            </form>
                </div>
                <!-- END PAGE CONTENT-->
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
