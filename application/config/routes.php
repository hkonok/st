<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/
$route['manager_list'] = 'admin/manager_list/';
$route['technical_manager_list'] = 'admin/technical_manager_list/';
$route['teacher_manager_list'] = 'admin/teacher_manager_list/';
$route['personal_manager_list'] = 'admin/personal_manager_list/';
$route['teacher_list'] = 'admin/teacher_list/';
$route['student_list'] = 'admin/student_list/';
$route['customer_service_list'] = 'admin/customer_service_list/';
$route['create_class'] = 'admin/create_class/';
$route['class_list'] = 'admin/class_list/';
$route['home'] = 'admin/home/';

$route['default_controller'] = "admin";
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */