<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Runningclass {
    public $student_name;
    public $course_name;
    public $course_leve;
    public $class_id;
    public $datetime;
    
    public function __construct(){
        
    }
    
    public function setValue($class_id, $course_name, $course_level, $student_name, $datetime){
        $this->class_id = $class_id;
        $this->course_name = $course_name;
        $this->course_leve = $course_level;
        $this->student_name = $student_name;
        $this->datetime = $datetime;
    }
    
    public function printValue(){
        return "".$this->course_name." - ".$this->course_leve." ( ".$this->student_name. " )";
    }
    
    
}

/* End of file RunningClass.php */