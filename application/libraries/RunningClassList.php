<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 
include_once("./application/libraries/Runningclass.php");
class Runningclasslist {
   public $list = array();
   public $count = 0;
    
   public function addElement($class_id, $course_name, $course_level, $student_name, $datetime){
       $element = new Runningclass();
       $element->setValue($class_id, $course_name, $course_level, $student_name, $datetime);
       $this->list[$this->count] = $element;
       $this->count++;
   }
   
   public function size(){
       return $this->count;
   }
   
   public function getElement($index){
       return $this->list[$index];
   }
   
   public function sortList(){
       usort($this->list, "RunningClassList::compareObj");
   }
   
   public static function compareObj($a, $b){
       return strcasecmp($a,$b);
   }
   
   
   
   
    
}

/* End of file RunningClassList.php */