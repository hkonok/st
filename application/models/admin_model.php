<?php

class Admin_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    function sign_in($email, $password) {

        $data = $this->db->where('email', $email)
                ->where('password', $password)
                ->get('users')
                ->row_array();
        if (!isset($data['id']))
            return -1;
        else
            return $data;
    }

    function get_user_data($user_id, $user_type) {
        $db_name = '';
        switch ($user_type) {
            case '0':
                $db_name = 'super_admin';
                break;
            case '1':
                $db_name = 'manager';
                break;
            case '2':
                $db_name = 'technical_manager';
                break;
            case '3':
                $db_name = 'teacher_manager';
                break;
            case '4':
                $db_name = 'personal_manager';
                break;
            case '5':
                $db_name = 'customer_service';
                break;
            case '6':
                $db_name = 'teacher';
                break;
            case '7':
                $db_name = 'student';
                break;
        }
        $data = $this->db->where('id', $user_id)
                ->get($db_name)
                ->row_array();
        return $data;
    }

    function update_user_profile($user_id, $first_name, $last_name, $email, $time_zone, $country, $birthday, $gender) {
        $data = $this->get_one_user($user_id);
        $user_data = $this->get_user_data($user_id, $data['user_type']);

        if ($country == '') {
            $country = $user_data['country'];
        }

        $db_name = '';
        switch ($data['user_type']) {
            case '0':
                $db_name = 'super_admin';
                break;
            case '1':
                $db_name = 'manager';
                break;
            case '2':
                $db_name = 'technical_manager';
                break;
            case '3':
                $db_name = 'teacher_manager';
                break;
            case '4':
                $db_name = 'personal_manager';
                break;
            case '5':
                $db_name = 'customer_service';
                break;
            case '6':
                $db_name = 'teacher';
                break;
            case '7':
                $db_name = 'student';
                break;
        }

        $users_data = array(
            'email' => $email
        );


        if ($data['user_type'] != 6 && $data['user_type'] != 7) {
            $details = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'time_zone' => $time_zone,
                'country' => $country
            );
        } else {
            $details = array(
                'first_name' => $first_name,
                'last_name' => $last_name,
                'time_zone' => $time_zone,
                'country' => $country,
                
            );
        }
        $this->db->where('id', $user_id)
                ->update('users', $users_data);

        $this->db->where('id', $user_id)
                ->update($db_name, $details);
    }

    function get_one_user($user_id) {

        $data = $this->db->where('id', $user_id)
                ->get('users')
                ->row_array();
        if (!isset($data['id']))
            return -1;
        else
            return $data;
    }

    function create_manager() {
        $user_data = array(
            'user_type' => 1,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );
        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );
        $insert2 = $this->db->insert('manager', $manager_data);

        if ($insert1 && $insert2) {
            return TRUE;
        }
    }

    function create_technical_manager() {
        $user_data = array(
            'user_type' => 2,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );


        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );
        $insert2 = $this->db->insert('technical_manager', $manager_data);

        if ($insert1 && $insert2) {
            return TRUE;
        }
    }

    function create_teacher_manager() {
        $user_data = array(
            'user_type' => 3,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );
        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );


        $insert2 = $this->db->insert('teacher_manager', $manager_data);

        if ($insert1 && $insert2) {
            return TRUE;
        }
    }

    function create_personal_manager() {
        $user_data = array(
            'user_type' => 4,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );


        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );


        $insert2 = $this->db->insert('personal_manager', $manager_data);

        if ($insert1 && $insert2) {
            return TRUE;
        }
    }

    function create_customer_service() {
        $user_data = array(
            'user_type' => 5,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );


        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $c_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );


        $insert2 = $this->db->insert('customer_service', $c_data);

        if ($insert1 && $insert2) {
            return TRUE;
        }
    }

    function create_teacher() {


        $user_data = array(
            'user_type' => 6,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );


        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $teacher_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );


        $insert2 = $this->db->insert('teacher', $teacher_data);

        if ($insert1 && $insert2) {
            return TRUE;
        }
    }

    function create_student() {
        $user_data = array(
            'user_type' => 7,
            'email' => $this->input->post('email', true),
            'password' => md5($this->input->post('password', true)),
            'reg_datetime' => date('Y-m-d H:i:s'),
            'status' => 1
        );


        $insert1 = $this->db->insert('users', $user_data);
        $id = $this->db->insert_id();
        $student_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'id' => $id
        );


        $insert2 = $this->db->insert('student', $student_data);
        /*
        if ($insert1 && $insert2) {
            return TRUE;
        }
         * 
         */
        return $id;
    }

    function get_manager_list() {
        $data = $this->db->join('users', 'manager.id=users.id')
                ->get('manager')
                ->result();
        return $data;
    }

    function get_personal_manager_list() {
        $this->db->join('users', 'personal_manager.id=users.id');
        $query = $this->db->get('personal_manager');
        return $query->result();
    }

    function get_teacher_manager_list() {
        $this->db->join('users', 'teacher_manager.id=users.id');
        $query = $this->db->get('teacher_manager');
        return $query->result();
    }

    function get_technical_manager_list() {
        $this->db->join('users', 'technical_manager.id=users.id');
        $query = $this->db->get('technical_manager');
        return $query->result();
    }

    function get_teacher_list() {
        $this->db->join('users', 'teacher.id=users.id');
        $query = $this->db->get('teacher');
        return $query->result();
    }

    function get_student_list() {
        $this->db->join('users', 'student.id=users.id');
        $query = $this->db->get('student');
        return $query->result();
    }

    function get_customer_service_list() {
        $this->db->join('users', 'customer_service.id=users.id');
        $query = $this->db->get('customer_service');
        return $query->result();
    }

    function create_class() {
        $class_data = array(
            'teacher_id' => $this->input->post('teacher', true),
            'start_time' => $this->input->post('start_time', true),
            'finishing_time' => $this->input->post('finishing_time', true),
            'days' => $this->input->post('days', true)
        );
        $insert1 = $this->db->insert('class', $class_data);
        $id = $this->db->insert_id();

        foreach ($this->input->post("students", true) as $st) {

            $c_data = array(
                'student_id' => $st,
                'class_id' => $id
            );
            $insert1 = $this->db->insert('class_student', $c_data);
        }
        return $insert1;
    }

    function update_manager($id) {
        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('manager', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_personal_manager($id) {
        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('personal_manager', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_teacher_manager($id) {
        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'time_zone' => $this->input->post('time_zone', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('teacher_manager', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_technical_manager($id) {
        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true),
            'time_zone' => $this->input->post('time_zone', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('technical_manager', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_manager_password($id) {

        $user_data = array(
            'password' => md5($this->input->post('password', true))
        );


        $this->db->where('id', $id);
        $up = $this->db->update('users', $user_data);

        return $up;
    }

    function update_teacher($id) {


        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('teacher', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_teacher_password($id) {

        $user_data = array(
            'password' => md5($this->input->post('password', true))
        );


        $this->db->where('id', $id);
        $up = $this->db->update('users', $user_data);

        return $up;
    }

    function update_student($id) {


        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('student', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_student_password($id) {

        $user_data = array(
            'password' => md5($this->input->post('password', true))
        );


        $this->db->where('id', $id);
        $up = $this->db->update('users', $user_data);

        return $up;
    }

    function update_customer_service($id) {


        $user_data = array(
            'email' => $this->input->post('email', true)
        );

        $this->db->where('id', $id);
        $up1 = $this->db->update('users', $user_data);

        $manager_data = array(
            'first_name' => $this->input->post('firstname', true),
            'last_name' => $this->input->post('lastname', true)
        );

        $this->db->where('id', $id);
        $up2 = $this->db->update('customer_service', $manager_data);

        if ($up1 && $up2) {
            return TRUE;
        }
    }

    function update_customer_service_password($id) {

        $user_data = array(
            'password' => md5($this->input->post('password', true))
        );


        $this->db->where('id', $id);
        $up = $this->db->update('users', $user_data);

        return $up;
    }

    public function student_remove_pending_request($request_id) {
        $this->db->where('request_id', $request_id)
                ->delete('request_course');

        $this->db->where('request_id', $request_id)
                ->delete('request_schedule');
    }

    public function student_remove_pending_class($request_id) {
        $this->db->where('id', $request_id)
                ->delete('class');
    }

    public function insert_request_course($user_id, $course_id, $level_id, $skill_id, $study_hours, $start_date, $request_datetime) {
        $prev_data = $this->db->where('user_id', $user_id)
                ->where('course_id', $course_id)
                ->where('status', 0)
                ->get('request_course')
                ->row_array();
        if (isset($prev_data['user_id'])) {
            return -1;
        }

        $data = array(
            'user_id' => $user_id,
            'course_id' => $course_id,
            'level' => $level_id,
            'skills_in_teacher' => $skill_id,
            'study_hours' => $study_hours,
            'start_date' => $start_date,
            'request_datetime' => $request_datetime,
            'status' => 0
        );
        $this->db->insert('request_course', $data);
        return $this->db->insert_id();
    }

    public function update_request_course_data($request_id, $course, $level, $teacher_skill, $study_hours) {
        $data = array(
            'course_id' => $course,
            'level' => $level,
            'skills_in_teacher' => $teacher_skill,
            'study_hours' => $study_hours
        );
        $this->db->where('request_id', $request_id)
                ->update('request_course', $data);
    }

    public function insert_request_course_schedule($request_id, $days, $class_time) {
        foreach ($days as $row) {
            $data = array(
                'request_id' => $request_id,
                'day' => $row,
                'time' => $class_time
            );
            $this->db->insert('request_schedule', $data);
        }
    }

    public function add_student_schedule_time($request_id, $day, $time) {
        $data = array(
            'request_id' => $request_id,
            'day' => $day,
            'time' => $time
        );
        $this->db->insert('request_schedule', $data);
    }

    public function remove_student_schedule_time($id) {
        $this->db->where('id', $id)
                ->delete('request_schedule');
    }

    public function get_request_list($user_id, $status) {
        $data = $this->db->where('user_id', $user_id)
                ->where('status', $status)
                ->order_by('course_id')
                ->get('request_course')
                ->result_array();
        return $data;
    }

    public function get_one_request_list($user_id, $request_id, $status) {
        $data = $this->db->where('user_id', $user_id)
                ->where('request_id', $request_id)
                ->where('status', $status)
                ->get('request_course')
                ->row_array();
        return $data;
    }

    public function get_one_request_data($request_id) {
        $data = $this->db->where('request_id', $request_id)
                ->get('request_course')
                ->row_array();
        return $data;
    }

    public function get_request_schedule($request_id) {
        $data = $this->db->where('request_id', $request_id)
                ->get('request_schedule')
                ->result_array();
        return $data;
    }

    public function create_new_ticket($subject, $message, $user_id, $user_type, $request_datetime) {
        $ticket_data = array(
            'name' => $subject,
            'user_id' => $user_id,
            'user_type' => $user_type,
            'datetime' => $request_datetime,
            'status' => '1'
        );
        $this->db->insert('ticket', $ticket_data);

        $ticket_id = $this->db->insert_id();

        $message_data = array(
            'ticket_id' => $ticket_id,
            'from' => $user_id,
            'datetime' => $request_datetime,
            'message' => $message
        );

        $this->db->insert('ticket_message', $message_data);
    }

    public function get_tickets($user_id) {
        $data = $this->db->where('user_id', $user_id)
                ->get('ticket')
                ->result_array();
        return $data;
    }

    public function get_ticket_message($ticket_id) {
        $data = $this->db->where('ticket_id', $ticket_id)
                ->get('ticket_message')
                ->result_array();
        return $data;
    }

    public function get_ticket_data($ticket_id) {
        $data = $this->db->where('id', $ticket_id)
                ->get('ticket')
                ->row_array();
        return $data;
    }

    public function insert_ticket_message($ticket_id, $message, $datetime, $user_id) {
        $data = array(
            'ticket_id' => $ticket_id,
            'datetime' => $datetime,
            'message' => $message,
            'from' => $user_id
        );
        $this->db->insert('ticket_message', $data);
        return $this->db->insert_id();
    }

    public function activate_class($class_id) {
        $data = array('status' => 1);
        
        $this->db->where('id', $class_id)
                 ->update('class', $data);
    }
    
    public function deactivate_class($class_id) {
        $data = array('status' => 0);
        
        $this->db->where('id', $class_id)
                 ->update('class', $data);
    }
    
    public function remove_class($class_id){
        $this->db->where('id', $class_id)
                 ->delete('class');
        
        $this->db->where('class_id', $class_id)
                 ->delete('class_schedule');
        
        $this->db->where('class_id', $class_id)
                 ->delete('class_student');
    }

    public function get_running_class_list($user_id) {
        $data = $this->db->select("*")
                ->where('class_student.student_id', $user_id)
                ->where('class.status', 1)
                ->from('class_student')
                ->join('class', ' class.id = class_student.class_id')
                ->get()
                ->result_array();
        return $data;
    }

    public function get_completted_class_list($user_id) {
        $data = $this->db->select("*")
                ->where('class_student.student_id', $user_id)
                ->where('class.status', 2)
                ->from('class_student')
                ->join('class', ' class.id = class_student.class_id')
                ->get()
                ->result_array();
        return $data;
    }

    public function student_class_validity($class_id, $user_id) {
        $data = $this->db->where('student_id', $user_id)
                ->where('class_id', $class_id)
                ->get('class_student')
                ->row_array();
        if (isset($data['class_id']))
            return TRUE;
        else
            return FALSE;
    }

    public function teacher_class_validity($class_id, $user_id) {
        $data = $this->db->where('teacher_id', $user_id)
                ->where('id', $class_id)
                ->get('class')
                ->row_array();
        if (isset($data['id']))
            return TRUE;
        else
            return FALSE;
    }

    public function get_class_data($class_id) {
        $data = $this->db->where('id', $class_id)
                ->get('class')
                ->row_array();
        return $data;
    }

    public function get_class_schedule($class_id) {
        $data = $this->db->where('class_id', $class_id)
                ->get('class_schedule')
                ->result_array();
        return $data;
    }

    public function insert_class_schedule($class_id, $day, $time) {
       
        $data = array(
            'class_id' => $class_id,
            'day' => $day,
            'time' => $time
        );
        $this->db->insert('class_schedule', $data);
    }

    
    public function insert_class_student_attendance($class_id, $student_id, $attendance, $datetime){
        $data = array(
            'class_id' => $class_id,
            'student_id' => $student_id,
            'attendance' => $attendance,
            'datetime' => $datetime
        );
        $this->db->insert('class_attendance_student', $data);
    }
    
    public function insert_class_teacher_attendance($class_id, $teacher_id, $attendance, $datetime){
        $data = array(
            'class_id' => $class_id,
            'teacher_id' => $teacher_id,
            'attendance' => $attendance,
            'datetime' => $datetime
        );
        $this->db->insert('class_attendance_teacher', $data);
    }
    
    public function get_class_students($class_id) {
        $data = $this->db->where('class_id', $class_id)
                ->get('class_student')
                ->result_array();
        return $data;
    }
    
    public function get_class_students_data($class_id) {
        $data = $this->db->where('class_id', $class_id)
                         ->from('class_student')
                         ->join('student', 'student.id = class_student.student_id')
                         ->join('users', 'student.id = users.id')
                         ->get()
                         ->result_array();
        return $data;
    }

    public function get_class_message($class_id) {
        $data = $this->db->where('class_id', $class_id)
                ->get('class_message')
                ->result_array();
        return $data;
    }

    public function insert_class_message($message, $user_id, $class_id, $request_datetime) {
        $data = array(
            'message' => $message,
            'from' => $user_id,
            'class_id' => $class_id,
            'datetime' => $request_datetime
        );
        $this->db->insert('class_message', $data);

        return $this->db->insert_id();
    }

    public function get_classes_teacher($user_id, $status) {
        $data = $this->db->where('teacher_id', $user_id)
                ->where('status', $status)
                ->get('class')
                ->result_array();
        return $data;
    }

    public function update_teacher_profile($teacher_id, $first_name, $last_name, $email, $birthday, $time_zone, $date_of_contract, $bank_account) {
        $data = array(
            'first_name' => $first_name,
            'last_name' => $last_name,
            'birthday' => $birthday,
            'time_zone' => $time_zone,
            'date_of_contract' => $date_of_contract,
            'bank_account' => $bank_account
        );
        $this->db->where('id', $teacher_id)
                ->update('teacher', $data);
    }

    public function update_user_email($user_id, $email) {
        $data = array('email' => $email);
        $this->db->where('id', $user_id)
                ->update('users', $data);
    }

    public function item_received_add_item($user_id, $item, $date) {
        $data = array(
            'user_id' => $user_id,
            'item' => $item,
            'date' => $date
        );
        $this->db->insert('items_received', $data);
    }

    public function item_received_return_item($user_id, $item_id) {
        $this->db->where('id', $item_id)
                ->where('user_id', $user_id)
                ->delete('items_received');
    }

    public function get_items_received($user_id) {
        $data = $this->db->where('user_id', $user_id)
                ->get('items_received')
                ->result_array();
        return $data;
    }

    public function get_papers_from_employee($user_id) {
        $data = $this->db->where('user_id', $user_id)
                ->get('paper_from_employee')
                ->result_array();
        return $data;
    }

    public function add_papers_from_employee($user_id, $paper) {
        $data = array(
            'user_id' => $user_id,
            'paper' => $paper
        );
        $this->db->insert('paper_from_employee', $data);
    }

    public function remove_papers_from_employee($user_id, $paper_id) {
        $this->db->where('user_id', $user_id)
                ->where('id', $paper_id)
                ->delete('paper_from_employee');
    }

    public function get_annual_vacation($user_id) {
        $data = $this->db->where('user_id', $user_id)
                ->get('annual_vacation')
                ->result_array();
        return $data;
    }

    public function add_annual_vacation($user_id, $from, $to) {
        $data = array(
            'user_id' => $user_id,
            'from' => $from,
            'to' => $to
        );
        $this->db->insert('annual_vacation', $data);
    }

    public function remove_annual_vacation($user_id, $vacation_id) {
        $this->db->where('user_id', $user_id)
                ->where('id', $vacation_id)
                ->delete('annual_vacation');
    }

    public function get_teacher_technical_info($user_id) {
        $data = $this->db->where('user_id', $user_id)
                ->get('teacher_technical_info')
                ->row_array();
        return $data;
    }

    public function insert_teacher_technical_info($user_id, $personality, $teaching_preference, $teacher_of, $english_level, $other_skill, $bio) {
        $data = array(
            'user_id' => $user_id,
            'personality' => $personality,
            'teaching_preference' => $teaching_preference,
            'teacher_of' => $teacher_of,
            'english_level' => $english_level,
            'other_skill' => $other_skill,
            'bio' => $bio
        );

        $check_data = $this->db->where('user_id', $user_id)
                ->get('teacher_technical_info')
                ->row_array();
        if (isset($check_data['user_id'])) {
            $this->db->where('user_id', $user_id)
                    ->update('teacher_technical_info', $data);
        } else {
            $this->db->insert('teacher_technical_info', $data);
        }
    }

    public function get_all_tickets() {
        $data = $this->db->from('users')
                ->join('ticket', 'users.id = ticket.user_id')
                ->get()
                ->result_array();
        return $data;
    }

    public function get_teacher_schedule_time($teacher_id) {
        $data = $this->db->where('user_id', $teacher_id)
                ->get('teacher_schedule_time')
                ->result_array();
        return $data;
    }

    public function add_teacher_schedule_time($user_id, $day, $from, $to) {
        $data = array(
            'user_id' => $user_id,
            'day' => $day,
            'from' => $from,
            'to' => $to
        );
        $this->db->insert('teacher_schedule_time', $data);
    }

    public function remove_teacher_schedule_time($id) {

        $this->db->where('id', $id)
                ->delete('teacher_schedule_time', $data);
    }

    public function get_teacher_quality($teacher_id) {
        $data = $this->db->where('user_id', $teacher_id)
                ->get('teacher_quality_monitoring')
                ->result_array();
        return $data;
    }

    public function add_teacher_quality_monitoring($teacher_id, $user_id, $quality, $request_datetime) {
        $data = array(
            'user_id' => $teacher_id,
            'quality' => $quality,
            'datetime' => $request_datetime,
            'from' => $user_id
        );
        $this->db->insert('teacher_quality_monitoring', $data);
    }

    public function add_teacher_monitoring_class($teacher_id, $user_id, $quality, $request_datetime) {
        $data = array(
            'user_id' => $teacher_id,
            'monitor' => $quality,
            'datetime' => $request_datetime,
            'from' => $user_id
        );
        $this->db->insert('teacher_monitoring_class', $data);
    }

    public function get_teacher_monitoring_class($teacher_id) {
        $data = $this->db->where('user_id', $teacher_id)
                ->get('teacher_monitoring_class')
                ->result_array();
        return $data;
    }

    public function get_teacher_obligation_to_the_managerial_order($teacher_id) {
        $data = $this->db->where('user_id', $teacher_id)
                ->get('obligation_to_the_managerial_order')
                ->result_array();

        return $data;
    }

    public function add_teacher_obligation_to_the_managerial_order($teacher_id, $user_id, $quality, $request_datetime) {
        $data = array(
            'user_id' => $teacher_id,
            'obligation' => $quality,
            'datetime' => $request_datetime,
            'from' => $user_id
        );
        $this->db->insert('obligation_to_the_managerial_order', $data);
    }

    public function change_user_status($user_id) {
        $data = $this->db->where('id', $user_id)
                ->get('users')
                ->row_array();
        if (isset($data['status'])) {
            if ($data['status'] == 1)
                $status = 0;
            else
                $status = 1;
        }
        $data2 = array('status' => $status);
        $this->db->where('id', $user_id)
                ->update('users', $data2);
    }

    public function change_password($user_id, $password) {
        $data = array('password' => $password);
        $this->db->where('id', $user_id)
                ->update('users', $data);
    }

    public function get_quiz_marks($class_id, $user_id) {
        $data = $this->db->where('class_id', $class_id)
                ->where('user_id', $user_id)
                ->order_by('date', 'asc')
                ->get('quiz_marks')
                ->result_array();

        return $data;
    }

    public function add_class_marks($class_id, $student, $topic, $total_marks, $obtain_marks, $start_date) {
        $data = array(
            'class_id' => $class_id,
            'user_id' => $student,
            'topic' => $topic,
            'total_marks' => $total_marks,
            'obtain_marks' => $obtain_marks,
            'date' => $start_date
        );
        $this->db->insert('quiz_marks', $data);
    }

    public function remove_quiz_marks($remove_id) {
        $this->db->where('id', $remove_id)
                ->delete('quiz_marks');
    }

    public function deactivate_request($request_id){
        $data = array('status' => 1);
        $this->db->where('request_id', $request_id)
                 ->update('request_course', $data);
    }
    
    public function activate_request($request_id){
        $data = array('status' => 0);
        $this->db->where('request_id', $request_id)
                 ->update('request_course', $data);
    }
    
    public function get_request_list_all() {
        $data = $this->db->where('status', 0)
                ->get('request_course')
                ->result_array();

        return $data;
    }

    public function get_course_teachers($request_id) {
        $request_data = $this->get_one_request_data($request_id);

        if ($request_data['course_id'] == 1)
            $course_name = "Arabic";
        else if ($request_data['course_id'] == 2)
            $course_name = "Quran";
        else
            $course_name = "Arabic and Quran";

        $data = $this->db->where('teacher_of', $course_name)
                ->from('teacher_technical_info')
                ->join('teacher', 'teacher.id = teacher_technical_info.user_id')
                ->join('users', 'teacher.id = users.id')
                ->get()
                ->result_array();
        return $data;
    }

    public function get_course_teacher_schedule($teacher_id) {

        $class_data = $this->db->where('teacher_id', $teacher_id)
                ->where('status', '1')
                ->get('class')
                ->result_array();
        $i = 0;
        $tmp = "";
        foreach ($class_data as $row) {
            $tmp2 = "";
            $schedule_data = $this->get_class_schedule($row['id']);
            $flag = 0;
            foreach ($schedule_data as $row2) {
                if ($flag == 0)
                    $tmp2 = $tmp2 . "( ";
                else
                    $tmp2 = $tmp2 . ", ";

                $tmp2 = $tmp2 . $row2['day'] . " - " . $row2['time'];

                $flag++;
            }
            if ($flag > 0) {
                $tmp2 = $tmp2 . " )<br>";
            }
            $tmp = $tmp2;
            $i++;
        }

        return $tmp;
    }

    public function create_empty_class($request_id, $course_id, $level_id, $skill_id, $study_hours, $start_date, $request_datetime) {
        $data = array(
            'id' => $request_id,
            'course_id' => $course_id,
            'level' => $level_id,
            'teacher_id' => -1,
            'start_date' => $start_date,
            'end_date' => "0000-00-00 00:00:00",
            'study_hours' => $study_hours,
            'skill_in_teacher' => $skill_id,
            'status' => '0'
        );

        $this->db->insert('class', $data);
    }
    
    public function sm_create_class($request_id, $course_id, $level_id, $skill_id, $study_hours, $start_date, $end_date, $request_datetime, $teacher) {
        $data = array(
            'id' => $request_id,
            'course_id' => $course_id,
            'level' => $level_id,
            'teacher_id' => $teacher,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'end_date' => "0000-00-00 00:00:00",
            'study_hours' => $study_hours,
            'skill_in_teacher' => $skill_id,
            'status' => '0'
        );

        $this->db->insert('class', $data);
    }

    public function get_more_class_students($class_id, $user_id) {
        $data = $this->db->where('class_id', $class_id)
                ->where('student_id !=', $user_id)
                ->from('class_student')
                ->join('student', 'class_student.student_id = student.id')
                ->join('users', 'users.id = student.id')
                ->get()
                ->result_array();
        return $data;
    }
    
    public function get_student_running_class($user_id ){
        $data = $this->db->where('student_id', $user_id)
                         ->from('class_student')
                         ->join('class', 'class.id = class_student.class_id AND class.status = 1')
                         ->get()
                         ->result_array();
        
        return $data;
    }
    
    public function get_teacher_running_class($user_id ){
        $data = $this->db->where('teacher_id', $user_id)
                         ->where('status', 1)
                         ->get('class')
                         ->result_array();
        
        return $data;
    }
    
    public function get_class_file($class_id){
        $data = $this->db->where('class_id', $class_id)
                         ->get('class_file')
                         ->result_array();
        return $data;
    }
    
    public function insert_class_file($class_id, $user_id, $file_name, $datetime){
        $data = array(
            'class_id' => $class_id,
            'user_id' => $user_id,
            'file_name' => $file_name,
            'file_url' => $file_name,
            'datetime' => $datetime
        );
        $this->db->insert('class_file', $data);
        return $this->db->insert_id();
    }
    
    public function update_class_file_url($file_id, $url){
        $data = array('file_url' => $url);
        $this->db->where('id', $file_id)
                 ->update('class_file', $data);
    }

    public function get_more_students($course_id, $level, $request_user_id) {
        $data = $this->db->where('course_id', $course_id)
                ->where('level', $level)
                ->where('user_id !=', $request_user_id)
                ->from('request_course')
                ->join('student', 'student.id = request_course.user_id')
                ->join('users', 'users.id = request_course.user_id')
                ->get()
                ->result_array();
        return $data;
    }

    public function remove_class_student($class_id, $student_id) {
        $this->db->where('class_id', $class_id)
                ->where('student_id', $student_id)
                ->delete('class_student');
    }

    public function add_class_student($class_id, $student_id) {

        $check_data = $this->db->where('class_id', $class_id)
                ->where('student_id', $student_id)
                ->get('class_student')
                ->row_array();

        if (!isset($check_data['class_id'])) {
            $data = array('class_id' => $class_id, 'student_id' => $student_id);
            $this->db->insert('class_student', $data);
        }
    }

    public function update_class_teacher($class_id, $teacher_id) {
        $data = array('teacher_id' => $teacher_id);
        $this->db->where('id', $class_id)
                ->update('class', $data);
    }

    public function remove_class_time($class_id, $id) {
        $this->db->where('class_id', $class_id)
                ->where('id', $id)
                ->delete('class_schedule');
    }
    
    public function get_inbox($user_id , $user_type){
        if($user_type == 7 || $user_type == 6){
            $data = $this->db->where('to', $user_id)
                             ->get('inbox')
                             ->result_array();
            return $data;
        }
    }
    
    public function insert_inbox($to, $user_type, $from_name, $message, $url, $datetime){
        $data = array(
            'to' => $to,
            'user_type' => $user_type,
            'from_name' => $from_name,
            'message' => $message,
            'url' => $url,
            'datetime' => $datetime
        );
        $this->db->insert('inbox', $data);
    }
    
    public function get_inbox_data($id){
        $data = $this->db->where('id', $id)
                         ->get('inbox')
                         ->row_array();
        return $data;
    }
    
    public function remove_inbox_data($id){
        $data = $this->db->where('id', $id)
                         ->delete('inbox');
    }
    
    public function insert_rate_teacher($class_id, $user_id, $teacher_id, $rate, $feedback, $datetime){
        $data = array(
            'class_id' => $class_id,
            'user_id' => $user_id,
            'teacher_id' => $teacher_id,
            'rating' => $rate,
            'feedback' => $feedback,
            'datetime' => $datetime
        );
        
        $this->db->insert('teacher_rating', $data);
    }
    
    public function get_class_rating($class_id, $user_id){
        $data = $this->db->where('class_id', $class_id)
                         ->where('user_id', $user_id)
                         ->get('teacher_rating')
                         ->row_array();
        return $data;
    }
    
    public function count_teacher_rating($teacher_id){
        $data = $this->db->where('teacher_id', $teacher_id)
                         ->select_sum('rating')
                         ->select('COUNT(id) as user_count')
                         ->group_by('teacher_id')
                         ->get('teacher_rating')
                         ->row_array();
        return $data;
    }
    
    public function get_teacher_rating_list($teacher_id){
        $data = $this->db->where('teacher_id', $teacher_id)
                         ->get('teacher_rating')
                         ->result_array();
        return $data;
    }
    
    public function get_all_running_class_list(){
        $data = $this->db->where('status', 1)
                         ->get('class')
                         ->result_array();
        return $data;
    }
    
    public function get_all_completed_class_list(){
        $data = $this->db->where('status', 2)
                         ->get('class')
                         ->result_array();
        return $data;
    }
    
    public function class_mark_as_complete($class_id){
        $data = array('status'=> 2);
        
        $this->db->where('id', $class_id)
                 ->update('class', $data);
    }
    
    public function latest_student_attendance($class_id){
        $data = $this->db->where('class_id', $class_id)
                         ->order_by('datetime', 'desc')
                         ->get('class_attendance_student')
                         ->row_array();
                
        return $data;
    }
    
    public function latest_teacher_attendance($class_id){
        $data = $this->db->where('class_id', $class_id)
                         ->order_by('datetime', 'desc')
                         ->get('class_attendance_teacher')
                         ->row_array();
                
        return $data;
    }
    
    public function get_student_attendance($class_id, $student_id){
        $data = $this->db->where('class_id', $class_id)
                         ->where('student_id', $student_id)
                         ->order_by('student_id', 'asc')
                         ->get('class_attendance_student')
                         ->result_array();
                
        return $data;
    }
    
    public function get_teacher_attendance($class_id, $teacher_id){
        $data = $this->db->where('class_id', $class_id)
                         ->where('teacher_id', $teacher_id)
                         ->order_by('teacher_id', 'asc')
                         ->get('class_attendance_teacher')
                         ->result_array();
                
        return $data;
    }
    
    public function update_sign_in_time($user_id, $request_datetime){
        $data = array('last_login_datetime' => $request_datetime);
        $this->db->where('id', $user_id)
                 ->update('users', $data);
    }
    
    public function get_all_promotional_offers(){
        $data = $this->db->order_by('datetime', 'desc')
                         ->get('promotional_offers')
                         ->result_array();
        
        return $data;
    }
    
    public function get_one_promotional_offer($id){
        $data = $this->db->where('id', $id)
                         ->get('promotional_offers')
                         ->row_array();

        return $data;
    }
    
    public function insert_promotional_offer($user_id, $subject, $offer, $request_datetime){
        $data = array(
            'user_id' => $user_id,
            'subject' => $subject,
            'offer' => $offer,
            'datetime' => $request_datetime
        );
        $this->db->insert('promotional_offers', $data);
        
        return $this->db->insert_id();
    }
    
    public function insert_user_notification($student_id, $name, $url, $datetime){
        $data = array(
            'to' => $student_id,
            'name' => $name,
            'url' => $url,
            'datetime' => $datetime
        );
        
        $this->db->insert('notification', $data);
    }
    
    public  function get_notification($user_id, $user_type){
        $data = $this->db->where("to", $user_id)
                        ->get('notification')
                        ->result_array();
        return $data;
    }
    
    public function get_notification_data($id){
        $data = $this->db->where('id', $id)
                        ->get('notification')
                        ->row_array();
        
        return $data;
    }
    
    public function remove_notification($id){
        $this->db->where('id', $id)
                 ->delete('notification');
    }
    
    public function update_student_profile($id, $skype_id, $address, $state, $city, $zip_code, $phone_number, $secondary_phone){
        $data = array(
            'skype_id' => $skype_id,
            'address' => $address,
            'state' => $state,
            'city' => $city,
            'zip_code' => $zip_code,
            'phone_number' => $phone_number,
            'secondary_phone' => $secondary_phone
        );
        
        $this->db->where('id', $id)
                 ->update('student', $data);
    }
    
    public function change_class_end_date($class_id, $end_date){
        $data = array('end_date' => $end_date);
        
        $this->db->where('id', $class_id)
                ->update('class', $data);
    }
    
    public function change_class_status($class_id, $status){
        $data = array('status' => $status);
        
        $this->db->where('id', $class_id)
                ->update('class', $data);
    }

}

?>
