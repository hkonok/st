<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->database();
    }

    public function index() {
        if ($this->session->userdata('user_id'))
            redirect('admin/home/');



        $this->load->view('login');
    }

    private function mail($email, $pass) {

        $base = base_url();

        $sender_mail = "gkabswebsite@gmail.com";
        $sender_name = "Online Library Management";
        $subject = "Registration Of Student Management System";

        $message = "You Have successfully registered to Saudi Arabia Atudent management system
            Your Password is : $pass";





        $config = Array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'gkabswebsite@gmail.com',
            'smtp_pass' => 'gkabswebsitepassword'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");

        $this->email->from($sender_mail, $sender_name);
        $this->email->to($email);
        $this->email->subject($subject);
        $this->email->message($message);

        $send = $this->email->send();

        return $send;
    }

    public function sign_in() {
        if ($this->session->userdata('user_id'))
            redirect('admin/home/');
        $this->load->model('admin_model');

        $email = $this->input->post('email', TRUE);
        $password = md5($this->input->post('password', TRUE));
        $data = $this->admin_model->sign_in($email, $password);
        echo $email . " " . $password . " <br>" . var_dump($data) . "<br>";

        if (isset($data['id'])) {
            $user_data = $this->admin_model->get_user_data($data['id'], $data['user_type']);
            $newdata = array(
                'user_id' => $data['id'],
                'email' => $data['email'],
                'user_type' => $data['user_type'],
                'first_name' => $user_data['first_name'],
                'last_name' => $user_data['last_name'],
                    'time_zone'=>$user_data['time_zone']
            );
            $this->session->set_userdata($newdata);

            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;


            $this->admin_model->update_sign_in_time($data['id'], $request_datetime);

            redirect('admin/home/');
        } else {
            redirect('admin/index');
        }
    }

    public function log_out() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        $this->session->sess_destroy();
        redirect('admin/index/');
    }

    public function home() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        
        echo $this->session->userdata('user_type');
        
        if ($this->session->userdata('user_type') == 7 || $this->session->userdata('user_type') == 6)
            redirect('admin/user_home/');


      //  $user_id = $this->session->userdata('user_id');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 1;
        $header_data['user_type'] = $this->session->userdata('user_type');

        /*
          foreach($footer_data['date_list'] as $row){
          echo "<br>".var_dump($row);
          }
         */
         $data=array();
         $query='select count(id) as num,country from student group by country';
         $query = $this->db->query($query);
         $data['student']=$query->result();
         //$this->db->join('courses', 'courses.id=course_taken.course_id');
         $q='select count(user_id) as num,course_id from request_course group by course_id';
         $q = $this->db->query($q);
         $data['course']=$q->result();

         $this->db->join('courses', 'courses.id=request_course.course_id');
         $this->db->join('student', 'student.id=request_course.user_id');
         $this->db->order_by('request_course.start_date','desc');
         $qu=$this->db->get('request_course');
         $data['record']=$qu->result();
         
            $this->load->view('header', $header_data);
            $this->load->view('home_1',$data);
            
       // $this->load->view('footer');
    }

    public function user_home() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7 && $this->session->userdata('user_type') != 6)
            redirect('admin/index/');


        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');
        $user_gmt=$this->session->userdata('time_zone');
        
        if($user_gmt == '')
        {
            $user_gmt=0;
        }

        $header_data['inbox_notification'] = $this->get_inbox_notification();

        $data['inbox_notification'] = $this->get_inbox_notification();

        $header_data['menu_id'] = 1;
        $header_data['user_type'] = $this->session->userdata('user_type');

        if ($user_type == 7) {
            $data['running_class_list'] = $this->admin_model->get_student_running_class($user_id);
        } else if ($user_type == 6) {
            $data['running_class_list'] = $this->admin_model->get_teacher_running_class($user_id);
        }
        $footer_data['date_list'] = $this->class_dates($data['running_class_list']);
        $footer_data['user_type'] = $user_type;
        $footer_data['gmt'] = $user_gmt;
        /*
          foreach($footer_data['date_list'] as $row){
          echo "<br>".var_dump($row);
          }
         */

        $this->load->view('header', $header_data);
        $this->load->view('home', $data);

        $this->load->view('footer', $footer_data);
    }

    private function class_dates($class_list) {

        $date_array = array();
        $user_type = $this->session->userdata('user_type');

        if ($user_type == 7)
            $id_name = 'class_id';
        else if ($user_type == 6)
            $id_name = 'id';

        $index = 0;
        foreach ($class_list as $row) {
            $class_data = $this->admin_model->get_class_data($row[$id_name]);
            $class_schedule = $this->admin_model->get_class_schedule($row[$id_name]);

            $class_name = "";

            switch ($class_data['course_id']) {
                case 1:
                    $class_name = "Arabic";
                    break;
                case 2:
                    $class_name = "Quran";
                    break;
                case 3:
                    $class_name = "Arabic & Quran";
                    break;
            }

            foreach ($class_schedule as $schedule) {
                $current_time = new DateTime;
                $otherTZ = new DateTimeZone('Europe/London');
                $current_time->setTimezone($otherTZ);

                $start_date = new DateTime($class_data['start_date']);
                $otherTZ = new DateTimeZone('Europe/London');
                $start_date->setTimezone($otherTZ);

                $change_date = $current_time;
                $change_date->modify('previous ' . $schedule['day']);

                for ($i = 0; $i < 6; $i++) {

                    //   echo "<br>".date_format($change_date, "Y-m-d")." > ".date_format($start_date, "Y-m-d");
                    if ($change_date > $start_date) {
                        //      echo "<br>fk";
                        $date_array[$index]['datetime'] = new Datetime(date_format($change_date, "Y-m-d"));
                        $date_array[$index]['class_name'] = $class_name;
                        $date_array[$index]['class_time'] = $schedule['time'];
                        $index++;
                    }
                    $change_date->modify('next ' . $schedule['day']);
                }
            }
        }

        return $date_array;
    }

    public function user_list() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('user_list');
        $this->load->view('footer');
    }

    public function create_new() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['menu_id'] = 2;
        $this->load->view('header', $header_data);
        $this->load->view('create_new');
        $this->load->view('footer');
    }

    public function change_user_status($user_id, $urls) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/index/');

        $this->admin_model->change_user_status($user_id);

        redirect('admin/' . $urls . "/");
    }

    public function create_manager() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 1) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_manager');
            $this->load->view('footer');
        } else {



            if ($this->admin_model->create_manager()) {
// $send = $this->mail($email, $pass);
                redirect('admin/manager_list/success');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    function edit_manager($id, $message = "-1") {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 1) {
            redirect('admin/home/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $this->db->where('manager.id', $id);
            $this->db->join('users', 'manager.id=users.id');
            $query = $this->db->get('manager');
            $data['record'] = $query->result();
            $data['message'] = $message;

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('manager_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_manager($id)) {
// $send = $this->mail($email, $pass);
                $data['msg'] = "Manager Info Successfully updated";
                $data['manager_list'] = $this->admin_model->get_manager_list();

                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['menu_id'] = 2;
                $header_data['sub_menu_id'] = 1;
                $header_data['user_type'] = $this->session->userdata('user_type');
                $this->load->view('header', $header_data);
                $this->load->view('manager_list', $data);
                $this->load->view('footer_list');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    function change_password($id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 0 &&
                $this->session->userdata('user_type') != 1) {
            redirect('admin/home/');
        }



        $this->load->library('form_validation');
        $this->form_validation->set_rules('password', '', 'required');
        $this->form_validation->set_rules('password2', '', 'required');
        $success_status = '';
        if ($this->form_validation->run() == TRUE) {
            $password1 = $this->input->post('password', TRUE);
            $password2 = $this->input->post('password2', TRUE);
            if ($password1 == $password2) {
                $password = md5($password1);
                $this->admin_model->change_password($id, $password);
                $success_status = '1';
            }
        }

        redirect('admin/' . $url . '/' . $id . '/' . $success_status);
    }

    public function manager_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 1) {
            redirect('admin/home/');
        }


        $data['manager_list'] = $this->admin_model->get_manager_list();
        if ($msg == 'success') {
            $data['msg'] = "Manager Successfully Created";
        }
        $data['user_type'] = $this->session->userdata('user_type');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 1;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('manager_list', $data);
        $this->load->view('footer_list');
    }

    public function create_personal_manager() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 4) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 4;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_personal_manager');
            $this->load->view('footer');
        } else {



            if ($this->admin_model->create_personal_manager()) {
// $send = $this->mail($email, $pass);
                redirect('admin/personal_manager_list/success');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    function edit_personal_manager($id, $message = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 4) {
            redirect('admin/home/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {
            $data = array();
            $this->db->where('personal_manager.id', $id);
            $this->db->join('users', 'personal_manager.id=users.id');
            $query = $this->db->get('personal_manager');
            $data['record'] = $query->result();

            $data['user_type'] = $this->session->userdata('user_type');
            $data['message'] = $message;
            $data['user_basic'] = $this->admin_model->get_one_user($id);

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 4;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('personal_manager_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_personal_manager($id)) {
// $send = $this->mail($email, $pass);
                $data['msg'] = "Personal Manager Info Successfully updated";
                $data['personal_manager_list'] = $this->admin_model->get_personal_manager_list();

                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['menu_id'] = 2;
                $header_data['sub_menu_id'] = 4;
                $header_data['user_type'] = $this->session->userdata('user_type');
                $this->load->view('header', $header_data);
                $this->load->view('personal_manager_list', $data);
                $this->load->view('footer_list');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    public function personal_manager_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 4) {
            redirect('admin/home/');
        }

        $data['personal_manager_list'] = $this->admin_model->get_personal_manager_list();
        if ($msg == 'success') {
            $data['msg'] = "Personal Manager Successfully Created";
        }
        $data['user_type'] = $this->session->userdata('user_type');
//var_dump($data);

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 4;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('personal_manager_list', $data);
        $this->load->view('footer_list');
    }

    public function create_teacher_manager() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 2 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 3;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_teacher_manager');
            $this->load->view('footer');
        } else {



            if ($this->admin_model->create_teacher_manager()) {
// $send = $this->mail($email, $pass);
                redirect('admin/teacher_manager_list/success');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    function edit_teacher_manager($id, $message = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 3 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {

            $data['user_basic'] = $this->admin_model->get_one_user($id);

            $data['user_data'] = $this->admin_model->get_user_data($id, $data['user_basic']['user_type']);
            $data['message'] = $message;
            $data['user_type'] = $this->session->userdata('user_type');

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $header_data['teacher_id'] = $id;
            $header_data['user_basic'] = $data['user_basic'];
            $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

            $header_data['user_type'] = $this->session->userdata('user_type');

            $data['user_type'] = $this->session->userdata('user_type');

            $this->load->view('header', $header_data);
            $this->load->view('teacher_manager_edit_view', $data);
            $this->load->view('footer_profile');
        } else {

            if ($this->admin_model->update_teacher_manager($id)) {
// $send = $this->mail($email, $pass);
                redirect('admin/edit_teacher_manager/' . $id . '/');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    function edit_customer_service($id, $message = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 3 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {

            $data['user_basic'] = $this->admin_model->get_one_user($id);

            $data['user_data'] = $this->admin_model->get_user_data($id, $data['user_basic']['user_type']);


            $data['message'] = $message;

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $header_data['teacher_id'] = $id;
            $header_data['user_basic'] = $data['user_basic'];
            $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('customer_service_edit_view', $data);
            $this->load->view('footer_profile');
        } else {

            if ($this->admin_model->update_customer_service($id)) {
// $send = $this->mail($email, $pass);

                redirect('admin/edit_customer_service/' . $id . '/');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    public function teacher_manager_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 2 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }


        $data['teacher_manager_list'] = $this->admin_model->get_teacher_manager_list();
        if ($msg == 'success') {
            $data['msg'] = "Teacher Manager Successfully Created";
        }
        $data['user_type'] = $this->session->userdata('user_type');
//var_dump($data);
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 3;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('teacher_manager_list', $data);
        $this->load->view('footer_list');
    }

    public function technical_manager_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 2 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }


        $data['technical_manager_list'] = $this->admin_model->get_technical_manager_list();
        if ($msg == 'success') {
            $data['msg'] = "Technical Manager Successfully Created";
        }
        $data['user_type'] = $this->session->userdata('user_type');
//var_dump($data);
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 2;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('technical_manager_list', $data);
        $this->load->view('footer_list');
    }

    public function create_technical_manager() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 2 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 2;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_technical_manager');
            $this->load->view('footer');
        } else {



            if ($this->admin_model->create_technical_manager()) {
// $send = $this->mail($email, $pass);
                redirect('admin/technical_manager_list/success');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    function edit_technical_manager($id, $message = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 2 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {




            $data['user_basic'] = $this->admin_model->get_one_user($id);

            $data['user_data'] = $this->admin_model->get_user_data($id, $data['user_basic']['user_type']);
            $data['message'] = $message;

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $header_data['teacher_id'] = $id;
            $header_data['user_basic'] = $data['user_basic'];
            $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);


            $header_data['user_type'] = $this->session->userdata('user_type');





            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 2;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('technical_manager_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_technical_manager($id)) {
// $send = $this->mail($email, $pass);
                redirect('admin/edit_technical_manager/' . $id . '/');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    public function create_teacher() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') >= 3) {
            redirect('admin/home/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 6;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_teacher');
            $this->load->view('footer');
        } else {



            if ($this->admin_model->create_teacher()) {
// $send = $this->mail($email, $pass);
                redirect('admin/teacher_list/success');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    public function teacher_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }


        $data['teacher_list'] = $this->admin_model->get_teacher_list();
        if ($msg == 'success') {
            $data['msg'] = "teacher Successfully Created";
        }
        $data['user_type'] = $this->session->userdata('user_type');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 6;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('teacher_list', $data);
        $this->load->view('footer_list');
    }

    public function create_student() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 7;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_student');
            $this->load->view('footer');
        } else {



            if (($student_id = $this->admin_model->create_student()) >= 0) {
// $send = $this->mail($email, $pass);
                redirect('admin/edit_student/' . $student_id . '/');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    public function student_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }


        $data['student_list'] = $this->admin_model->get_student_list();
        if ($msg == 'success') {
            $data['msg'] = "student Successfully Created";
        }
        $data['user_type'] = $this->session->userdata('user_type');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 7;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('student_list', $data);
        $this->load->view('footer_list');
    }

    public function create_customer_service() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 5) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');
        $this->form_validation->set_rules('password', '', 'required|matches[c_password]');
        $this->form_validation->set_message('matches', 'Password was not confirmed correctly');


        $email = $this->input->post('email', true);
        $pass = $this->input->post('password', true);

        if ($this->form_validation->run() == FALSE) {

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 5;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('create_new_customer_service');
            $this->load->view('footer');
        } else {



            if ($this->admin_model->create_customer_service()) {
// $send = $this->mail($email, $pass);
                redirect('admin/customer_service_list/success');
            } else {

                echo "an error occured please enter correct data and try again";
            }
        }
    }

    public function customer_service_list($msg = '') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') >= 2 && $this->session->userdata('user_type') != 4) {
            redirect('admin/home/');
        }


        $data['customer_service_list'] = $this->admin_model->get_customer_service_list();
        $data['user_type'] = $this->session->userdata('user_type');

        if ($msg == 'success') {
            $data['msg'] = "customer_service Successfully Created";
        }

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 5;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('customer_service_list', $data);
        $this->load->view('footer_list');
    }

    public function create_class() {

        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
 
        if (($this->session->userdata('user_type') != 5) && ($this->session->userdata('user_type') != 0)) {
            redirect('admin/home/');
        }

 
        
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 30;
        $header_data['user_type'] = $this->session->userdata('user_type');

        
         $this->load->library('form_validation');

        $this->form_validation->set_rules('teacher', 'Teacher', 'required');
        $this->form_validation->set_rules('course', 'Course', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');
        $this->form_validation->set_rules('teacher_skill', 'Skills Should be in Teacher', 'required');
        $this->form_validation->set_rules('study_hours', 'Study Hours/Week', 'required');
        $this->form_validation->set_rules('start_date', 'Start Days', 'required');
        $this->form_validation->set_rules('end_date', 'End Days', 'required');
        
        $this->form_validation->set_rules('students', 'Students', 'required');

        if (
                $this->form_validation->run() == FALSE ||
                $this->input->post('course', TRUE) == '' ||
                $this->input->post('level', TRUE) == '' ||
                $this->input->post('teacher_skill', TRUE) == '' ||
                $this->input->post('study_hours', TRUE) == ''
        ) {
            $data = array();
            $query = $this->db->get('teacher');
            $data['teacher'] = $query->result();
            $query = $this->db->get('student');
            $data['student'] = $query->result();

            $this->load->view('header', $header_data);
            $this->load->view('create_class_view', $data);
            $this->load->view('footer_form');
            
        } else {
            $teacher = $this->input->post('teacher', TRUE);
            
            $course_id = $this->input->post('course', TRUE);
            $level_id = $this->input->post('level', TRUE);
            $skill_id = $this->input->post('teacher_skill', TRUE);
            $study_hours = $this->input->post('study_hours', TRUE);
            $start_date = $this->input->post('start_date', TRUE);
            
            $end_date = $this->input->post('end_date', TRUE);
            
            $students = $this->input->post('students', TRUE);
            
            $student = array();
            
            $st_size = 0;
            foreach($students as $row){
                $student[$st_size] = $row;
                $st_size++;
            }
            
            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;


            $request_id = $this->admin_model->insert_request_course($student[0], $course_id, $level_id, $skill_id, $study_hours, $start_date, $request_datetime);


            $success_data['success'] = 0;
            if ($request_id >= 1) {
                $this->admin_model->sm_create_class($request_id, $course_id, $level_id, $skill_id, $study_hours, $start_date, $end_date, $request_datetime, $teacher);
                if($st_size > 1){
                    for($j = 1; $j<$st_size; $j++)
                        $this->admin_model->add_class_student($request_id, $student[$j]);
                }
                redirect("admin/assign_class_details/" . $request_id . "/");
            } else {
                redirect('admin/create_class/');
            }
        }
    }

    public function class_list() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        if ($this->session->userdata('user_type') >= 5) {
            redirect('admin/home/');
        }


        $header_var['header_tag'] = 'class';

        $class_id = "";
        $this->db->join('teacher', 'teacher.id=class.teacher_id');
        $query = $this->db->get('class');
        $data['class_list'] = $query->result();
        foreach ($query->result() as $row) {
            $class_id = $row->id;

            $this->db->where('class_id', $class_id);
            $this->db->join('student', 'student.id=class_student.student_id');
            $query = $this->db->get('class_student');
            $data['student_list'] = $query->result();
        }

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 13;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('class_list', $data);
        $this->load->view('footer_list');
    }

    function edit_teacher($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('firstname', '', 'required');
        $this->form_validation->set_rules('lastname', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $this->db->where('teacher.id', $id);
            $this->db->join('users', 'teacher.id=users.id');
            $query = $this->db->get('teacher');
            $data['record'] = $query->result();

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 6;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('teacher_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_teacher($id)) {
// $send = $this->mail($email, $pass);
                $data['msg'] = "Teacher Info Successfully updated";
                $data['teacher_list'] = $this->admin_model->get_teacher_list();

                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['menu_id'] = 2;
                $header_data['sub_menu_id'] = 6;
                $header_data['user_type'] = $this->session->userdata('user_type');
                $this->load->view('header', $header_data);
                $this->load->view('teacher_list', $data);
                $this->load->view('footer_list');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    function edit_teacher_password($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', '', 'required');


        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $this->db->where('teacher.id', $id);
            $this->db->join('users', 'teacher.id=users.id');
            $query = $this->db->get('teacher');
            $data['record'] = $query->result();

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 6;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('teacher_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_teacher_password($id)) {
// $send = $this->mail($email, $pass);
                $data['msg'] = "Teacher password Successfully changed";
                $data['teacher_list'] = $this->admin_model->get_teacher_list();

                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['menu_id'] = 2;
                $header_data['sub_menu_id'] = 6;
                $header_data['user_type'] = $this->session->userdata('user_type');
                $this->load->view('header', $header_data);
                $this->load->view('teacher_list', $data);
                $this->load->view('footer_list');
            } else {

                echo "an error occured please try again";
            }
        }
    }

    function edit_student($id, $message = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('first_name', '', 'required');
        $this->form_validation->set_rules('last_name', '', 'required');
        $this->form_validation->set_rules('email', '', 'valid_email');


        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $this->db->where('student.id', $id);
            $this->db->join('users', 'student.id=users.id');
            $query = $this->db->get('student');
            $data['record'] = $query->result();

            $data['message'] = $message;
            $data['user_basic'] = $this->admin_model->get_one_user($id);
            $data['user_data'] = $this->admin_model->get_user_data($id, 7);
            $data['user_type'] = $this->session->userdata('user_type');



            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 7;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('student_edit_view', $data);
            $this->load->view('footer_list');
        } else {
            $user_id = $id;
            $first_name = $this->input->post('first_name', TRUE);
            $last_name = $this->input->post('last_name', TRUE);
            $email = $this->input->post('email', TRUE);
            $time_zone = $this->input->post('time_zone', TRUE);
            $country = $this->input->post('country', TRUE);

            $skype_id = $this->input->post('skype_id', TRUE);
            $address = $this->input->post('address', TRUE);
            $state = $this->input->post('state', TRUE);
            $city = $this->input->post('city', TRUE);
            $zip_code = $this->input->post('zip_code', TRUE);
            $phone_number = $this->input->post('phone_number', TRUE);
            $secondary_phone = $this->input->post('secondary_phone', TRUE);

            $birthday = "";
            $gender = "";
            $birthday = $this->input->post('birthday', TRUE);
            $gender = $this->input->post('gender', TRUE);

            $this->admin_model->update_user_profile($user_id, $first_name, $last_name, $email, $time_zone, $country, $birthday, $gender);

            $this->admin_model->update_student_profile($user_id, $skype_id, $address, $state, $city, $zip_code, $phone_number, $secondary_phone);

            redirect('admin/edit_student/' . $id . '/');
            
        }
    }

    function edit_student_password($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        if ($this->session->userdata('user_type') >= 6) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', '', 'required');


        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $this->db->where('student.id', $id);
            $this->db->join('users', 'student.id=users.id');
            $query = $this->db->get('student');
            $data['record'] = $query->result();

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 7;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('student_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_student_password($id)) {
// $send = $this->mail($email, $pass);
                $data['msg'] = "Student password Successfully changed";
                $data['student_list'] = $this->admin_model->get_student_list();

                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['menu_id'] = 2;
                $header_data['sub_menu_id'] = 7;
                $header_data['user_type'] = $this->session->userdata('user_type');
                $this->load->view('header', $header_data);
                $this->load->view('student_list', $data);
                $this->load->view('footer_list');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    function edit_customer_service_password($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');
        if ($this->session->userdata('user_type') >= 5) {
            redirect('admin/home/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('password', '', 'required');


        if ($this->form_validation->run() == FALSE) {

            $data = array();
            $this->db->where('customer_service.id', $id);
            $this->db->join('users', 'customer_service.id=users.id');
            $query = $this->db->get('customer_service');
            $data['record'] = $query->result();

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 5;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('customer_service_edit_view', $data);
            $this->load->view('footer_list');
        } else {

            if ($this->admin_model->update_customer_service_password($id)) {
// $send = $this->mail($email, $pass);
                $data['msg'] = "Customer Service password Successfully changed";
                $data['customer_service_list'] = $this->admin_model->get_customer_service_list();

                $header_data['inbox_notification'] = $this->get_inbox_notification();
                $header_data['menu_id'] = 2;
                $header_data['sub_menu_id'] = 5;
                $header_data['user_type'] = $this->session->userdata('user_type');
                $this->load->view('header', $header_data);
                $this->load->view('customer_service_list', $data);
                $this->load->view('footer_list');
            } else {

                echo "an error occured please  try again";
            }
        }
    }

    public function profile($user_id = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($user_id == '-1') {
            $user_id = $this->session->userdata('user_id');
        }

        $user_basic = $data['user_basic'] = $this->admin_model->get_one_user($user_id);
        $user_data = $data['user_data'] = $this->admin_model->get_user_data($user_id, $this->session->userdata('user_type'));

        $this->load->library('form_validation');


        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('time_zone', 'Timezone', 'required');

        if ($this->form_validation->run() == FALSE) {
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 'profile';
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('profile_view', $data);
            $this->load->view('footer_profile');
        } else {
            $user_id = $this->session->userdata('user_id');
            $first_name = $this->input->post('first_name', TRUE);
            $last_name = $this->input->post('last_name', TRUE);
            $email = $this->input->post('email', TRUE);
            $time_zone = $this->input->post('time_zone', TRUE);
            $country = $this->input->post('country', TRUE);

            if ($data['user_basic']['user_type'] == 7) {
                $skype_id = $this->input->post('skype_id', TRUE);
                $address = $this->input->post('address', TRUE);
                $state = $this->input->post('state', TRUE);
                $city = $this->input->post('city', TRUE);
                $zip_code = $this->input->post('zip_code', TRUE);
                $phone_number = $this->input->post('phone_number', TRUE);
                $secondary_phone = $this->input->post('secondary_phone', TRUE);
            }

            $birthday = "";
            $gender = "";

            if ($data['user_basic']['user_type'] == 6 || $data['user_basic']['user_type'] == 7) {
                $birthday = $this->input->post('birthday', TRUE);
                $gender = $this->input->post('gender', TRUE);
            }

            $this->admin_model->update_user_profile($user_id, $first_name, $last_name, $email, $time_zone, $country, $birthday, $gender);
            if ($user_basic['user_type'] == 7) {

                $this->admin_model->update_student_profile($user_basic['id'], $skype_id, $address, $state, $city, $zip_code, $phone_number, $secondary_phone);
            }
            redirect('admin/profile/');
        }
    }

    public function start_new_course() {

        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != '7')
            redirect('admin/home/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');



        $this->load->library('form_validation');

        $this->form_validation->set_rules('course', 'Course', 'required');
        $this->form_validation->set_rules('level', 'Level', 'required');
        $this->form_validation->set_rules('teacher_skill', 'Skills Should be in Teacher', 'required');
        $this->form_validation->set_rules('study_hours', 'Study Hours/Week', 'required');
        $this->form_validation->set_rules('start_date', 'Start Days', 'required');

        if (
                $this->form_validation->run() == FALSE ||
                $this->input->post('course', TRUE) == '' ||
                $this->input->post('level', TRUE) == '' ||
                $this->input->post('teacher_skill', TRUE) == '' ||
                $this->input->post('study_hours', TRUE) == ''
        ) {
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 7;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $this->load->view('header', $header_data);
            $this->load->view('start_new_course');
            $this->load->view('footer_form');
        } else {

            $course_id = $this->input->post('course', TRUE);
            $level_id = $this->input->post('level', TRUE);
            $skill_id = $this->input->post('teacher_skill', TRUE);
            $study_hours = $this->input->post('study_hours', TRUE);
            $start_date = $this->input->post('start_date', TRUE);


            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;


            $request_id = $this->admin_model->insert_request_course($user_id, $course_id, $level_id, $skill_id, $study_hours, $start_date, $request_datetime);


            $success_data['success'] = 0;
            if ($request_id >= 1) {
                $this->admin_model->create_empty_class($request_id, $course_id, $level_id, $skill_id, $study_hours, $start_date, $request_datetime);
                redirect("admin/course_schedule/" . $request_id . "/");
            } else {
                $success_data['success'] = -1;
            }
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 7;
            $header_data['sub_menu_id'] = 1;
            $header_data['user_type'] = $this->session->userdata('user_type');

            $this->load->view('header', $header_data);
            $this->load->view('start_new_course', $success_data);
            $this->load->view('footer_form');
        }
    }

    public function course_schedule($request_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 8;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $data['course_data'] = $this->admin_model->get_one_request_list($user_id, $request_id, 0);
        $data['schedule'] = $this->admin_model->get_request_schedule($request_id);
        $data['request_id'] = $request_id;

        $this->load->view('header', $header_data);
        $this->load->view('course_schedule', $data);
        $this->load->view('footer_form');
    }

    public function add_student_schedule_time($request_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');
        $user_id = $this->session->userdata('user_id');

        $data = $this->admin_model->get_one_request_list($user_id, $request_id, 0);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('day', 'Day', 'required');
        $this->form_validation->set_rules('time', 'Time', 'required');


        if (isset($data['user_id']) && $this->form_validation->run() == TRUE) {

            $day = $this->input->post('day', TRUE);
            $time = $this->input->post('time', TRUE);

            $this->admin_model->add_student_schedule_time($request_id, $day, $time);
        }

        redirect('admin/' . $url . '/' . $request_id);
    }

    public function remove_student_schedule_time($request_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');
        $user_id = $this->session->userdata('user_id');

        $data = $this->admin_model->get_one_request_list($user_id, $request_id, 0);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('remove_schedule', 'Remove Schedule', 'required');


        if (isset($data['user_id']) && $this->form_validation->run() == TRUE) {
            $schedule_id = $this->input->post('remove_schedule', TRUE);

            $this->admin_model->remove_student_schedule_time($schedule_id);
        }

        redirect('admin/' . $url . '/' . $request_id);
    }

    public function student_remove_pending_request($request_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');
        $user_id = $this->session->userdata('user_id');

        $data = $this->admin_model->get_one_request_list($user_id, $request_id, 0);

        if (!isset($data['user_id']))
            redirect('admin/index/');

        $this->admin_model->student_remove_pending_request($request_id);
        $this->admin_model->student_remove_pending_class($request_id);
        redirect('admin/pending_request/');
    }
    
    public function sm_remove_pending_request($request_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');

        $this->admin_model->student_remove_pending_request($request_id);
        $this->admin_model->student_remove_pending_class($request_id);
        redirect('admin/assign_class/');
    }

    public function pending_request() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');


        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $data['request_list'] = $this->admin_model->get_request_list($user_id, 0);
        $data['user_id'] = $user_id;
        $data['user_type'] = $user_type;
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 8;
        $header_data['user_type'] = $this->session->userdata('user_type');

        $this->load->view('header', $header_data);
        $this->load->view('student_pending_request', $data);
        $this->load->view('footer_form');
    }

    public function pending_request_details($request_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $data['request_data'] = $this->admin_model->get_one_request_list($user_id, $request_id, 0);

        if (!isset($data['request_data']['user_id']))
            redirect('admin/index/');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('level', 'Level', 'required');
        $this->form_validation->set_rules('teacher_skill', 'Skills Should be in Teacher', 'required');
        $this->form_validation->set_rules('study_hours', 'Study Hours/Week', 'required');
        $this->form_validation->set_rules('start_date', 'Start Days', 'required');

        if (
                $this->form_validation->run() == FALSE ||
                $this->input->post('level', TRUE) == '' ||
                $this->input->post('teacher_skill', TRUE) == '' ||
                $this->input->post('study_hours', TRUE) == ''
        ) {
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 8;
            $header_data['user_type'] = $this->session->userdata('user_type');



            $data['request_id'] = $request_id;
            $data['schedule'] = $this->admin_model->get_request_schedule($request_id);

            $this->load->view('header', $header_data);
            $this->load->view('pending_request_details', $data);
            $this->load->view('footer_form');
        } else {
            $course = $this->input->post('course', TRUE);
            $level = $this->input->post('level', TRUE);
            $teacher_skill = $this->input->post('teacher_skill', TRUE);
            $study_hours = $this->input->post('study_hours', TRUE);

            $this->admin_model->update_request_course_data($request_id, $course, $level, $teacher_skill, $study_hours);
            redirect('admin/pending_request_details/' . $request_id . "/");
        }
    }

    public function view_teacher($user_id, $message = '-1') {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') == 6 || $this->session->userdata('user_type') == 7)
            redirect('admin/home/');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($user_id);

        $data['user_data'] = $this->admin_model->get_user_data($user_id, $data['user_basic']['user_type']);
        $data['message'] = $message;
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 1;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $user_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $this->load->library('form_validation');


        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');



        if ($header_data['user_type'] == 6) {

            $this->form_validation->set_rules('birthday', 'Birthday', 'required');
            $this->form_validation->set_rules('time_zone', 'Time Zone', 'required');
            $this->form_validation->set_rules('date_of_contract', 'Date of Contruct Signing', 'required');
            $this->form_validation->set_rules('bank_account', 'Bank Account', 'required');
        }

        if ($this->form_validation->run() == FALSE) {

            $this->load->view('header', $header_data);
            $this->load->view('teacher_basic_info_view', $data);
            $this->load->view('footer_profile');
        } else if ($data['user_type'] != 5 && $data['user_type'] <= 4) {


            $first_name = $this->input->post('first_name', TRUE);
            $last_name = $this->input->post('last_name', TRUE);
            $email = $this->input->post('email', TRUE);
            $birthday = $this->input->post('birthday', TRUE);
            $time_zone = $this->input->post('time_zone', TRUE);
            $date_of_contract = $this->input->post('date_of_contract', TRUE);
            $bank_account = $this->input->post('bank_account', TRUE);

            $this->admin_model->update_teacher_profile($user_id, $first_name, $last_name, $email, $birthday, $time_zone, $date_of_contract, $bank_account);

            $this->admin_model->update_user_email($user_id, $email);

            redirect('admin/view_teacher/' . $user_id . '/');
        } else {
            redirect('admin/view_teacher/' . $user_id . '/');
        }
    }

    public function item_received_add_item($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('item', 'Item', 'required');
        $this->form_validation->set_rules('date', 'Date', 'required');

        if ($this->form_validation->run() == TRUE) {
            $item = $this->input->post('item', TRUE);
            $date = $this->input->post('date', TRUE);
            $this->admin_model->item_received_add_item($user_id, $item, $date);
        }
        redirect('admin/user_items_received/' . $user_id . '/');
    }

    public function item_received_return_item($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('return_item', 'Item', 'required');
        $this->form_validation->set_rules('returning_date', 'Date', 'required');

        if ($this->form_validation->run() == TRUE) {
            $item_id = $this->input->post('return_item', TRUE);
            $date = $this->input->post('returning_date', TRUE);
            $this->admin_model->item_received_return_item($user_id, $item_id);
        }
// echo $item_id;
        redirect('admin/user_items_received/' . $user_id . '/');
    }

    public function user_items_received($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') != 3 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($user_id);

        $data['user_data'] = $this->admin_model->get_user_data($user_id, $data['user_basic']['user_type']);
        $data['items'] = $this->admin_model->get_items_received($user_id);

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 2;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['sub_menu_id'] = 2;
        $header_data['teacher_id'] = $user_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $this->load->view('header', $header_data);
        $this->load->view('user_items_received', $data);
        $this->load->view('footer_profile');
    }

    public function add_papers_from_employee($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('add_paper', 'Add Paper', 'required');

        if ($this->form_validation->run() == TRUE) {
            $paper = $this->input->post('add_paper', TRUE);
            $this->admin_model->add_papers_from_employee($user_id, $paper);
        }

        redirect('admin/papers_from_employees/' . $user_id . "/");
    }

    public function remove_papers_from_employee($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('remove_paper', 'Add Paper', 'required');

        if ($this->form_validation->run() == TRUE) {
            $paper_id = $this->input->post('remove_paper', TRUE);
            $this->admin_model->remove_papers_from_employee($user_id, $paper_id);
        }

        redirect('admin/papers_from_employees/' . $user_id . "/");
    }

    public function papers_from_employees($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') != 3 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($user_id);

        $data['user_data'] = $this->admin_model->get_user_data($user_id, $data['user_basic']['user_type']);
        $data['papers'] = $this->admin_model->get_papers_from_employee($user_id);

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 3;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $user_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $this->load->view('header', $header_data);
        $this->load->view('papers_from_employees', $data);
        $this->load->view('footer_profile');
    }

    public function teacher_technical_info($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($user_id);

        if (!isset($data['user_basic']) || $data['user_basic']['user_type'] != 6)
            redirect('admin/home/');

        $data['user_data'] = $this->admin_model->get_user_data($user_id, $data['user_basic']['user_type']);
        $data['technical_info'] = $this->admin_model->get_teacher_technical_info($user_id);

        $tmp_data['none'] = 0;
        $tok = strtok($data['technical_info']['other_skill'], ",");
        while ($tok) {
            $tmp_data[$tok] = 1;
            $tok = strtok(",");
        }
         $data['other_skill'] = $tmp_data;
         
        $tmp_personality['none'] = 0;
        $tok = strtok($data['technical_info']['personality'], ",");
        while ($tok) {
            $tmp_personality[$tok] = 1;
            $tok = strtok(",");
        }
         $data['personality'] = $tmp_personality;
        

       

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 8;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $user_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);



        $this->load->library('form_validation');


        $this->form_validation->set_rules('personality', 'Personality', 'required');
        $this->form_validation->set_rules('teaching_preference', 'Teaching Preference', 'required');
        $this->form_validation->set_rules('teacher_of', 'Teacher Of', 'required');
        $this->form_validation->set_rules('english_level', 'English Level', 'required');
        $this->form_validation->set_rules('bio', 'Bio', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $header_data);
            $this->load->view('teacher_technical_info', $data);
            $this->load->view('footer_profile');
        } else {

            if ($this->session->userdata('user_type') == 2) {
                $personality_1 = $this->input->post('personality', TRUE);
                $teaching_preference = $this->input->post('teaching_preference', TRUE);
                $teacher_of = $this->input->post('teacher_of', TRUE);
                $english_level = $this->input->post('english_level', TRUE);
                $other_skill_1 = $this->input->post('other_skill', TRUE);
                $other_skill = "";
                $personality = "";
                $i = 0;
                foreach ($other_skill_1 as $row) {

                    if ($i > 0)
                        $other_skill .= ",";
                    $i++;
                    $other_skill .= $row;
                }
                
                $i = 0;
                foreach ($personality_1 as $row) {
                    if ($i > 0)
                        $personality .= ",";
                    $i++;
                    $personality .= $row;
                }
                $bio = $this->input->post('bio', TRUE);
                $this->admin_model->insert_teacher_technical_info($user_id, $personality, $teaching_preference, $teacher_of, $english_level, $other_skill, $bio);
            }
            redirect('admin/teacher_technical_info/' . $user_id . "/");
        }
    }

    public function annual_vacation($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') > 5)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($user_id);

        $data['user_data'] = $this->admin_model->get_user_data($user_id, $data['user_basic']['user_type']);
        $data['vacation_list'] = $this->admin_model->get_annual_vacation($user_id);

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 4;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $user_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $this->load->view('header', $header_data);
        $this->load->view('annual_vacation', $data);
        $this->load->view('footer_profile');
    }

    public function teacher_feedback_rating($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') > 5)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($user_id);

        $data['user_data'] = $this->admin_model->get_user_data($user_id, $data['user_basic']['user_type']);
        $data['rating_list'] = $this->admin_model->get_teacher_rating_list($user_id);
        
        
        if($data['user_type'] <= 2){
            $tmp_rating_from = array();
            $i=0;
            $data_user_type = array(
                '0' => 'Super Admin',
                '1' => 'Manager',
                '2' => 'Technical Manager',
                '3' => 'Teacher Manager',
                '4' => 'Personnel Manager',
                '5' => 'Customer Support',
                '6' => 'Teacher',
                '7' => 'Student'
                );
            foreach($data['rating_list'] as $row){
                $tmp1 = $this->admin_model->get_one_user($row['user_id']);
                $tmp2 = $this->admin_model->get_user_data($tmp1['id'], $tmp1['user_type']);
                $tmp_rating_from[$i] = $data_user_type[$tmp1['user_type']]." (". $tmp2['first_name'] ." ".$tmp2['last_name'] ." )";
            
                $i++;
            }
            
            $data['rating_from'] = $tmp_rating_from;
        }


        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 11;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $user_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $footer_data = array();
        if (isset($data['rating_list']) && sizeof($data['rating_list']) > 0) {
            $footer_data['teacher_feedback_rating'] = 1;
            $footer_data['rating_list'] = $data['rating_list'];
        }

        $this->load->view('header', $header_data);
        $this->load->view('teacher_feedback_rating', $data);
        $this->load->view('footer_profile', $footer_data);
    }

    public function add_annual_vacation($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') != 3)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('from', 'From', 'required');
        $this->form_validation->set_rules('to', 'To', 'required');

        if ($this->form_validation->run() == TRUE) {
            $from = "0000-" . $this->input->post('from', TRUE);
            $to = "0000-" . $this->input->post('to', TRUE);
            $this->admin_model->add_annual_vacation($user_id, $from, $to);
        }

        redirect('admin/annual_vacation/' . $user_id . "/");
    }

    public function remove_annual_vacation($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 4 && $this->session->userdata('user_type') != 3)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('remove_vacation', 'Remove Vacation', 'required');

        if ($this->form_validation->run() == TRUE) {
            $vacation_id = $this->input->post('remove_vacation', TRUE);
            $this->admin_model->remove_annual_vacation($user_id, $vacation_id);
        }

        redirect('admin/annual_vacation/' . $user_id . "/");
    }

    public function teacher_quality_monitoring($teacher_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 3 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('quality', 'Day', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['user_id'] = $this->session->userdata('user_id');
            $data['user_type'] = $this->session->userdata('user_type');

            $data['user_basic'] = $this->admin_model->get_one_user($teacher_id);

            $data['user_data'] = $this->admin_model->get_user_data($teacher_id, $data['user_basic']['user_type']);
            $data['quality'] = $this->admin_model->get_teacher_quality($teacher_id);

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 6;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $header_data['teacher_id'] = $teacher_id;
            $header_data['user_basic'] = $data['user_basic'];
            $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

            $this->load->view('header', $header_data);
            $this->load->view('teacher_quality_monitoring', $data);
            $this->load->view('footer_profile');
        } else {

            if ($this->session->userdata('user_type') == 3) {
                $quality = $this->input->post('quality', TRUE);
                $datetime = new DateTime; // current time = server time
                $otherTZ = new DateTimeZone('Europe/London');
                $datetime->setTimezone($otherTZ); // calculates with new TZ now
                $post_time = $datetime->format('Y-m-d H:i:s');
                $request_datetime = $post_time;

                $user_id = $this->session->userdata('user_id');

                $this->admin_model->add_teacher_quality_monitoring($teacher_id, $user_id, $quality, $request_datetime);
            }

            redirect('admin/teacher_quality_monitoring/' . $teacher_id . "/");
        }
    }

    public function teacher_monitoring_class($teacher_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('class_monitor', 'Monitoring Class', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['user_id'] = $this->session->userdata('user_id');
            $data['user_type'] = $this->session->userdata('user_type');

            $data['user_basic'] = $this->admin_model->get_one_user($teacher_id);

            $data['user_data'] = $this->admin_model->get_user_data($teacher_id, $data['user_basic']['user_type']);
            $data['quality'] = $this->admin_model->get_teacher_monitoring_class($teacher_id);

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 9;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $header_data['teacher_id'] = $teacher_id;
            $header_data['user_basic'] = $data['user_basic'];
            $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

            $this->load->view('header', $header_data);
            $this->load->view('teacher_monitoring_class', $data);
            $this->load->view('footer_profile');
        } else {

            if ($this->session->userdata('user_type') == 2) {
                $quality = $this->input->post('class_monitor', TRUE);

                $datetime = new DateTime; // current time = server time
                $otherTZ = new DateTimeZone('Europe/London');
                $datetime->setTimezone($otherTZ); // calculates with new TZ now
                $post_time = $datetime->format('Y-m-d H:i:s');
                $request_datetime = $post_time;

                $user_id = $this->session->userdata('user_id');

                $this->admin_model->add_teacher_monitoring_class($teacher_id, $user_id, $quality, $request_datetime);
            }

            redirect('admin/teacher_monitoring_class/' . $teacher_id . "/");
        }
    }

    public function teacher_obligation_to_the_managerial_order($teacher_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('obligation_to_the_managerial_order', 'Monitoring Class', 'required');

        if ($this->form_validation->run() == FALSE) {
            $data['user_id'] = $this->session->userdata('user_id');
            $data['user_type'] = $this->session->userdata('user_type');

            $data['user_basic'] = $this->admin_model->get_one_user($teacher_id);

            $data['user_data'] = $this->admin_model->get_user_data($teacher_id, $data['user_basic']['user_type']);
            $data['quality'] = $this->admin_model->get_teacher_obligation_to_the_managerial_order($teacher_id);

            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = 2;
            $header_data['sub_menu_id'] = 10;
            $header_data['user_type'] = $this->session->userdata('user_type');
            $header_data['teacher_id'] = $teacher_id;
            $header_data['user_basic'] = $data['user_basic'];
            $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

            $this->load->view('header', $header_data);
            $this->load->view('teacher_obligation_to_the_managerial_order', $data);
            $this->load->view('footer_profile');
        } else {
            if ($this->session->userdata('user_type') == 2) {
                $obligation_to_the_managerial_order = $this->input->post('obligation_to_the_managerial_order', TRUE);

                $datetime = new DateTime; // current time = server time
                $otherTZ = new DateTimeZone('Europe/London');
                $datetime->setTimezone($otherTZ); // calculates with new TZ now
                $post_time = $datetime->format('Y-m-d H:i:s');
                $request_datetime = $post_time;

                $user_id = $this->session->userdata('user_id');

                $this->admin_model->add_teacher_obligation_to_the_managerial_order($teacher_id, $user_id, $obligation_to_the_managerial_order, $request_datetime);
            }
            redirect('admin/teacher_obligation_to_the_managerial_order/' . $teacher_id . "/");
        }
    }

    public function teacher_schedule_time($teacher_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 3 && $this->session->userdata('user_type') != 2 && $this->session->userdata('user_type') != 1 && $this->session->userdata('user_type') != 0)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');

        $data['user_basic'] = $this->admin_model->get_one_user($teacher_id);

        $data['user_data'] = $this->admin_model->get_user_data($teacher_id, $data['user_basic']['user_type']);
        $data['schedule_time'] = $this->admin_model->get_teacher_schedule_time($teacher_id);

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 5;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $teacher_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $this->load->view('header', $header_data);
        $this->load->view('teacher_schedule_time', $data);
        $this->load->view('footer_profile');
    }

    public function add_teacher_schedule_time($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 3)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('day', 'Day', 'required');
        $this->form_validation->set_rules('from', 'Time', 'required');
        $this->form_validation->set_rules('to', 'Time', 'required');

        if ($this->form_validation->run() == TRUE) {
            $day = $this->input->post('day', TRUE);
            $from = $this->input->post('from', TRUE) . ":00";
            $to = $this->input->post('to', TRUE) . ":00";
            $this->admin_model->add_teacher_schedule_time($user_id, $day, $from, $to);
        }

        redirect('admin/teacher_schedule_time/' . $user_id . "/");
    }

    public function remove_teacher_schedule_time($user_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') != 3)
            redirect('admin/home/');

        $this->load->library('form_validation');


        $this->form_validation->set_rules('remove_schedule', 'Remove Schedule', 'required');

        if ($this->form_validation->run() == TRUE) {
            $id = $this->input->post('remove_schedule', TRUE);

            $this->admin_model->remove_teacher_schedule_time($id);
        }

        redirect('admin/teacher_schedule_time/' . $user_id . "/");
    }

    public function ticket_message($ticket_id = -1) {
        if (!$this->session->userdata('user_id') || $ticket_id == -1)
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7 && $this->session->userdata('user_type') != 6)
            redirect('admin/index/');

//authenticate user
        $temp_data = $this->admin_model->get_ticket_data($ticket_id);
        if (!isset($temp_data['user_id']) || $temp_data['user_id'] != $this->session->userdata('user_id'))
            redirect('admin/index/');
//end authenticate user


        $this->load->library('form_validation');
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {

            $header_data['user_type'] = $this->session->userdata('user_type');

            if ($header_data['user_type'] == 7)
                $header_data['menu_id'] = '12';
            else
                $header_data['menu_id'] = '18';


            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $data['ticket_message'] = $this->admin_model->get_ticket_message($ticket_id);
            $data['user_id'] = $this->session->userdata('user_id');
            $data['name'] = $this->session->userdata('first_name') . " " . $this->session->userdata('last_name');
            $data['user_type'] = $this->session->userdata('user_type');

            $this->load->view('header', $header_data);
            $this->load->view('ticket_message', $data);
            $this->load->view('footer_profile');
        } else {
            $message = $this->input->post('message', TRUE);
            $user_id = $this->session->userdata('user_id');
            $ticket_data = $this->admin_model->get_ticket_data($ticket_id);



            if (isset($ticket_data['user_id']) && $ticket_data['user_id'] == $user_id) {
                $datetime = new DateTime; // current time = server time
                $otherTZ = new DateTimeZone('Europe/London');
                $datetime->setTimezone($otherTZ); // calculates with new TZ now
                $post_time = $datetime->format('Y-m-d H:i:s');
                $request_datetime = $post_time;

                $this->admin_model->insert_ticket_message($ticket_id, $message, $request_datetime, $user_id);
            }


            redirect('admin/ticket_message/' . $ticket_id);
        }
    }

    public function customer_service_inbox() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5 && $this->session->userdata('user_type') != 0 && $this->session->userdata('user_type') != 1)
            redirect('admin/index/');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '19';
        $header_data['sub_menu_id'] = '2';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['usre_type'] = $this->session->userdata('user_type');
        $data['usre_id'] = $this->session->userdata('user_id');
        $data['tickets'] = $this->admin_model->get_all_tickets();

        $this->load->view('header', $header_data);
        $this->load->view('customer_service_inbox', $data);
        $this->load->view('footer_profile');
    }

    public function customer_service_inbox_thread($ticket_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5 && $this->session->userdata('user_type') != 0 && $this->session->userdata('user_type') != 1)
            redirect('admin/index/');


        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '19';
        $header_data['sub_menu_id'] = '2';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['usre_type'] = $this->session->userdata('user_type');
        $data['usre_id'] = $this->session->userdata('user_id');
        $data['ticket'] = $this->admin_model->get_ticket_data($ticket_id);
        $data['ticket_message'] = $this->admin_model->get_ticket_message($ticket_id);
        $data['user_basic'] = $this->admin_model->get_one_user($data['ticket']['user_id']);
        $data['user_data'] = $this->admin_model->get_user_data($data['ticket']['user_id'], $data['user_basic']['user_type']);

        $this->load->library('form_validation');

        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('header', $header_data);
            $this->load->view('customer_service_inbox_thread', $data);
            $this->load->view('footer_profile');
        } else {
            if ($data['usre_type'] != 5) {
                redirect('admin/index/');
            }

            $message = $this->input->post('message', TRUE);
            $user_id = $this->session->userdata('user_id');
            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;

            $ticket_data = $this->admin_model->get_ticket_data($ticket_id);
            $url = "index.php/admin/ticket_message/" . $ticket_id . "/";

            $this->admin_model->insert_ticket_message($ticket_id, $message, $request_datetime, $user_id);
            $this->insert_inbox_notification($ticket_data['user_id'], 7, "Customer Service", $message, $url, $request_datetime);
            redirect('admin/customer_service_inbox_thread/' . $ticket_id . '/');
        }
    }

    public function customer_service() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = '12';
            $header_data['user_type'] = $this->session->userdata('user_type');

            $data['ticket_list'] = $this->admin_model->get_tickets($this->session->userdata('user_id'));

            $this->load->view('header', $header_data);
            $this->load->view('customer_service', $data);
            $this->load->view('footer_profile');
        } else {
            $subject = $this->input->post('subject', TRUE);
            $message = $this->input->post('message', TRUE);

            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;

            $this->admin_model->create_new_ticket($subject, $message, $this->session->userdata('user_id'), $this->session->userdata('user_type'), $request_datetime);
            redirect('admin/customer_service');
        }
    }

    public function contact_support() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 6)
            redirect('admin/index/');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = '18';
            $header_data['user_type'] = $this->session->userdata('user_type');

            $data['ticket_list'] = $this->admin_model->get_tickets($this->session->userdata('user_id'));

            $this->load->view('header', $header_data);
            $this->load->view('contact_support', $data);
            $this->load->view('footer_profile');
        } else {
            $subject = $this->input->post('subject', TRUE);
            $message = $this->input->post('message', TRUE);

            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;

            $this->admin_model->create_new_ticket($subject, $message, $this->session->userdata('user_id'), $this->session->userdata('user_type'), $request_datetime);
            redirect('admin/contact_support/');
        }
    }

    public function running_courses() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '7';
        $header_data['sub_menu_id'] = '2';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['course_list'] = $this->admin_model->get_running_class_list($this->session->userdata('user_id'));

        $this->load->view('header', $header_data);
        $this->load->view('running_courses', $data);
        $this->load->view('footer_profile');
    }

    public function completted_courses() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 7)
            redirect('admin/index/');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '7';
        $header_data['sub_menu_id'] = '3';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['course_list'] = $this->admin_model->get_completted_class_list($this->session->userdata('user_id'));

        $this->load->view('header', $header_data);
        $this->load->view('completted_courses', $data);
        $this->load->view('footer_profile');
    }

    public function running_classes() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 6)
            redirect('admin/index/');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '16';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $this->load->library('runningclasslist');




        $data['course_list'] = $this->admin_model->get_classes_teacher($this->session->userdata('user_id'), 1);

        $course_name = array('1' => 'Arabic', '2' => 'Quran', '3' => 'Arabic & Quran');
        $course_level = array('1' => 'Beginner', '6' => 'Intermediate', '13' => 'Advanced');
        $student_name = "";
        foreach ($data['course_list'] as $row) {
            $tmp = $this->admin_model->get_class_students($row['id']);

            $datetime = new DateTime($row['start_date']);
            $student_name = "";
            foreach ($tmp as $row2) {
                if ($student_name != "") {
                    $student_name = $student_name . ', ';
                }
                $name_tmp = $this->admin_model->get_user_data($row2['student_id'], '7');

                $student_name = $student_name . $name_tmp['first_name'] . " " . $name_tmp['last_name'];
            }

            $this->runningclasslist->addElement($row['id'], $course_name[$row['course_id']], $course_level[$row['level']], $student_name, date_format($datetime, "d, M Y"));
        }
        $this->runningclasslist->sortList();
        $list_class = array();
        for ($i = 0; $i < $this->runningclasslist->size(); $i++) {
            $tmp_data = $this->runningclasslist->getElement($i);
            $data = array(
                'class_id' => $tmp_data->class_id,
                'course_name' => $tmp_data->course_name,
                'course_level' => $tmp_data->course_leve,
                'student_name' => $tmp_data->student_name,
                'datetime' => $tmp_data->datetime
            );
            $list_class[$i] = $data;
        }
        $data['list_class'] = $list_class;



        $this->load->view('header', $header_data);
        $this->load->view('running_classes', $data);
        $this->load->view('footer_profile');
    }

    public function completed_classes() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 6)
            redirect('admin/index/');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '17';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['course_list'] = $this->admin_model->get_classes_teacher($this->session->userdata('user_id'), 2);

        $this->load->view('header', $header_data);
        $this->load->view('completed_classes', $data);
        $this->load->view('footer_profile');
    }

    public function student_attendance_dates($class_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $date_array = array();
        $user_type = $this->session->userdata('user_type');

        $index = 0;


        $class_data = $this->admin_model->get_class_data($class_id);
        $class_schedule = $this->admin_model->get_class_schedule($class_id);
        $class_name = "";
        switch ($class_data['course_id']) {
            case 1:
                $class_name = "Arabic";
                break;
            case 2:
                $class_name = "Quran";
                break;
            case 3:
                $class_name = "Arabic & Quran";
                break;
        }

        $attendance_data = $this->admin_model->latest_student_attendance($class_id);
        //  var_dump($attendance_data);
        $flag = FALSE;
        if (!isset($attendance_data['class_id'])) {
            $tmp_date = new DateTime($class_data['start_date']);
            $flag = TRUE;
        } else
            $tmp_date = new DateTime($attendance_data['datetime']);

        foreach ($class_schedule as $schedule) {
            $current_time = new DateTime;
            $otherTZ = new DateTimeZone('Europe/London');
            $current_time->setTimezone($otherTZ);


            $change_date = new DateTime(date_format($tmp_date, "Y-m-d"));

            if ($flag == TRUE)
                $change_date->modify('previous ' . $schedule['day']);

            for ($i = 0;; $i++) {

                $change_date->modify('next ' . $schedule['day']);

                //echo "<br>".date_format($change_date, "Y-m-d")." > ".date_format($current_time, "Y-m-d")." day = ".$schedule['day'];
                if ($change_date <= $current_time) {
                    //      echo "<br>fk";
                    $date_array[$index]['datetime'] = new Datetime(date_format($change_date, "Y-m-d"));
                    $date_array[$index]['class_name'] = $class_name;
                    $date_array[$index]['class_time'] = $schedule['time'];
                    $index++;
                } else {
                    break;
                }
            }
        }

        return $date_array;
    }

    private function teacher_attendance_dates($teacher_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $class_list = $this->admin_model->get_teacher_running_class($teacher_id);

        $date_array = array();


        $id_name = 'id';

        $index = 0;
        foreach ($class_list as $row) {
            $class_data = $this->admin_model->get_class_data($row['id']);
            $class_schedule = $this->admin_model->get_class_schedule($row['id']);

            $class_name = "";

            switch ($class_data['course_id']) {
                case 1:
                    $class_name = "Arabic";
                    break;
                case 2:
                    $class_name = "Quran";
                    break;
                case 3:
                    $class_name = "Arabic & Quran";
                    break;
            }

            $latest_attendance = $this->admin_model->latest_teacher_attendance($row['id']);

            foreach ($class_schedule as $schedule) {
                $current_time = new DateTime;
                $otherTZ = new DateTimeZone('Europe/London');
                $current_time->setTimezone($otherTZ);
                $current_time->modify('+1 day');

                if (!isset($latest_attendance['datetime'])) {
                    $change_date = new DateTime($class_data['start_date']);
                    $change_date->modify('previous ' . $schedule['day']);
                } else {
                    $change_date = new DateTime($latest_attendance['datetime']);
                }


                for ($i = 0;; $i++) {
                    $change_date->modify('next ' . $schedule['day']);
                    //   echo "<br>".date_format($change_date, "Y-m-d")." > ".date_format($start_date, "Y-m-d");
                    if ($change_date <= $current_time) {
                        //      echo "<br>fk";
                        $date_array[$index]['datetime'] = new Datetime(date_format($change_date, "Y-m-d"));
                        $date_array[$index]['class_name'] = $class_name;
                        $date_array[$index]['class_id'] = $row['id'];
                        $date_array[$index]['class_time'] = $schedule['time'];
                        $index++;
                    } else {
                        break;
                    }
                }
            }
        }

        return $date_array;
    }

    public function class_view($class_id = 0, $tabname = "") {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '7';
        $header_data['sub_menu_id'] = '2';
        $header_data['user_type'] = $this->session->userdata('user_type');

        if ($header_data['user_type'] == 7) {
            if (!$this->admin_model->student_class_validity($class_id, $user_id))
                redirect('admin/index/');

            $data['teacher_rating'] = $this->admin_model->get_class_rating($class_id, $user_id);
        }

        if ($header_data['user_type'] == 6) {
            if (!$this->admin_model->teacher_class_validity($class_id, $user_id))
                redirect('admin/index/');
        }

        $this->load->library('form_validation');

        $this->form_validation->set_rules('message', 'Message', 'required');

        if ($this->form_validation->run() == FALSE) {

            $data['class_data'] = $this->admin_model->get_class_data($class_id);
            $data['class_schedule'] = $this->admin_model->get_class_schedule($class_id);
            $data['class_students'] = $this->admin_model->get_class_students($class_id);
            $data['class_file'] = $this->admin_model->get_class_file($class_id);

            $data['student_attendance_dates'] = $this->student_attendance_dates($class_id);



            $data['class_message'] = $this->admin_model->get_class_message($class_id);

            $data['teacher_details'] = $this->admin_model->get_user_data($data['class_data']['teacher_id'], '6');

            $data['user_type'] = $header_data['user_type'];
            $data['user_id'] = $this->session->userdata('user_id');

            $data['class_id'] = $class_id;

            $data['tabname'] = $tabname;
            //initialization 
            $tmp['000'] = 0;

            $tmp2['000'] = 0;

            foreach ($data['class_students'] as $row) {
                $tmp[$row['student_id']] = $this->admin_model->get_user_data($row['student_id'], '7');

                $tmp2[$row['student_id']] = $this->admin_model->get_quiz_marks($class_id, $row['student_id']);

                $data['student_attendance'][$row['student_id']] = $this->admin_model->get_student_attendance($class_id, $row['student_id']);
            }
            $data['student_details'] = $tmp;
            $data['quiz_marks'] = $tmp2;

            $header_data['inbox_notification'] = $this->get_inbox_notification();

            if ($header_data['user_type'] == 7 && $data['class_data']['status'] == 2) {
                $header_data['sub_menu_id'] = 3;
            } else if ($header_data['user_type'] == 6 && $data['class_data']['status'] == 1) {
                $header_data['menu_id'] = 16;
            } else if ($header_data['user_type'] == 6 && $data['class_data']['status'] == 2) {
                $header_data['menu_id'] = 17;
            }

            $footer_data = array();
            if (isset($data['teacher_rating']['teacher_id']) && $data['class_data']['status'] == 2) {
                $footer_data['score'] = $data['teacher_rating']['rating'];
                $footer_data['rating_status'] = 1;
            } else if ($data['class_data']['status'] == 2) {
                $footer_data['rating_status'] = 0;
            }



            $this->load->view('header', $header_data);
            $this->load->view('class_view', $data);
            $this->load->view('footer_profile', $footer_data);
        } else {
            $message = $this->input->post('message', TRUE);
            $user_id = $this->session->userdata('user_id');
            $user_type = $this->session->userdata('user_type');


            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;

            /*
             * *process inbox notification
             */
            $class_data = $this->admin_model->get_class_data($class_id);
            $class_name = "";
            switch ($class_data['course_id']) {
                case 1:
                    $class_name = "Arabic - course";
                    break;
                case 2:
                    $class_name = "Quran - course";
                    break;
                case 3:
                    $class_name = "Arabic and Quran - course";
                    break;
            }

            if ($user_type == 6) {
                $students = $this->admin_model->get_class_students($class_id);

                foreach ($students as $row) {
                    $to = $row['student_id'];
                    $user_type = 7;
                    $from_name = $class_name;
                    $url = "index.php/admin/class_view/" . $class_id . "/";

                    $this->insert_inbox_notification($to, $user_type, $from_name, $message, $url, $request_datetime);
                }
            } else if ($user_type == 7) {
                $user_data = $this->admin_model->get_user_data($user_id, $user_type);
                $to = $class_data['teacher_id'];
                $from_name = $class_name . " (" . $user_data['first_name'] . ")";
                $url = "index.php/admin/class_view/" . $class_id . "/";

                $this->insert_inbox_notification($to, $user_type, $from_name, $message, $url, $request_datetime);
            }

            /*
             * * end process inbox notification
             */


            $this->admin_model->insert_class_message($message, $user_id, $class_id, $request_datetime);

            redirect('admin/class_view/' . $class_id . '/message/');
        }
    }

    public function give_teacher_attendance($teacher_id) {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('datetime', 'Datetime', 'required');
        $this->form_validation->set_rules('attendance', 'Attendance', 'required');
        if ($this->form_validation->run() == TRUE) {
            $attendance = $this->input->post('attendance' . $row['student_id'], TRUE);
            $datetime_tmp = $this->input->post('datetime', TRUE);
            $class_id = strtok($datetime_tmp, "|");
            $datetime = strtok("|");

            $this->admin_model->insert_class_teacher_attendance($class_id, $teacher_id, $attendance, $datetime);
        }

        redirect('admin/teacher_daily_update/' . $teacher_id . '/');
    }

    public function teacher_daily_update($teacher_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        if ($this->session->userdata('user_type') > 5)
            redirect('admin/home/');

        $data['user_id'] = $this->session->userdata('user_id');
        $data['user_type'] = $this->session->userdata('user_type');
        $data['teacher_id'] = $teacher_id;

        $data['user_basic'] = $this->admin_model->get_one_user($teacher_id);

        $data['user_data'] = $this->admin_model->get_user_data($teacher_id, $data['user_basic']['user_type']);

        $data['running_class'] = $this->admin_model->get_teacher_running_class($teacher_id);

        $data['teacher_details'] = $this->admin_model->get_user_data($teacher_id, 6);
        $data['teacher_attendance_dates'] = $this->teacher_attendance_dates($teacher_id);




        foreach ($data['running_class'] as $row) {
            $data['attendance_data'][$row['id']] = $this->admin_model->get_teacher_attendance($row['id'], $teacher_id);
        }

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 2;
        $header_data['sub_menu_id'] = 7;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['teacher_id'] = $teacher_id;
        $header_data['user_basic'] = $data['user_basic'];
        $data['sub_menu_data'] = $this->load->view('sub_menu_data', $header_data, TRUE);

        $footer_data = array();
        if (isset($data['rating_list']) && sizeof($data['rating_list']) > 0) {
            $footer_data['teacher_feedback_rating'] = 1;
            $footer_data['rating_list'] = $data['rating_list'];
        }

        $this->load->view('header', $header_data);
        $this->load->view('teacher_daily_update', $data);
        $this->load->view('footer_profile', $footer_data);
    }

    public function give_student_attendance($class_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 6)
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        if (!$this->admin_model->teacher_class_validity($class_id, $user_id))
            redirect('admin/index/');

        $class_data = $this->admin_model->get_class_data($class_id);
        $class_students = $this->admin_model->get_class_students($class_id);

        if (sizeof($class_students) > 0) {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('datetime', 'Datetime', 'required');

            foreach ($class_students as $row) {
                $this->form_validation->set_rules('attendance' . $row['student_id'], 'Attendance', 'required');
            }

            if ($this->form_validation->run() == TRUE) {
                foreach ($class_students as $row) {
                    $attendance = $this->input->post('attendance' . $row['student_id'], TRUE);
                    $datetime = $this->input->post('datetime', TRUE);
                    $this->admin_model->insert_class_student_attendance($class_id, $row['student_id'], $attendance, $datetime);
                }
            }
        }

        redirect('admin/class_view/' . $class_id . "/attendance/");
    }

    public function class_upload_file($class_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 6)
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        if (!$this->admin_model->teacher_class_validity($class_id, $user_id))
            redirect('admin/index/');


        $config['upload_path'] = './class_files/';
        $config['allowed_types'] = 'pdf';
        $config['max_size'] = '30000';


        $this->load->library('upload', $config);



        if ($this->upload->do_upload('choose_file')) {



            $file = $this->upload->data();
            $file_name = $this->security->sanitize_filename($file['file_name'], TRUE);
            $file_path = $file['file_path'];
            $file_full_path = $file['full_path'];
            $file_ext = $file['file_ext'];

            if ($this->security->xss_clean($file, TRUE) == TRUE) {

                $datetime = new DateTime; // current time = server time
                $otherTZ = new DateTimeZone('Europe/London');
                $datetime->setTimezone($otherTZ); // calculates with new TZ now
                $post_time = $datetime->format('Y-m-d H:i:s');
                $request_datetime = $post_time;
                $file_id = $this->admin_model->insert_class_file($class_id, $user_id, $file_name, $request_datetime);
                $file_new_ulr = md5($file_id) . $file_ext;
                $this->admin_model->update_class_file_url($file_id, $file_new_ulr);

                rename($file_full_path, $file_path . $file_new_ulr);

                $class_data = $this->admin_model->get_class_data($class_id);
                $class_name = "";
                switch ($class_data['course_id']) {
                    case 1:
                        $class_name = "Arabic";
                        break;
                    case 2:
                        $class_name = "Quran";
                        break;
                    case 3:
                        $class_name = "Arabic and Quran";
                        break;
                }

                $name = $class_name . " - " . $file_name;
                $url = 'index.php/admin/class_view/' . $class_id . '/';

                $students = $this->admin_model->get_class_students($class_id);

                foreach ($students as $row)
                    $this->admin_model->insert_user_notification($row['student_id'], $name, $url, $request_datetime);
            } else {
                unlink("./class_files/" . $file_name);
            }
        } else {
            $error = array('error' => $this->upload->display_errors());
        }
        redirect('admin/class_view/' . $class_id . '/files/');
    }

    public function clss_view_update_marks($class_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');


        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['inbox_notification'] = $this->get_inbox_notification();

        if ($header_data['user_type'] != 6) {
            redirect('admin/index/');
        }

        if ($header_data['user_type'] == 6) {
            if (!$this->admin_model->teacher_class_validity($class_id, $user_id))
                redirect('admin/index/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('student', 'Student', 'required');
        $this->form_validation->set_rules('total_marks', 'Total Marks', 'required');
        $this->form_validation->set_rules('obtain_marks', 'Obtain Marks', 'required');
        $this->form_validation->set_rules('topic', 'topic', 'required');
        $this->form_validation->set_rules('start_date', 'Start Date', 'required');

        if ($this->form_validation->run() == TRUE && $this->input->post('student', TRUE) != "") {
            $student = $this->input->post('student', TRUE);
            $total_marks = $this->input->post('total_marks', TRUE);
            $obtain_marks = $this->input->post('obtain_marks', TRUE);
            $topic = $this->input->post('topic', TRUE);
            $start_date = $this->input->post('start_date', TRUE);

            $this->admin_model->add_class_marks($class_id, $student, $topic, $total_marks, $obtain_marks, $start_date);
        }
        redirect('admin/class_view/' . $class_id . "/grades/");
    }

    public function remove_quiz_marks($class_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');


        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['inbox_notification'] = $this->get_inbox_notification();

        if ($header_data['user_type'] != 6) {
            redirect('admin/index/');
        }

        if ($header_data['user_type'] == 6) {
            if (!$this->admin_model->teacher_class_validity($class_id, $user_id))
                redirect('admin/index/');
        }


        $this->load->library('form_validation');

        $this->form_validation->set_rules('remove_schedule', 'Remove Marks', 'required');

        if ($this->form_validation->run() == TRUE && $this->input->post('remove_schedule', TRUE) != "") {
            $remove_id = $this->input->post('remove_schedule', TRUE);

            $this->admin_model->remove_quiz_marks($remove_id);
        }
        redirect('admin/class_view/' . $class_id . "/grades/");
    }

    public function assign_class() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if (($this->session->userdata('user_type') != 5) && ($this->session->userdata('user_type') != 0) )
            redirect('admin/index/');

        $header_data['menu_id'] = '20';
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['inbox_notification'] = $this->get_inbox_notification();

        $data['request_list'] = $this->admin_model->get_request_list_all();

        $this->load->view('header', $header_data);
        $this->load->view('assign_class', $data);
        $this->load->view('footer_profile');
    }

    public function assign_class_details($request_id, $message = -1) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');

        $header_data['menu_id'] = '20';
        $header_data['user_type'] = $this->session->userdata('user_type');
        $header_data['inbox_notification'] = $this->get_inbox_notification();

        $data['message'] = $message;
        $footer_data = array();


        $data['class_id'] = $request_id;
        $data['request_data'] = $this->admin_model->get_one_request_data($request_id);
        $data['requested_schedule'] = $this->admin_model->get_request_schedule($request_id);
        $data['suggested_teachers'] = $this->admin_model->get_course_teachers($request_id);
        $data['class_data'] = $this->admin_model->get_class_data($request_id);

        if ($data['class_data']['status'] == 1)
            $header_data['menu_id'] = 21;
        if ($data['class_data']['status'] == 2)
            $header_data['menu_id'] = 22;

        if ($data['class_data']['teacher_id'] != -1) {
            $data['teacher_details'] = $this->admin_model->get_user_data($data['class_data']['teacher_id'], 6);
            $data['teacher_basic'] = $this->admin_model->get_one_user($data['class_data']['teacher_id']);
        }
        $data['student_details'] = $this->admin_model->get_user_data($data['request_data']['user_id'], 7);
        $data['student_basic'] = $this->admin_model->get_one_user($data['request_data']['user_id']);
        $data['more_class_students'] = $this->admin_model->get_more_class_students($request_id, $data['request_data']['user_id']);
        $data['class_schedule'] = $this->admin_model->get_class_schedule($request_id);

        $data['more_student_list'] = $this->admin_model->get_more_students($data['request_data']['course_id'], $data['request_data']['level'], $data['request_data']['user_id']);

        foreach ($data['suggested_teachers'] as $row) {
            $tmp[$row['user_id']] = $this->admin_model->get_course_teacher_schedule($row['user_id']);
        }

        if (isset($tmp))
            $data['teacher_schedule'] = $tmp;

        //var_dump($footer_data['teacher_rating']);

        if (sizeof($data['suggested_teachers']) > 0) {
            $footer_data['assign_class'] = 1;
        }
        $i = 0;
        foreach ($data['suggested_teachers'] as $row) {
            $data['teacher_rating'][$i] = $footer_data['teacher_rating'][$i] = $this->admin_model->count_teacher_rating($row['user_id']);
        }


        $this->load->view('header', $header_data);
        $this->load->view('assign_class_details', $data);
        $this->load->view('footer_list', $footer_data);
    }

    public function add_class_time($class_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('day', 'Day', 'required');
        $this->form_validation->set_rules('time', 'Time', 'required');

        if ($this->form_validation->run() === TRUE && $this->input->post('day', TRUE) != "" && $this->input->post('time', TRUE) != "") {
            $day = $this->input->post('day', TRUE);
            $time = $this->input->post('time', TRUE);
            $gmt=$this->session->userdata('time_zone');
            $sign = substr($gmt, 0, 1);
            if($sign=='+')
            {

                $a=explode('.', $gmt);
                $b=$a[0];
                $c=$a[1];
                $b=substr($b,1);
                $b=$b * 60;
                $b=$b+$c;
                $date = new DateTime("2000-01-20 $time");
                $date->sub(new DateInterval('PT'.$b.'M'));
                $time=$date->format('H:i:s');
            }
            else if($sign=='-')
            {
                $a=explode('.', $gmt);
                $b=$a[0];
                $c=$a[1];
                $b=substr($b,1);
                $b=$b * 60;
                $b=$b+$c;
                $date = new DateTime("2000-01-20 $time");
                $date->add(new DateInterval('PT'.$gmt.'M'));
                $time=$date->format('H:i:s');
            }

            $this->admin_model->insert_class_schedule($class_id, $day, $time);
        }

        redirect('admin/' . $url . "/" . $class_id . "/");
    }

    public function remove_class_time($class_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('remove_schedule', 'Remove Schedule', 'required');
        if ($this->form_validation->run() === TRUE && $this->input->post('remove_schedule', TRUE) != "") {
            $id = $this->input->post('remove_schedule', TRUE);
            $this->admin_model->remove_class_time($class_id, $id);
        }

        redirect('admin/' . $url . "/" . $class_id . "/");
    }

    public function add_class_teacher($class_id, $teacher_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');

        $this->admin_model->update_class_teacher($class_id, $teacher_id);

        redirect('admin/' . $url . "/" . $class_id . "/");
    }

    public function add_class_student($class_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('student_id', 'Student Id', 'required');
        if ($this->form_validation->run() === TRUE && $this->input->post('student_id', TRUE) != "") {
            $id = $this->input->post('student_id', TRUE);
            $this->admin_model->add_class_student($class_id, $id);
        }

        redirect('admin/' . $url . "/" . $class_id . "/");
    }

    public function remove_class_student($class_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');

        $this->load->library('form_validation');

        $this->form_validation->set_rules('student_id', 'Student Id', 'required');
        if ($this->form_validation->run() === TRUE && $this->input->post('student_id', TRUE) != "") {
            $id = $this->input->post('student_id', TRUE);
            $this->admin_model->remove_class_student($class_id, $id);
        }

        redirect('admin/' . $url . "/" . $class_id . "/");
    }

    public function activate_class($class_id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');



        $class_data = $this->admin_model->get_class_data($class_id);
        $request_data = $this->admin_model->get_one_request_data($class_id);
        $class_schedule = $this->admin_model->get_class_schedule($class_id);

        $message = "";
        if ($class_data['teacher_id'] != -1 && sizeof($class_schedule) > 0) {
            $this->admin_model->add_class_student($class_id, $request_data['user_id']);
            //acitivate this class
            $this->admin_model->activate_class($class_id);
            //deactivete this request
            $this->admin_model->deactivate_request($class_id);

            $more_class_student = $this->admin_model->get_more_class_students($request_id, $request_data['user_id']);

            foreach ($more_class_student as $row) {
                $request_list = $this->admin_model->get_request_list($row['student_id'], 0);

                foreach ($request_list as $one_request) {
                    if (
                            $one_request['course_id'] == $request_data['course_id'] &&
                            $one_request['level'] == $request_data['level']
                    ) {
                        //delete classes of other students
                        $this->admin_model->remove_class($one_request['id']);
                        //deactivate other student's request
                        $this->admin_model->deactivate_request($one_request['id']);
                    }
                }
            }
            $message = 1;
        } else {
            $message = 0;
        }

        redirect('admin/assign_class_details/' . $class_id . "/" . $message . "/");
    }

    private function get_inbox_notification() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $data['inbox_notification'] = $this->admin_model->get_inbox($user_id, $user_type);
        $data['other_notification'] = $this->admin_model->get_notification($user_id, $user_type);

        return $data;
    }

    private function insert_inbox_notification($to, $user_type, $from_name, $message, $url, $datetime) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');


        $data = $this->admin_model->insert_inbox($to, $user_type, $from_name, $message, $url, $datetime);

        return $data;
    }

    public function process_inbox($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_id');

        $data = $this->admin_model->get_inbox_data($id);
        if ($user_type == 7) {
            if (!isset($data['to']) || ($data['to'] != $user_id ))
                redirect('admin/index/');
        }
        if ($user_type == 6) {
            if (!isset($data['to']) || ($data['to'] != $user_id ))
                redirect('admin/index/');
        }

        $this->admin_model->remove_inbox_data($id);
        redirect(base_url() . $data['url']);
    }

    public function process_notification($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $data = $this->admin_model->get_notification_data($id);

        if ($data['to'] != $user_id)
            redirect('admin/index/');

        $this->admin_model->remove_notification($id);

        redirect(base_url() . $data['url']);
    }

    public function rate_teacher($class_id, $cs_teacher_id = -1) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($class_id != -200 && $this->session->userdata('user_type') != 7)
            redirect('admin/index/');
        else if ($this->session->userdata('user_type') != 5)
            redirect('admin/index/');
        /*
          if (!$this->admin_model->student_class_validity($class_id, $this->session->userdata('user_id')))
          redirect('admin/index/');
         */
        $this->load->library('form_validation');

        $this->form_validation->set_rules('entity', 'Rate Teacher', 'required');
        $this->form_validation->set_rules('feedback', 'Feedback', 'required');

        $user_id = $this->session->userdata('user_id');
        $class_data = $this->admin_model->get_class_data($class_id);
        $teacher_rating = $this->admin_model->get_class_rating($class_id, $user_id);


        if ($class_id == -200 && $this->session->userdata('user_type') == 5) {
            if ($this->form_validation->run() === TRUE) {

                $rate = $this->input->post('entity', TRUE);
                $feedback = $this->input->post('feedback', TRUE);
                $user_id = $this->session->userdata('user_id');

                $datetime = new DateTime; // current time = server time
                $otherTZ = new DateTimeZone('Europe/London');
                $datetime->setTimezone($otherTZ); // calculates with new TZ now
                $post_time = $datetime->format('Y-m-d H:i:s');
                $request_datetime = $post_time;
                foreach ($rate as $row) {
                    $rating = $row;
                }

                $this->admin_model->insert_rate_teacher($class_id, $user_id, $cs_teacher_id, $rating, $feedback, $request_datetime);
            }

            redirect('admin/teacher_feedback_rating/' . $cs_teacher_id . '/');
        } else if ($this->form_validation->run() === TRUE && isset($class_data['status']) && $class_data['status'] == 2 && $teacher_rating['teacher_id']) {
            $rate = $this->input->post('entity', TRUE);
            $feedback = $this->input->post('feedback', TRUE);
            $user_id = $this->session->userdata('user_id');

            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;



            foreach ($rate as $row) {
                $rating = $row;
            }

            $this->admin_model->insert_rate_teacher($class_id, $user_id, $class_data['teacher_id'], $rating, $feedback, $request_datetime);
        }

        redirect('admin/class_view/' . $class_id);
    }

    public function running_class() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5 && $this->session->userdata('user_type') > 1) {
            redirect('admin/home/');
        }


        $data['running_class_list'] = $this->admin_model->get_all_running_class_list();

        foreach ($data['running_class_list'] as $row) {
            $data['teacher_details'][$row['id']] = $this->admin_model->get_user_data($row['teacher_id'], 6);
            $data['teacher_basic'][$row['id']] = $this->admin_model->get_one_user($row['teacher_id']);
            $data['class_students'][$row['id']] = $this->admin_model->get_class_students_data($row['id']);
        }

        $data['user_type'] = $this->session->userdata('user_type');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 21;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('running_class_list', $data);
        $this->load->view('footer_list');
    }

    public function completed_class() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5 && $this->session->userdata('user_type') > 1) {
            redirect('admin/home/');
        }


        $data['completed_class_list'] = $this->admin_model->get_all_completed_class_list();

        foreach ($data['completed_class_list'] as $row) {
            $data['teacher_details'][$row['id']] = $this->admin_model->get_user_data($row['teacher_id'], 6);
            $data['teacher_basic'][$row['id']] = $this->admin_model->get_one_user($row['teacher_id']);
            $data['class_students'][$row['id']] = $this->admin_model->get_class_students_data($row['id']);
        }

        $data['user_type'] = $this->session->userdata('user_type');

        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = 22;
        $header_data['user_type'] = $this->session->userdata('user_type');
        $this->load->view('header', $header_data);
        $this->load->view('completed_class_list', $data);
        $this->load->view('footer_list');
    }

    public function class_mark_as_complete($class_id, $url) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5) {
            redirect('admin/home/');
        }
        $class_details = $this->admin_model->get_class_data($class_id);

        if ($class_details['status'] == 1) {
            $this->admin_model->class_mark_as_complete($class_id);
        }
        redirect('admin/' . $url . "/" . $class_id . "/");
    }

    public function promotional_offers() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') > 1) {
            redirect('admin/home/');
        }
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');


        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '23';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['usre_type'] = $this->session->userdata('user_type');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['promotional_offers'] = $this->admin_model->get_all_promotional_offers();

        foreach ($data['promotional_offers'] as $row) {
            $tmp = $this->admin_model->get_one_user($row['user_id']);
            $data['user_name'][$row['id']] = $this->admin_model->get_user_data($row['user_id'], $tmp['user_type']);
        }

        /* student data list */

//        foreach ($data['user_name'] as $header_data['start']) {
//            $tmp = $header_data['start']['id'];
//            $data['user_name'][$row['id']] = $tmp['session_data'];
//        }
        /* end student data list */

        $this->load->view('header', $header_data);
        $this->load->view('promotional_offers', $data);
        $this->load->view('footer_profile');
    }

    public function new_promotional_offer() {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') > 1) {
            redirect('admin/home/');
        }
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');

        $this->load->library('form_validation');
        $this->form_validation->set_rules('subject', 'Subject', 'required');
        $this->form_validation->set_rules('offer', 'Offer', 'required');

        if ($this->form_validation->run() == FALSE) {
            $header_data['inbox_notification'] = $this->get_inbox_notification();
            $header_data['menu_id'] = '23';
            $header_data['user_type'] = $this->session->userdata('user_type');

            $data['usre_type'] = $this->session->userdata('user_type');
            $data['user_id'] = $this->session->userdata('user_id');


            $this->load->view('header', $header_data);
            $this->load->view('new_promotional_offer', $data);
            $this->load->view('footer_profile');
        } else {
            $subject = $this->input->post('subject', TRUE);
            $offer = $this->input->post('offer', TRUE);

            $datetime = new DateTime; // current time = server time
            $otherTZ = new DateTimeZone('Europe/London');
            $datetime->setTimezone($otherTZ); // calculates with new TZ now
            $post_time = $datetime->format('Y-m-d H:i:s');
            $request_datetime = $post_time;

            $this->admin_model->insert_promotional_offer($user_id, $subject, $offer, $request_datetime);

            redirect('admin/new_promotional_offer');
        }
    }

    public function view_promotional_offer($id) {
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') > 1) {
            redirect('admin/home/');
        }
        $user_id = $this->session->userdata('user_id');
        $user_type = $this->session->userdata('user_type');


        $header_data['inbox_notification'] = $this->get_inbox_notification();
        $header_data['menu_id'] = '23';
        $header_data['user_type'] = $this->session->userdata('user_type');

        $data['usre_type'] = $this->session->userdata('user_type');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['promotional_offer_data'] = $this->admin_model->get_one_promotional_offer($id);



        $this->load->view('header', $header_data);
        $this->load->view('view_promotional_offer', $data);
        $this->load->view('footer_profile');
    }
    
    public function reactivate_class($class_id){
        if (!$this->session->userdata('user_id'))
            redirect('admin/index/');

        if ($this->session->userdata('user_type') != 5) {
            redirect('admin/home/');
        }
        
        $this->load->library('form_validation');
        $this->form_validation->set_rules('end_date', 'End Date', 'required');

        if ($this->form_validation->run() == TRUE) {
            $end_date = $this->input->post('end_date', TRUE);
            $this->admin_model->change_class_end_date($class_id, $end_date);
            $this->admin_model->change_class_status($class_id, 1);
        }
        
        redirect('admin/assign_class_details/'.$class_id."/");
    }
    
   

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */    